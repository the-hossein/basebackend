﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entity;
using Microsoft.Extensions.DependencyInjection;
using RepositoryLayer.Contract;
using RepositoryLayer.Repositories;


namespace IocLayer
{
    public static partial class RepositoryInstance
    {
        public static void AddRepositoryInstance(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IUserUploadFileRepository, UserUploadFileRepository>();
            services.AddScoped<IReservationRepository, ReservationRepository>();
            services.AddScoped<ICounselingRepository, CounselingRepository>();
            services.AddScoped<IOtpRepository, OtpRepository>();
            services.AddScoped<ISmsRepository, SmsRepository>();
            services.AddScoped<ISampleRepository, SampleRepository>();
            services.AddScoped<IPescriptionRepository, PescriptionRepository>();

        }
    }
}
