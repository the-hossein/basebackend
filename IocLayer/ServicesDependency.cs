﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entity;
using Microsoft.Extensions.DependencyInjection;
using ServicesLayer.Contract;
using ServicesLayer.Services;

namespace IocLayer
{
    public static partial class ServicesDependency
    {
        public static void AddServicesDependency(this IServiceCollection services)
        {
            services.AddTransient<IStoredProcedures, StoredProcedures>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUserUploadFileService, UserUploadFileService>();
            services.AddTransient<IShortUrlService, ShortUrlService>();
            services.AddTransient<IReservationService, ReservationService>();
            services.AddTransient<ICounselingService, CounselingService>();
            services.AddTransient<ISmsService, SmsService>();
            services.AddTransient<IOtpService, OtpService>();
            services.AddTransient<ISampleService, SampleService>();
            services.AddTransient<IPescriptionService, PescriptionService>();
        }
    }
}
