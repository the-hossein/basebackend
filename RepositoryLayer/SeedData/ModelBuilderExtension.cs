﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Enums;
using DataAccessLayer.Enums.Attributes;
using DataAccessLayer.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Extensions;

namespace RepositoryLayer.SeedData
{
    public static class ModelBuilderExtension
    {

        public static void Seed(this ModelBuilder modelBuilder)
        {
            // Seed Data for Admin Roles

            #region Roles

            modelBuilder.Entity<ApplicationRole>().HasData(
               new ApplicationRole
               {
                   Id = (Guid.Parse(UserRolesEnum.AdminSite.DescriptionAttr())),
                   Name = nameof(UserRolesEnum.AdminSite),
                   FaName = (UserRolesEnum.AdminSite.GetDisplayName()),
                   CreatedDatetime = DateTime.Now,
                   NormalizedName = nameof(UserRolesEnum.AdminSite).ToUpper()

               }

            );

            #endregion



        }
    }
}
