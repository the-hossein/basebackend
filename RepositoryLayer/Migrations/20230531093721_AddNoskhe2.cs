﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RepositoryLayer.Migrations
{
    public partial class AddNoskhe2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TblPescription",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDelete = table.Column<bool>(type: "bit", nullable: false),
                    OpgImage = table.Column<int>(type: "int", nullable: true),
                    Userid = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedDatetime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblPescription", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TblPescription_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TblPescription_AspNetUsers_Userid",
                        column: x => x.Userid,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TblPescription_TblUserUploadFile_OpgImage",
                        column: x => x.OpgImage,
                        principalTable: "TblUserUploadFile",
                        principalColumn: "Id");
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "7c7fd86c-657c-46b1-b6b3-4c0f7970088e", new DateTime(2023, 5, 31, 13, 7, 21, 29, DateTimeKind.Local).AddTicks(520) });

            migrationBuilder.CreateIndex(
                name: "IX_TblPescription_CreatorUserId",
                table: "TblPescription",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_TblPescription_OpgImage",
                table: "TblPescription",
                column: "OpgImage");

            migrationBuilder.CreateIndex(
                name: "IX_TblPescription_Userid",
                table: "TblPescription",
                column: "Userid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TblPescription");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "84ee0aab-c23a-4f55-9f52-d9e9fcf71a0d", new DateTime(2023, 5, 31, 12, 54, 40, 738, DateTimeKind.Local).AddTicks(6750) });
        }
    }
}
