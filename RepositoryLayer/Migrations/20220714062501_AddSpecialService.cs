﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RepositoryLayer.Migrations
{
    public partial class AddSpecialService : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "SpecialService",
                table: "TblReservation",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "05e3c089-8943-480e-838a-6677165a192d", new DateTime(2022, 7, 14, 10, 55, 1, 651, DateTimeKind.Local).AddTicks(3347) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SpecialService",
                table: "TblReservation");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "1b8cc9d2-d36b-4536-803f-ded87a930e13", new DateTime(2022, 7, 13, 12, 27, 52, 314, DateTimeKind.Local).AddTicks(8057) });
        }
    }
}
