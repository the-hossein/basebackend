﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RepositoryLayer.Migrations
{
    public partial class AddImages2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TblSample",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TitleEn = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DescriptionEn = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDelete = table.Column<bool>(type: "bit", nullable: false),
                    Image1 = table.Column<int>(type: "int", nullable: true),
                    Image2 = table.Column<int>(type: "int", nullable: true),
                    Image3 = table.Column<int>(type: "int", nullable: true),
                    Image4 = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedDatetime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblSample", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TblSample_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TblSample_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TblSample_TblUserUploadFile_Image1",
                        column: x => x.Image1,
                        principalTable: "TblUserUploadFile",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TblSample_TblUserUploadFile_Image2",
                        column: x => x.Image2,
                        principalTable: "TblUserUploadFile",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TblSample_TblUserUploadFile_Image3",
                        column: x => x.Image3,
                        principalTable: "TblUserUploadFile",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TblSample_TblUserUploadFile_Image4",
                        column: x => x.Image4,
                        principalTable: "TblUserUploadFile",
                        principalColumn: "Id");
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "e24d03a7-539e-4ef8-921b-832173e9190f", new DateTime(2022, 11, 8, 20, 52, 51, 138, DateTimeKind.Local).AddTicks(3342) });

            migrationBuilder.CreateIndex(
                name: "IX_TblSample_CreatorUserId",
                table: "TblSample",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_TblSample_Image1",
                table: "TblSample",
                column: "Image1");

            migrationBuilder.CreateIndex(
                name: "IX_TblSample_Image2",
                table: "TblSample",
                column: "Image2");

            migrationBuilder.CreateIndex(
                name: "IX_TblSample_Image3",
                table: "TblSample",
                column: "Image3");

            migrationBuilder.CreateIndex(
                name: "IX_TblSample_Image4",
                table: "TblSample",
                column: "Image4");

            migrationBuilder.CreateIndex(
                name: "IX_TblSample_UserId",
                table: "TblSample",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TblSample");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "0566b650-28f7-4b12-ae4b-3085598ee3cd", new DateTime(2022, 11, 8, 20, 51, 53, 217, DateTimeKind.Local).AddTicks(7860) });
        }
    }
}
