﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RepositoryLayer.Migrations
{
    public partial class AddCounseling12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TblCounseling",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    RememberPassword = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(11)", maxLength: 11, nullable: true),
                    Service = table.Column<int>(type: "int", nullable: false),
                    CreatedDatetime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblCounseling", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TblCounseling_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "e29b8a4f-a739-489d-a251-8ae7260a5d78", new DateTime(2022, 7, 12, 18, 47, 31, 659, DateTimeKind.Local).AddTicks(4854) });

            migrationBuilder.CreateIndex(
                name: "IX_TblCounseling_CreatorUserId",
                table: "TblCounseling",
                column: "CreatorUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TblCounseling");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "fcae4a9c-aa5b-4341-bfcf-4197031fe339", new DateTime(2022, 7, 6, 13, 2, 22, 224, DateTimeKind.Local).AddTicks(6055) });
        }
    }
}
