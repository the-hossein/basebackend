﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RepositoryLayer.Migrations
{
    public partial class AddNoskhe3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TblPescription_TblUserUploadFile_OpgImage",
                table: "TblPescription");

            migrationBuilder.RenameColumn(
                name: "OpgImage",
                table: "TblPescription",
                newName: "OpgImageId");

            migrationBuilder.RenameIndex(
                name: "IX_TblPescription_OpgImage",
                table: "TblPescription",
                newName: "IX_TblPescription_OpgImageId");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "a57cbb00-5e03-4e3a-a5b7-ccd9fbe49e0d", new DateTime(2023, 5, 31, 13, 14, 35, 290, DateTimeKind.Local).AddTicks(9010) });

            migrationBuilder.AddForeignKey(
                name: "FK_TblPescription_TblUserUploadFile_OpgImageId",
                table: "TblPescription",
                column: "OpgImageId",
                principalTable: "TblUserUploadFile",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TblPescription_TblUserUploadFile_OpgImageId",
                table: "TblPescription");

            migrationBuilder.RenameColumn(
                name: "OpgImageId",
                table: "TblPescription",
                newName: "OpgImage");

            migrationBuilder.RenameIndex(
                name: "IX_TblPescription_OpgImageId",
                table: "TblPescription",
                newName: "IX_TblPescription_OpgImage");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "7c7fd86c-657c-46b1-b6b3-4c0f7970088e", new DateTime(2023, 5, 31, 13, 7, 21, 29, DateTimeKind.Local).AddTicks(520) });

            migrationBuilder.AddForeignKey(
                name: "FK_TblPescription_TblUserUploadFile_OpgImage",
                table: "TblPescription",
                column: "OpgImage",
                principalTable: "TblUserUploadFile",
                principalColumn: "Id");
        }
    }
}
