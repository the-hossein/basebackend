﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RepositoryLayer.Migrations
{
    public partial class AddNoskhe : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "84ee0aab-c23a-4f55-9f52-d9e9fcf71a0d", new DateTime(2023, 5, 31, 12, 54, 40, 738, DateTimeKind.Local).AddTicks(6750) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "bc9803ab-e95c-4350-bcb4-50e84bb20ec9", new DateTime(2022, 11, 8, 22, 4, 12, 523, DateTimeKind.Local).AddTicks(2226) });
        }
    }
}
