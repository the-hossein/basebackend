﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RepositoryLayer.Migrations
{
    public partial class AddFirstTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TblReservation",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Date = table.Column<DateTime>(type: "datetime2", maxLength: 50, nullable: false),
                    Time = table.Column<int>(type: "int", maxLength: 250, nullable: false),
                    Services = table.Column<int>(type: "int", maxLength: 255, nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedDatetime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblReservation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TblReservation_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TblReservation_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TblSms",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    From = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeliverStatus = table.Column<int>(type: "int", nullable: false),
                    Massage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SMSOnlineCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    To = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RecId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SmsType = table.Column<int>(type: "int", nullable: true),
                    KaveNegarResult = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDatetime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblSms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TblSms_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TblOtp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(11)", maxLength: 11, nullable: false),
                    SmsId = table.Column<int>(type: "int", nullable: true),
                    EnterDateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    CreatedDatetime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblOtp", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TblOtp_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TblOtp_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TblOtp_TblSms_SmsId",
                        column: x => x.SmsId,
                        principalTable: "TblSms",
                        principalColumn: "Id");
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "fcae4a9c-aa5b-4341-bfcf-4197031fe339", new DateTime(2022, 7, 6, 13, 2, 22, 224, DateTimeKind.Local).AddTicks(6055) });

            migrationBuilder.CreateIndex(
                name: "IX_TblOtp_CreatorUserId",
                table: "TblOtp",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_TblOtp_SmsId",
                table: "TblOtp",
                column: "SmsId");

            migrationBuilder.CreateIndex(
                name: "IX_TblOtp_UserId",
                table: "TblOtp",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_TblReservation_CreatorUserId",
                table: "TblReservation",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_TblReservation_UserId",
                table: "TblReservation",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_TblSms_CreatorUserId",
                table: "TblSms",
                column: "CreatorUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TblOtp");

            migrationBuilder.DropTable(
                name: "TblReservation");

            migrationBuilder.DropTable(
                name: "TblSms");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "76fb6532-bbf5-4a96-a3f7-d68e046deb20", new DateTime(2022, 4, 28, 10, 27, 8, 285, DateTimeKind.Local).AddTicks(638) });
        }
    }
}
