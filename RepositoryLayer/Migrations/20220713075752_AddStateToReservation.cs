﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RepositoryLayer.Migrations
{
    public partial class AddStateToReservation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "State",
                table: "TblReservation",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "1b8cc9d2-d36b-4536-803f-ded87a930e13", new DateTime(2022, 7, 13, 12, 27, 52, 314, DateTimeKind.Local).AddTicks(8057) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "State",
                table: "TblReservation");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "e29b8a4f-a739-489d-a251-8ae7260a5d78", new DateTime(2022, 7, 12, 18, 47, 31, 659, DateTimeKind.Local).AddTicks(4854) });
        }
    }
}
