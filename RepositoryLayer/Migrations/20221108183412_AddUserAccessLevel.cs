﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RepositoryLayer.Migrations
{
    public partial class AddUserAccessLevel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountNumber",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Capacity",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "KanoonUserCounter",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ShabaNumber",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "UsedCapacity",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<int>(
                name: "UserAccessLevel",
                table: "AspNetUsers",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "bc9803ab-e95c-4350-bcb4-50e84bb20ec9", new DateTime(2022, 11, 8, 22, 4, 12, 523, DateTimeKind.Local).AddTicks(2226) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserAccessLevel",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<string>(
                name: "AccountNumber",
                table: "AspNetUsers",
                type: "nvarchar(30)",
                maxLength: 30,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Capacity",
                table: "AspNetUsers",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "KanoonUserCounter",
                table: "AspNetUsers",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ShabaNumber",
                table: "AspNetUsers",
                type: "nvarchar(30)",
                maxLength: 30,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UsedCapacity",
                table: "AspNetUsers",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("7aef70d1-cd96-4260-93c2-17b8cf2ede58"),
                columns: new[] { "ConcurrencyStamp", "CreatedDatetime" },
                values: new object[] { "e24d03a7-539e-4ef8-921b-832173e9190f", new DateTime(2022, 11, 8, 20, 52, 51, 138, DateTimeKind.Local).AddTicks(3342) });
        }
    }
}
