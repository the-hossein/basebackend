﻿using RepositoryLayer.Contract;
using DataAccessLayer.Entity;

namespace RepositoryLayer.Repositories
{
    public class SampleRepository : RepositoryBase<TblSample>, ISampleRepository
    {
        public SampleRepository(ApplicationContext Context) : base(Context) { }
    }
}
