﻿using RepositoryLayer.Contract;
using DataAccessLayer.Entity;

namespace RepositoryLayer.Repositories
{
    public class ReservationRepository : RepositoryBase<TblReservation>, IReservationRepository
    {
        public ReservationRepository(ApplicationContext Context) : base(Context) { }
    }
}
