﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entity;
using RepositoryLayer.Contract;

namespace RepositoryLayer.Repositories
{
    public class UserRoleRepository : RepositoryBase<ApplicationUserRole>, IUserRoleRepository
    {
        public UserRoleRepository(ApplicationContext Context) : base(Context) { }
    }
}
