﻿using RepositoryLayer.Contract;
using DataAccessLayer.Entity;

namespace RepositoryLayer.Repositories
{
    public class CounselingRepository : RepositoryBase<TblCounseling>, ICounselingRepository
    {
        public CounselingRepository(ApplicationContext Context) : base(Context) { }
    }
}
