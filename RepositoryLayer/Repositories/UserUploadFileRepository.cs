﻿using RepositoryLayer.Contract;
using DataAccessLayer.Entity;

namespace RepositoryLayer.Repositories
{
    public class UserUploadFileRepository : RepositoryBase<TblUserUploadFile>, IUserUploadFileRepository
    {
        public UserUploadFileRepository(ApplicationContext Context) : base(Context) { }
    }
}
