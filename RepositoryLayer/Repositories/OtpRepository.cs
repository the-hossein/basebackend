﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entity;
using RepositoryLayer.Contract;

namespace RepositoryLayer.Repositories
{
    public class OtpRepository : RepositoryBase<TblOtp>, IOtpRepository
    {
        public OtpRepository(ApplicationContext Context) : base(Context) { }
    }
}
