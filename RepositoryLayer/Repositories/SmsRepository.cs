﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entity;
using RepositoryLayer.Contract;

namespace RepositoryLayer.Repositories
{
    public class SmsRepository : RepositoryBase<TblSms>, ISmsRepository
    {
        public SmsRepository(ApplicationContext Context) : base(Context) { }
    }
}
