﻿using RepositoryLayer.Contract;
using DataAccessLayer.Entity;

namespace RepositoryLayer.Repositories
{
    public class PescriptionRepository : RepositoryBase<TblPescription>, IPescriptionRepository
    {
        public PescriptionRepository(ApplicationContext Context) : base(Context) { }
    }
}
