﻿using DataAccessLayer.Entity;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryLayer.Contract
{
    public interface IPescriptionRepository : IRepositoryBase<TblPescription>
    {
    }
}
