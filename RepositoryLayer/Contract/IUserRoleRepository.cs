﻿using DataAccessLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryLayer.Contract
{
    public interface IUserRoleRepository : IRepositoryBase<ApplicationUserRole>
    {
    }
}
