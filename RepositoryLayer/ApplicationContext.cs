﻿using DataAccessLayer.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using DataAccessLayer.ViewModel.StoredProcedures;
using RepositoryLayer.SeedData;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Collections.Generic;

namespace RepositoryLayer
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid, ApplicationUserClaim, ApplicationUserRole, ApplicationUserLogin, ApplicationRoleClaim, ApplicationUserToken>
    {

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {

        }


        public class ApplicationContextDbFactory : IDesignTimeDbContextFactory<ApplicationContext>
        {
            ApplicationContext IDesignTimeDbContextFactory<ApplicationContext>.CreateDbContext(string[] args)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
               .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
               .AddJsonFile("appsettings.json")
               .Build();
                var optionsBuilder = new DbContextOptionsBuilder<ApplicationContext>();
                optionsBuilder.UseLazyLoadingProxies().UseSqlServer<ApplicationContext>(configuration.GetConnectionString("DefaultConnection"));

                return new ApplicationContext(optionsBuilder.Options);
            }
        }




        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Seed

            modelBuilder.Seed();

            #endregion

            #region Entity

            // Use
            modelBuilder.Entity<ApplicationUser>();
            modelBuilder.Entity<ApplicationRole>();
            modelBuilder.Entity<TblUserUploadFile>();
            modelBuilder.Entity<TblReservation>();
            modelBuilder.Entity<TblCounseling>();
            modelBuilder.Entity<TblOtp>();
            modelBuilder.Entity<TblSms>();
            modelBuilder.Entity<TblSample>();
            modelBuilder.Entity<TblPescription>();
            // Un Use

            #endregion

        }






    }
}

