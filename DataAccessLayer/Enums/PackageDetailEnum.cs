﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum PackageDetailEnum
    {

        [Display(Name = "پکیج کتاب جامع")]
        [Description("8D7D1C00-DF91-4D53-BD1F-0DAF2E61F545")]
        KetabJame = 1,
        [Display(Name = "جزوه هوشمند")]
        [Description("5610BAC3-E26D-46B5-A2DB-2945CD5D4694")]
        JozveHooshmand = 2,
        [Display(Name = "وب کارت")]
        [Description("94798823-014B-4478-AF26-55359AEB0F1A")]
        WebCart = 3,
        [Display(Name = "لوح آزمون")]
        [Description("01940481-074F-4AE6-A5C5-1D76F7228947")]
        LohAzmoon = 4,
        [Display(Name = "آزمون تشریحی")]
        [Description("D1C876E6-9B58-459D-98F5-44995AD1A77F")]
        AzmoonTashrihi = 5,
        [Display(Name = "پکیج کتاب مقدماتی")]
        [Description("1BDC9AAC-510B-49A9-B2E2-4FDB14314514")]
        KetabMoghadamati = 6,
        [Display(Name = "پکیج کتاب پایه")]
        [Description("6EA4D7CE-617E-4AF4-B0F5-B18D2A9B88A9")]
        KetabPaye = 7,
    }
}
