﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum PayMentKindEnum
    {
        [Display(Name = "پرداخت کامل و حضوری")]
        [Description("")]
        Kamel_Hozoori = 1,
        [Display(Name = "پرداخت کامل و آنلاین")]
        [Description("")]
        Kamel_online = 2,
        [Display(Name = "پرداخت اقساطی- پیش پرداخت حضوری")]
        [Description("")]
        Check_hozoori = 3,
        [Display(Name = "پرداخت اقساطی - پیش پرداخت آنلاین")]
        [Description("")]
        Check_online = 4,

    }
}
