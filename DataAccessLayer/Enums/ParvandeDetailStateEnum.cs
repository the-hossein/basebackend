﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum ParvandeDetailStateEnum
    {
        Pending = 17,
        Doing = 18,
        Cancel = 19
    }
}
