﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum AzmoonTypeEnum
    {
        [Description("کل")]
        Kol = 1,
        [Description("از مهر ماه")]
        Mehr = 2,
        [Description("نیمسال دوم")]
        NimSal = 3,
        [Description("جمع بندی")]
        JamBandi = 4
    }
}
