﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ResourcesLayer;

namespace DataAccessLayer.Enums
{
    public enum GenderEnum
    {
        [Display(ResourceType = typeof(Resource), Name = "Female")]
        [Description("زن")]
        Female = 0,

        [Display(ResourceType = typeof(Resource), Name = "Male")]
        [Description("مرد")]
        Male = 1,
    }
}
