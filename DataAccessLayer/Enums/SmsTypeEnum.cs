﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum SmsTypeEnum
    {
        [Display(Name = "NormalMessage")]
        NormalMessage = 1,
        [Display(Name = "OtpSend")]
        OtpSend = 2,

    }
}
