﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum NahadChildTypeEnum
    {
        [Display( Name = "درصد")]
        Percent = 1,
        [Display( Name = "عددی")]
        Number = 2,


    }
}
