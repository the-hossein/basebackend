﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum ParvandeCallTypeEnum
    {
        [Display(Name = "تماس موفق")]
        [Description("تماس موفق")]
        Success = 1,
        [Display(Name = "تماس ناموفق")]
        [Description("تماس ناموفق")]
        Fail = 2
    }
}
