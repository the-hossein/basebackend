﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ResourcesLayer;

namespace DataAccessLayer.Enums
{
    public enum TypeTeamEnum
    {
        [Display(ResourceType = typeof(Resource), Name = "Team")]
        Starter = 1,

        [Display(ResourceType = typeof(Resource), Name = "Organizational")]
        Regular = 2,

      

    }
}
