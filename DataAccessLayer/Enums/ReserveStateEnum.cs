﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ResourcesLayer;

namespace DataAccessLayer.Enums
{
    public enum ReserveStateEnum
    {
        [Display(ResourceType = typeof(Resource), Name = "Reserved")]
        Reserved = 0,

        [Display(ResourceType = typeof(Resource), Name = "Disable")]
        Disable = 1,

        [Display(ResourceType = typeof(Resource), Name = "Canceled")]
        Canceled =2,
        
        [Display(ResourceType = typeof(Resource), Name = "Null")]
        Null =3,

        [Display(ResourceType = typeof(Resource), Name = "Expired")]
        Expired = 4,
    }
}
