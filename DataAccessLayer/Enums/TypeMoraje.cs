﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum TypeMoraje
    {
        [Display(Name = "حضوری")]
        Hozori = 0,
        [Display(Name = "غیر حضوری")]
        GheyrHozori = 1,

    }
}
