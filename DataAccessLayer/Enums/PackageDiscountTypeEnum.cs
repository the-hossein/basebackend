﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum PackageDiscountTypeEnum
    {
        [Display(Name = "درصد")]
        Percent = 1,
        [Display(Name = "مبلغ")]
        Amount = 2
    }
}
