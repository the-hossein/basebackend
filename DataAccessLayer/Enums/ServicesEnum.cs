﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ResourcesLayer;

namespace DataAccessLayer.Enums
{
    public enum ServicesEnum
    {

        [Display(ResourceType = typeof(Resource), Name = "Pediatricdentistry")]
        [Description("دندان پزشکی اطفال")]
        Pediatricdentistry = 0,

        [Display(ResourceType = typeof(Resource), Name = "Orthodontics")]
        [Description("ارتودنسی")]
        Orthodontics = 1,

        [Display(ResourceType = typeof(Resource), Name = "Teethbleaching")]
        [Description("بلیچینگ دندان")]
        Teethbleaching = 2,

        [Display(ResourceType = typeof(Resource), Name = "Rootcanal")]
        [Description("عصب کشی دندان")]
        Rootcanal = 3,

        [Display(ResourceType = typeof(Resource), Name = "Prosthesisandcover")]
        [Description("پروتز و روکش دندان")]
        Prosthesisandcover = 4,
        
        [Display(ResourceType = typeof(Resource), Name = "Implant")]
        [Description("ایمپلنت")]
        Implant = 5,
        
        [Display(ResourceType = typeof(Resource), Name = "Gumsurgery")]
        [Description("جراحی لثه")]
        Gumsurgery = 6,  
        
        [Display(ResourceType = typeof(Resource), Name = "Oralsurgery")]
        [Description("جراحی دهان و دندان")]
        Oralsurgery = 7,

        [Display(ResourceType = typeof(Resource), Name = "Gumsurgery")]
        [Description("جراحی فرنکتومی")]
        Frenectomysurgery = 8,

        [Display(ResourceType = typeof(Resource), Name = "Dentallaminate")]
        [Description("لمینت دندان")]
        Dentallaminate = 9,

        [Display(ResourceType = typeof(Resource), Name = "Composite")]
        [Description("کامپوزیت")]
        Composite = 10,




    }
}
