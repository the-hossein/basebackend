﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum ResponseCallLogStatusEnum
    {


        [Display(Name = "source-answer")]
        [Description("")]
        SourceAnswer = 1,
        [Display(Name = "finished")]
        [Description("3cda4ebe-6cfc-4255-b643-dcbc8a7168b3")]
        Finished = 2,
        [Display(Name = "initial")]
        [Description("")]
        Initial = 3,
        [Display(Name = "in-progress")]
        [Description("")]
        InProgress = 4,
        [Display(Name = "destination-ringing")]
        [Description("")]
        DestinationRining = 5,
        [Display(Name = "bridged")]
        [Description("")]
        Bridged = 6,

    }
}
