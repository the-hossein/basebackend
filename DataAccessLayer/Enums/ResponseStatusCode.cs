﻿using System;
using System.Collections.Generic;
using System.Linq;
using ResourcesLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataAccessLayer.Enums
{
    public enum ResponseStatusCode
    {
        [Display(ResourceType = typeof(Resource), Name = "OK")]

        OK = 200,
        [Display(ResourceType = typeof(Resource), Name = "Created")]
        Created = 201,
        [Display(ResourceType = typeof(Resource), Name = "Accepted")]
        Accepted = 202,
        [Display(ResourceType = typeof(Resource), Name = "Non-Authoritative Information")]
        Non_Authoritative_Information = 203,
        [Display(ResourceType = typeof(Resource), Name = "No Content")]
        NoContent = 204,
        [Display(ResourceType = typeof(Resource), Name = "Multiple Choices")]
        MultipleChoices = 300,
        [Display(ResourceType = typeof(Resource), Name = "Moved Permanently")]
        MovedPermanently = 301,
        [Display(ResourceType = typeof(Resource), Name = "Found")]
        Found = 302,
        [Display(ResourceType = typeof(Resource), Name = "Not Modified")]
        NotModifieded = 304,
        [Display(ResourceType = typeof(Resource), Name = "Bad Request")]
        BadRequest = 400,
        [Display(ResourceType = typeof(Resource), Name = "Unauthorized")]
        Unauthorized = 401,
        [Display(ResourceType = typeof(Resource), Name = "Forbidden")]
        Forbidden = 403,
        [Display(ResourceType = typeof(Resource), Name = "Not Found")]
        NotFound = 404,
        [Display(ResourceType = typeof(Resource), Name = "Internal Server Error")]
        InternalServerError = 500,
        [Display(ResourceType = typeof(Resource), Name = "Not Implemented")]
        NotImplemented = 501,
    }
}
