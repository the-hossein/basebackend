﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum TicketPriorityEnum
    {
        [Display(Name = "اضطراری")]
        [Description("")]
        Emergency = 1,
        [Display(Name = "معمولی")]
        [Description("")]
        Normal = 2,

    }
}
