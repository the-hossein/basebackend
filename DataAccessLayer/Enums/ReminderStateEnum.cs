﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum ReminderStateEnum
    {
        [Display(Name = "در انتظار")]
        Pending = 20,
        [Display(Name = "موفق")]
        Done = 21,
        [Display(Name = "ناموفق")]
        Reject = 22
    }
}
