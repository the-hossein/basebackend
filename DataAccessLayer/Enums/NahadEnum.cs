﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum NahadEnum
    {
        [Display(Name = "دانشگاهیان")]
        [Description("5")]
        Daneshgahian = 1,
        [Display(Name = "بانک ها")]
        [Description("4")]
        Bankha = 2,
        [Display(Name = "طرح حکمت")]
        [Description("1")]
        TarheHekmat = 3,
        [Display(Name = "سازمان ها و نهاد ها")]
        [Description("2")]
        SazmanhaVaNahadHA = 4,
        [Display(Name = "موسسات درمانی")]
        [Description("7")]
        MoassesateDarmani = 5,
        [Display(Name = "فرهنگیان")]
        [Description("3")]
        Farhangian = 6,
        [Display(Name = "کتابخانه ها")]
        [Description("6")]
        KetabKhaneha = 7,
        
    }
}
