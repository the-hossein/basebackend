﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum ParvandeCallDetailStateEnum
    {
        Cancel = 23,
        DarHalPeygiri = 24,
        SohbatNashode = 27
    }
}
