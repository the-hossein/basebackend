﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum DuplicateParvandeSystemReasonEnum
    {
        [Description("کد ملی")]
        NationalCode = 1,
        [Description("موبایل - موبایل")]
        MobileMobile = 2,
        [Description("رابط 1 - موبایل")]
        MobileCommTel1 = 3,
        [Description("رابط 2 - موبایل")]
        MobileCommTel2 = 4,
        [Description("موبایل - رابط 1")]
        CommTel1Mobile = 5,
        [Description("رابط 1 - رابط 1")]
        CommTel1CommTel1 = 6,
        [Description("رابط 2 - رابط 1")]
        CommTel1CommTel2 = 7,
        [Description("موبایل - رابط 2")]
        CommTel2Mobile = 8,
        [Description("رابط 1 - رابط 2")]
        CommTel2CommTel1 = 9,
        [Description("رابط 2 - رابط 2")]
        CommTel2CommTel2 = 10,
        [Description("تلفن")]
        Tel = 11
    }
}
