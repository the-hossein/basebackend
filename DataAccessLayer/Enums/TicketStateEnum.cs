﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum TicketStateEnum
    {
        [Display(Name = "در حال بررسی")]
        [Description("16557d99-cd07-460c-9682-c73998d8e61b")]
        DarhalBaresi = 1,
        [Display(Name = "پاسخ داده شده")]
        [Description("32557d99-cd07-460c-9682-c73998d8e61b")]
        PasokhDadeShod = 2,
        [Display(Name = "بسته شده")]
        [Description("12557d99-cd07-460c-9682-c73998d8e61b")]
        BasteShode = 3,
    }
}
