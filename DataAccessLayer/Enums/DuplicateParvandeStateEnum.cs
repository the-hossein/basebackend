﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum DuplicateParvandeStateEnum
    {
        Pending = 14,
        Accept = 15,
        Reject = 16
    }
}
