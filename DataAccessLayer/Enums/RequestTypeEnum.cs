﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum RequestTypeEnum
    {
        Azmoon = 1,
        MoshavereKetab = 2,
        Tadris = 3,
        MoshavereTahsili = 4,
        PoshtibanVije = 5,
        Shekayat = 6,
        Boorsie = 7
    }
}
