﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum SpecialUsersEnum
    {
        [Display(Name = "Student")]
        [Description("F1074540-2BAD-4694-A8D0-08D9B8930CD1")]
        Student = 1
    }
}
