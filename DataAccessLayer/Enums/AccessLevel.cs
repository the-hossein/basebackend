﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ResourcesLayer;

namespace DataAccessLayer.Enums
{
    public enum AccessLevel
    {
        [Display(ResourceType = typeof(Resource), Name = "Manager")]
        Manager = 1,

        [Display(ResourceType = typeof(Resource), Name = "Administrator")]
        Administrator = 2,

        [Display(ResourceType = typeof(Resource), Name = "Personnel")]
        Personnel = 3,

        [Display(ResourceType = typeof(Resource), Name = "User")]
        User = 4,
        
        [Display(ResourceType = typeof(Resource), Name = "Admin")]
        Admin = 5,

    }
}
