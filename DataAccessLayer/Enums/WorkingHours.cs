﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ResourcesLayer;

namespace DataAccessLayer.Enums
{
    public enum WorkingHouseEnum
    {
        [Display(ResourceType = typeof(Resource), Name = "Specifiedworkinghours")]
        Specifiedworkinghours = 1,

        [Display(ResourceType = typeof(Resource), Name = "Overtime")]
        Overtime = 2,


    }
}
