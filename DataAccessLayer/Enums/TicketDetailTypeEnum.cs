﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum TicketDetailTypeEnum
    {
        [Display(Name = "Incoming")]
        Incoming = 1,
        [Display(Name = "Outgoing")]
        Outgoing = 2,
    }
}
