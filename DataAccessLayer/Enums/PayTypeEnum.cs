﻿using System.ComponentModel;

namespace DataAccessLayer.Enums
{
    public enum PayTypeEnum
    {
        [Description("کامل حضوری")]
        KamelHozoori = 1,
        [Description("کامل غیرحضوری")]
        KamelGheyrHozoori = 2,
        [Description("چکی با پیش پرداخت حضوری")]
        CheckHozoori = 3,
        [Description("چکی با پیش پرداخت غیرحضوری")]
        CheckGheyrHozoori = 4,
        [Description("اقساطی با پیش پرداخت حضوری")]
        GhestHozoori = 5,
        [Description("اقساطی با پیش پرداخت غیرحضوری")]
        GhestGheyrHozoori = 6
    }
}
