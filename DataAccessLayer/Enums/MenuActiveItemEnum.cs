﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum MenuActiveItemEnum
    {
        //Dashboard = 1,
        DashboardIndex = 2,
        DashboardDuplicate = 3,
        DashboardConflict = 4,
        PayamIndexMe = 5,
        // Peygir
        PeygirIndexMe = 60,
        PeygirCancelList = 61,
        PeygirEnrolList = 62,
        PeygirPackageList = 63,
        PeygirExtraIndex = 64,
        PeygirExtraCancelList = 65,
        PeygirExtraPackageList = 66,

        ParvandeCreate = 7,
        ParvandeDuplicate = 8,
        SearchStudent = 9,
        Basket = 10,
        Location = 11,
        ParvandeLog = 12,
        BookIndex = 13,
        BookCD = 14,
        BookStyleDVD = 15,
        BookStyleBook = 16,
        PackageDiscount = 17,
        Project = 18,

        // Admin
        AdminAzmoon = 90,
        AdminUser = 91,

        // KarshenasForoshKetab
        KarshenasForoshKetabIndex = 1000,
        KarshenasForoshKetabInvoice = 1001,
        // --KarshenasForoshKetab

        // KarshenasSefareshKetab
        KarshenasSefareshKetabIndex = 1100,
        KarshenasSefareshKetabConfirmed = 1101,
        KarshenasSefareshKetabIndexReject = 1102,
        KarshenasSefareshKetabIndexPayed = 1103,
        // --KarshenasSefareshKetab


        // KarshenasMaliKetab
        KarshenasMaliKetabIndex = 1200,
        KarshenasMaliKetabConfirmed = 1201,
        KarshenasMaliKetabRejected = 1202,
        KarshenasMaliKetabVarizi = 1203,
        // --KarshenasMaliKetab

    }
}
