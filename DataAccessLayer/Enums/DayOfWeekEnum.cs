﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResourcesLayer;

namespace DataAccessLayer.Enums
{
    public enum DayOfWeekEnum
    {
        [Display(ResourceType = typeof(Resource), Name = "Sunday")]
        Sunday = 0,
        [Display(ResourceType = typeof(Resource), Name = "Monday")]
        Monday = 1,
        [Display(ResourceType = typeof(Resource), Name = "Tuesday")]
        Tuesday = 2,
        [Display(ResourceType = typeof(Resource), Name = "Wednesday")]
        Wednesday = 3,
        [Display(ResourceType = typeof(Resource), Name = "Thursday")]
        Thursday = 4,
        [Display(ResourceType = typeof(Resource), Name = "Friday")]
        Friday = 5,
        [Display(ResourceType = typeof(Resource), Name = "Saturday")]
        Saturday = 6,
    }
}
