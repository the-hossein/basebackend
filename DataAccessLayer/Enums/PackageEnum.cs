﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum PackageEnum
    {

        [Display(Name = "جزوه هوشمند")]
        [Description("8429B785-0282-4A78-B98F-0B3CEA093EAA")]
        JozveHooshmand = 1,
        [Display(Name = "وب کارت")]
        [Description("1CF9A40F-72CE-4BE5-A1D5-4E049267EF7C")]
        WebCart = 2,
        [Display(Name = "آژمون")]
        [Description("B26E80C3-CFFA-48F6-B4C5-6321FFBC9A82")]
        Azmoon = 3,
        [Display(Name = "لوح آزمون")]
        [Description("0619BE0A-4129-4F84-9463-A31C89A03D75")]
        LohAzmoon = 4,
        [Display(Name = "آزمون تشریحی")]
        [Description("34EFEC9B-442E-4AA2-A19E-A3FDB9333C11")]
        AzmoonTashrihi = 5,
        [Display(Name = "کتاب")]
        [Description("FA2E3532-6D27-48DF-A40A-AAA7CF2C577A")]
        Ketab = 6,
    }
}
