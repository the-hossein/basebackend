﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ResourcesLayer;

namespace DataAccessLayer.Enums
{
    public enum EmploymentStateEnum
    {
        [Display(ResourceType = typeof(Resource), Name = "Accepted")]
        Accepted = 1,

        [Display(ResourceType = typeof(Resource), Name = "Faild")]
        Faild = 2,

        [Display(ResourceType = typeof(Resource), Name = "IntTheReviewQueue")]
        IntTheReviewQueue = 3,
    }
}
