﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum ParvandeCallFollowStatusEnum
    {
        [Display(Name = "پیگیری مجدد")]
        [Description("پیگیری مجدد")]
        Follow = 1,
        [Display(Name = "کنسلی")]
        [Description("کنسلی")]
        Cancel = 2
    }
}
