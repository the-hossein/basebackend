﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ResourcesLayer;

namespace DataAccessLayer.Enums
{
    public enum RequestType
    {
        [Display(ResourceType = typeof(Resource), Name = "Recruitment")]
        [Description("استخدام")]
        Recruitment = 0,

        [Display(ResourceType = typeof(Resource), Name = "Counseling")]
        [Description("مشاوره")]
        Counseling = 1,


        [Display(ResourceType = typeof(Resource), Name = "DayOff")]
        [Description("مرخصی")]
        DayOff =2,

        [Display(ResourceType = typeof(Resource), Name = "AdvanceMoney")]
        [Description("مساعده")]
        AdvanceMoney = 3,
    }
}
