﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum SabtKindRegisterKanoonEnum
    {
        [Display(Name = "Normal")] 
        Normal = 1,
        [Display(Name = "BonyadShahid")]
        BonyadShahid = 2,
        [Display(Name = "Boorsie")]
        Boorsie = 3,
        [Display(Name = "Hekmat")]
        Hekmat = 4,
    }
}
