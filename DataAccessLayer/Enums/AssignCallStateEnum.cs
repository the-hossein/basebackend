﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum AssignCallStateEnum
    {
        [Display(Name = "در حال پیگیری")]
        Pending = 30,
        [Display(Name = "پاسخ نداد")]
        NoAnswer = 31,
        [Display(Name = "خاموش بود")]
        Off = 32,
        [Display(Name = "نمیخواد")]
        NoNeed = 33,
        [Display(Name = "نمیخواد - احتمال خرید دازد")]
        MayBuy = 34,
        [Display(Name = " نمیخواد - مالی")]
        NoMoney = 35,
        [Display(Name = "موفق")]
        Success = 36
    }
}
