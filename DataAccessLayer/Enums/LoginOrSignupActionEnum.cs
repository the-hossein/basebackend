﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResourcesLayer;

namespace DataAccessLayer.Enums
{
    public enum LoginOrSignupActionEnum
    {
        [Display(ResourceType = typeof(Resource), Name = "CheckPhoneNumberLogin")]
        CheckPhoneNumberLogin = 1,
        [Display(ResourceType = typeof(Resource), Name = "CheckPhoneNumberSignUp")]
        CheckPhoneNumberSignUp = 5,
        [Display(ResourceType = typeof(Resource), Name = "CheckOtp")]
        CheckOtp = 2,
        [Display(ResourceType = typeof(Resource), Name = "LoginWithPassword")]
        LoginWithPassword = 3,
        [Display(ResourceType = typeof(Resource), Name = "Signup")]
        Signup = 4,

    }
}
