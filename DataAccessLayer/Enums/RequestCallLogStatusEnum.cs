﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum RequestCallLogStatusEnum
    {
        [Display(Name = "Start_Request")]
        [Description("")]
        StartRequest = 1,
        [Display(Name = "finished")]
        [Description("")]
        Finished = 2,


    }
}
