﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum ParvandeLogTypeEnum
    {
        EditPersonalData = 1,
        EditGrade = 2,
        EditGender = 3,
        EditLocation = 4
    }
}
