﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Enums
{
    public enum CyberPasswordEncription : byte
    {
        [Description("CAP32Solve")]
        [Display(Name = "Random")]
        Encryption = 1,
    }
}
