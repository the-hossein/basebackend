﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResourcesLayer;

namespace DataAccessLayer.Enums
{
    public enum UserRolesEnum
    {
        #region roles

        [Display(ResourceType = typeof(Resource), Name = "مدیر سایت")]
        [Description("7AEF70D1-CD96-4260-93C2-17B8CF2EDE58")]
        AdminSite = 1,

        [Display(ResourceType = typeof(Resource), Name = "کاربر معمولی")]
        [Description("12EF30D1-CD96-4260-93C2-17B8CF2EDE58")]
        Normal = 2,


        #endregion


    }
}
