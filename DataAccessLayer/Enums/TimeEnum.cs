﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ResourcesLayer;

namespace DataAccessLayer.Enums
{
    public enum TimeEnum
    {
        [Display(ResourceType = typeof(Resource), Name = "NineAm")]
        NineAm = 0,

        [Display(ResourceType = typeof(Resource), Name = "TenAm")]
        TenAm = 1,

        [Display(ResourceType = typeof(Resource), Name = "ElevenAm")]
        ElevenAm = 2,

        [Display(ResourceType = typeof(Resource), Name = "twelveAm")]
        twelveAm = 3,
        
        [Display(ResourceType = typeof(Resource), Name = "OnePm")]
        OnePm = 4,

        [Display(ResourceType = typeof(Resource), Name = "TwoPm")]
        TwoPm = 5,

        [Display(ResourceType = typeof(Resource), Name = "ThreePm")]
        ThreePm = 6,

        [Display(ResourceType = typeof(Resource), Name = "FourPm")]
        FourPm = 7,

        [Display(ResourceType = typeof(Resource), Name = "FivePm")]
        FivePm = 8,

        [Display(ResourceType = typeof(Resource), Name = "SixPm")]
        SixPm = 9,

        [Display(ResourceType = typeof(Resource), Name = "SevenPm")]
        SevenPm = 10,

        [Display(ResourceType = typeof(Resource), Name = "EightPm")]
        EightPm = 11,

        [Display(ResourceType = typeof(Resource), Name = "NinePm")]
        NinePm = 12,

    }
}
