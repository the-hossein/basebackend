﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum ParvandeLogStateEnum
    {
        Pending = 20,
        Accept = 21,
        Reject = 22
    }
}
