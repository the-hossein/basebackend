﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum ZarinpalPayTypeEnum
    {
        [Display(Name = "Zarinpal")]
        Zarinpal = 1,
        [Display(Name = "Sandbox")]
        Sandbox = 2,
    }
}
