﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum ZarinPalMerchantId
    {
        [Display(Name = "KanoonHesab")]
        //[Description("17bea21f-6ceb-4392-9b2b-794ade967ce7")]
        [Description("0e7e2150-31eb-43fd-b9d0-9657242c38ef")]
        KanoonHesab = 0,
    }
}
