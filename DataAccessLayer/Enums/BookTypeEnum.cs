﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum BookTypeEnum
    {
        [Description("کتاب")]
        Book = 1,
        [Description("لوح")]
        DVD = 2
    }
}
