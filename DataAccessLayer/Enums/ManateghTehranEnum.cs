﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ResourcesLayer;

namespace DataAccessLayer.Enums
{
    public enum ManateghTehranEnum
    {
        [Display( Name = "تهران شمال")]
        Shomal = 1,
        [Display( Name = "تهران جنوب")]
        Jonub = 2,
        [Display( Name = "تهران شرق")]
        Shargh = 3,
        [Display( Name = "تهران غرب")]
        Gharb = 4,
       
    }
}
