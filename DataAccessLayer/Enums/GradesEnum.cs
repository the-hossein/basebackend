﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ResourcesLayer;

namespace DataAccessLayer.Enums
{
    public enum GradesEnum
    {
        [Display(ResourceType = typeof(Resource), Name = "Diploma")]
        Diploma = 1,
        [Display(ResourceType = typeof(Resource), Name = "AssociateDegree")]
        AssociateDegree = 2,
        [Display(ResourceType = typeof(Resource), Name = "BachelorDegree")]
        BachelorDegree = 3,
        [Display(ResourceType = typeof(Resource), Name = "MasterDegree")]
        MasterDegree = 4,
        [Display(ResourceType = typeof(Resource), Name = "Other")]
        Other = 5,
    }
}
