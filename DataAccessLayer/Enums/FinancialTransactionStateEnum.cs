﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum FinancialTransactionStateEnum
    {
        Pending = 37,
        RejectKarshenasMali = 38, 
        PendingBank = 39,
        AcceptBank = 40,
        RejectBank = 41,
        Paid = 42
    }
}
