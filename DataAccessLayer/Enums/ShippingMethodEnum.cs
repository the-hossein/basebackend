﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum ShippingMethodEnum
    {
        [Display(Name = "پست")]
        [Description("150000")]
        Post = 1,
        [Display(Name = "پیک")]
        [Description("300000")]
        Peyk = 2,
        [Display(Name ="تحویل توسط نمایندگی")]
        [Description("0")]
        TahvilNamayandegi = 3
    }
}
