﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer.Validation
{
    public class NationalCodeValidation : ValidationAttribute
    {
        public NationalCodeValidation(string nationalcode)
        {
            NationalCode = nationalcode;
        }

        public string NationalCode { get; }

        public string GetErrorMessage() =>
            $"کد ملی وارد شده اشتباه میباشد";

        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {

            if (!Helpers.IsValidNationalCode(value.ToString()))
            {
                return new ValidationResult(GetErrorMessage());
            }

            return ValidationResult.Success;
        }
    }
}
