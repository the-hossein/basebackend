﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DataAccessLayer.Validation
{
    public static class Helpers
    {
        

        public static string FixShamsiBirthDay(string BirthDay)
        {
            try
            {
                if (BirthDay.Length == 10)
                    return BirthDay;


                string[] BirthDaySpilit = BirthDay.Split('/');


                string Year = BirthDaySpilit[0];
                string Month = BirthDaySpilit[1];
                string Day = BirthDaySpilit[2];

                if (Month.Length == 1)
                    Month = "0" + Month;

                if (Day.Length == 1)
                    Day = "0" + Day;



                return Year + "/" + Month + "/" + Day;


            }
            catch (Exception)
            {

                return BirthDay;
            }

        }

        public static bool IsValidBirthDate(this string Birth)
        {
            if (Regex.IsMatch(Birth, @"(?:1[23]\d{2})(\/|\-)(?:0?[1-9]|1[0-2])(\/|\-)(?:0?[1-9]|[12][0-9]|3[01])$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool IsValidMiladiBirthDate(this string Birth)
        {
            Birth = Birth.Replace("/", "-");
            if (Regex.IsMatch(Birth, @"([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsValidPostCode(this string PostCode)
        {
            if (String.IsNullOrEmpty(PostCode))
                return false;

            var allDigitEqual = new[]
               {
                "1111111111", "6511111111", "8111111111", "0000000000", "3111111111", "7911111111", "8888888888",
                "7777777777", "111111111", "0111111111" , "8999999999" , "1234567891", "1234567890", "9411111111", "1288899999", "1222222222"
            };

            if (allDigitEqual.Contains(PostCode)) return false;

            if (PostCode.Length != 10)
                return false;

            Regex regex = new Regex("^[0-9]+$");

            if (regex.IsMatch(PostCode))
            {
                return true;
            }


            return false;
        }


        /// <summary>
        ///     تعیین معتبر بودن کد ملی
        /// </summary>
        /// <param name="nationalCode">کد ملی وارد شده</param>
        /// <returns>
        ///     در صورتی که کد ملی صحیح باشد خروجی <c>true</c> و در صورتی که کد ملی اشتباه باشد خروجی <c>false</c> خواهد بود
        /// </returns>
        /// <exception cref="System.Exception"></exception>
        public static bool IsValidNationalCode(this string nationalCode)
        {
            try
            {
                if (!Regex.IsMatch(nationalCode, @"^\d{10}$"))
                    return false;

                var check = Convert.ToInt32(nationalCode.Substring(9, 1));
                var sum = Enumerable.Range(0, 9)
                    .Select(x => Convert.ToInt32(nationalCode.Substring(x, 1)) * (10 - x))
                    .Sum() % 11;

                return (sum < 2 && check == sum) || (sum >= 2 && check + sum == 11);
            }
            catch (Exception)
            {

                return false;
            }

        }




        public static bool IsValidFullname(string Fullname)
        {
            //input has 11 digits that all of them are not equal
            if (!Regex.IsMatch(Fullname, @"^[\u0600-\u06FF\.\'\-]{2,50}(?: [\u0600-\u06FF\.\'\-]{2,50})+$"))
                return false;



            return true;
        }



      

      


        public static bool IsValidPhoneNumber(string input)
        {
            if (input.Length != 11)
                return false;
            input = input.Trim();

            Regex regex1 = new Regex(@"09(\d{2})(" + input.Substring(4, 1) + @"{7})");
            Regex regex2 = new Regex(@"09(\d{2})(1234567)");
            Regex regex3 = new Regex(@"09(\d{2})(7654321)");

            // Step 2: call Match on Regex instance.
            Match match1 = regex1.Match(input);
            Match match2 = regex2.Match(input);
            Match match3 = regex3.Match(input);
            // Step 3: test for Success.

            if (!String.IsNullOrWhiteSpace(input) && input.Length == 11 && !match1.Success && !match2.Success && !match3.Success)
            {

                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool IsNumber(string input)
        {
            var match = Regex.Match(input, @"^\d+$", RegexOptions.IgnoreCase);

            if (!match.Success)
            {
                // does not match
                return false;
            }
            return true;
        }



    }
}
