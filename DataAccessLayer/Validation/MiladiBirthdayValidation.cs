﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer.Validation
{
    public class MiladiBirthdayValidation : ValidationAttribute
    {
        public MiladiBirthdayValidation(string birthday)
        {
            Birthday = birthday;
        }

        public string Birthday { get; }

        public string GetErrorMessage() =>
            $"تاریخ تولد وارد شده اشتباه میباشد";

        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {

            if (!Helpers.IsValidMiladiBirthDate(value.ToString()))
            {
                return new ValidationResult(GetErrorMessage());
            }

            return ValidationResult.Success;
        }
    }
}
