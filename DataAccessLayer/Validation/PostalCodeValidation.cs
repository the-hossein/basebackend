﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer.Validation
{
    public class PostalCodeValidation : ValidationAttribute
    {
        public PostalCodeValidation(string postalcode)
        {
            PostalCode = postalcode;
        }

        public string PostalCode { get; }

        public string GetErrorMessage() =>
            $"کد پستی وارد شده اشتباه میباشد";

        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {

            if (!Helpers.IsValidPostCode(value.ToString()))
            {
                return new ValidationResult(GetErrorMessage());
            }

            return ValidationResult.Success;
        }
    }
}
