﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer.Validation
{
    public class PhonenumberValidation : ValidationAttribute
    {
        public PhonenumberValidation(string phonenumber)
        {
            Phonenumber = phonenumber;
        }

        public string Phonenumber { get; }

        public string GetErrorMessage() =>
            $"شماره همراه وارد شده اشتباه میباشد";

        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {

            if (!Helpers.IsValidPhoneNumber(value.ToString()))
            {
                return new ValidationResult(GetErrorMessage());
            }

            return ValidationResult.Success;
        }
    }
}
