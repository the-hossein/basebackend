﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer.Validation
{
    public class BirthdayValidation : ValidationAttribute
    {
        public BirthdayValidation(string birthday)
        {
            Birthday = birthday;
        }

        public string Birthday { get; }

        public string GetErrorMessage() =>
            $"تاریخ تولد وارد شده اشتباه میباشد";

        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {

            if (!Helpers.IsValidBirthDate(value.ToString()))
            {
                return new ValidationResult(GetErrorMessage());
            }

            return ValidationResult.Success;
        }
    }
}
