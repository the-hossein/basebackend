﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ResourcesLayer;


namespace DataAccessLayer.Entity
{
    public class BaseModel
    {
        public BaseModel()
        {


            CreatedDatetime = DateTime.Now;
        }

        [JsonProperty(PropertyName = "id")]
        [Key]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "createddatetime")]
        [DefaultValue(nameof(DateTime.Now))]
        public DateTime CreatedDatetime { get; set; }


        [JsonProperty(PropertyName = "creatoruserid")]
        [ForeignKey(nameof(UserCreator))]
        public Guid? CreatorUserId { get; set; }

        //[JsonProperty(PropertyName = "modifieduserid")]
        //[ForeignKey(nameof(UserModify))]
        //public Guid? ModifiedUserId { get; set; }

        //[JsonProperty(PropertyName = "deactiveuserid")]
        //[ForeignKey(nameof(UserDeactive))]
        //public Guid? DeactiveUserId { get; set; }


        #region  Relations

        public virtual ApplicationUser UserCreator { get; set; }

        //public virtual ApplicationUser UserModify { get; set; }

        //public virtual ApplicationUser UserDeactive { get; set; }


        #endregion

        #region ICollection



        #endregion

    }
}
