﻿using DataAccessLayer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entity
{
    public partial class TblSms : BaseModel
    {
        public TblSms()
        {

        }
        public Guid? UserId { get; set; }
        public string From { get; set; }
        public int DeliverStatus { get; set; }
        public string Massage { get; set; }
        public string SMSOnlineCode { get; set; }
        public string To { get; set; }
        public string RecId { get; set; }

        public SmsTypeEnum? SmsType { get; set; }

        public string KaveNegarResult { get; set; }
    }
}
