using DataAccessLayer.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text; 

namespace DataAccessLayer.Entity
{
    public partial class TblReservation : BaseModel
    {
        #region Constractor

        public TblReservation()
        {

        }

        #endregion

        #region Fields

        [StringLength(50)]
        [JsonProperty(PropertyName = "date")]
        public DateTime Date { get; set; }

        [DataType(DataType.MultilineText),StringLength(250)]
        [JsonProperty(PropertyName = "time")]
        public TimeEnum Time { get; set; }

        [StringLength(255), Required]
        [JsonProperty(PropertyName = "service")]
        public ServicesEnum Services { get; set; }

        [ForeignKey(nameof(User))]
        [JsonProperty(PropertyName = "userid")]
        public Guid? UserId { get; set; }


        [JsonProperty(PropertyName = "state")]
        public ReserveStateEnum State { get; set; }


        [JsonProperty(PropertyName = "specialservice")]
        public bool SpecialService { get; set; }

        #endregion

        #region Relations

        public virtual ApplicationUser User { get; set; }

        #endregion


        #region ICollections



        #endregion


    }
}
