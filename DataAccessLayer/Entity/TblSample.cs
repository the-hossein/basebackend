using DataAccessLayer.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text; 

namespace DataAccessLayer.Entity
{
    public partial class TblSample : BaseModel
    {
        #region Constractor

        public TblSample()
        {

        }

        #endregion

        #region Fields

        #region Constractor

        #endregion
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "titleen")]
        public string TitleEn { get; set; }
    
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "descriptionen")]
        public string DescriptionEn { get; set; }

        [JsonProperty(PropertyName = "isdelete")]
        public bool IsDelete { get; set; }

        [ForeignKey(nameof(ImageFile1))]
        [JsonProperty(PropertyName = "image1")]
        public int? Image1 { get; set; }

        [ForeignKey(nameof(ImageFile2))]
        [JsonProperty(PropertyName = "image2")]
        public int? Image2 { get; set; }

        [ForeignKey(nameof(ImageFile3))]
        [JsonProperty(PropertyName = "image3")]
        public int? Image3 { get; set; }


        [ForeignKey(nameof(ImageFile4))]
        [JsonProperty(PropertyName = "image4")]
        public int? Image4 { get; set; }



       
        public virtual TblUserUploadFile ImageFile1 { get; set; }
        public virtual TblUserUploadFile ImageFile2 { get; set; }
        public virtual TblUserUploadFile ImageFile3 { get; set; }
        public virtual TblUserUploadFile ImageFile4 { get; set; }
       

        #endregion

        #region Relations

        public virtual ApplicationUser User { get; set; }

        #endregion


        #region ICollections



        #endregion


    }
}
