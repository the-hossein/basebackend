using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text; 

namespace DataAccessLayer.Entity
{
    public partial class TblUserUploadFile : BaseModel
    {
        #region Constractor

        public TblUserUploadFile()
        {

        }

        #endregion

        #region Fields

        [StringLength(50)]
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [DataType(DataType.MultilineText),StringLength(250)]
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [StringLength(255), Required]
        [JsonProperty(PropertyName = "filepath")]
        public string FilePath { get; set; }

        [ForeignKey(nameof(User))]
        [JsonProperty(PropertyName = "userid")]
        public Guid? UserId { get; set; }

        public bool SaveSuccess { get; set; }


        [Required]
        [StringLength(250)]
        public string Filename { get; set; }


        #endregion

        #region Relations

        public virtual ApplicationUser User { get; set; }

        #endregion


        #region ICollections



        #endregion


    }
}
