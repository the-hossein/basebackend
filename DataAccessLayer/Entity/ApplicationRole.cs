﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace DataAccessLayer.Entity
{
    public partial class ApplicationRole : IdentityRole<Guid>
    {


        [JsonProperty(PropertyName = "faname")]
        [Required, StringLength(50)]
        public string FaName { get; set; }

        [JsonProperty(PropertyName = "createddatetime")]
        [DefaultValue(nameof(DateTime.Now))]
        public DateTime CreatedDatetime { get; set; }

    }
}
