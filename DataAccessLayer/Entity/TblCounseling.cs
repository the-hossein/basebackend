using DataAccessLayer.Enums;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ResourcesLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text; 

namespace DataAccessLayer.Entity
{
    public partial class TblCounseling : BaseModel
    {
        #region Constractor

        public TblCounseling()
        {

        }

        #endregion

        #region Fields

        [StringLength(50)]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        public string RememberPassword { get; set; }
        [JsonProperty(PropertyName = "phonenumber")]
        [Display(ResourceType = typeof(Resource), Name = "PhoneNumber")]
        [StringLength(11, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 11)]
        [DataType(DataType.Text)]
        [MinLength(11)]
        [Remote(action: "VerifyPhoneNumber", controller: "Validator", AdditionalFields = nameof(PhoneNumber))]
        public string PhoneNumber { get; set; }

       

       
        [JsonProperty(PropertyName = "service")]
        public ServicesEnum Service { get; set; }



        #endregion

        #region Relations

        #endregion


        #region ICollections



        #endregion


    }
}
