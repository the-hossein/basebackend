﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using DataAccessLayer.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ResourcesLayer;

namespace DataAccessLayer.Entity
{
    public partial class ApplicationUser : IdentityUser<Guid>
    {

        public ApplicationUser()
        {
            this.CreatedDatetime = DateTime.UtcNow;
        }

        #region BaseModel

        [JsonProperty(PropertyName = "deactivedatetime")]
        public DateTime? DeactiveDateTime { get; set; }

        [JsonProperty(PropertyName = "createddatetime")]
        [DefaultValue(nameof(DateTime.Now))]
        public DateTime CreatedDatetime { get; set; }

        [JsonProperty(PropertyName = "modifieddatetime")]
        public DateTime? ModifiedDatetime { get; set; }



        [JsonProperty(PropertyName = "creatoruserid")]
        [ForeignKey(nameof(CreatorUser))]
        public Guid? CreatorUserId { get; set; }

        [JsonProperty(PropertyName = "modifieduserid")]

        public Guid? ModifiedUserId { get; set; }

        [JsonProperty(PropertyName = "deactiveuserid")]

        public Guid? DeactiveUserId { get; set; }



        #region  Relations

        public virtual ApplicationUser CreatorUser { get; set; }


        #endregion

        #endregion

       

        [JsonProperty(PropertyName = "name")]
        [Display(ResourceType = typeof(Resource), Name = "Name")]
        [StringLength(50)]
        [DataType(DataType.Text)]
        public string Name { get; set; }


        [JsonProperty(PropertyName = "family")]
        [Display(ResourceType = typeof(Resource), Name = "Family")]
        [StringLength(100)]
        [DataType(DataType.Text)]
        public string Family { get; set; }

        public string RememberPassword { get; set; }
        [JsonProperty(PropertyName = "phonenumber")]
        [Display(ResourceType = typeof(Resource), Name = "PhoneNumber")]
        [StringLength(11, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 11)]
        [DataType(DataType.Text)]
        [MinLength(11)]
        [Remote(action: "VerifyPhoneNumber", controller: "Validator", AdditionalFields = nameof(PhoneNumber))]
        public string PhoneNumber { get; set; }

        [JsonProperty(PropertyName = "nationalcode")]
        [StringLength(11, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 10)]
        [Display(ResourceType = typeof(Resource), Name = "NationalCode")]
        [DataType(DataType.Text)]
        [Remote(action: "VerifyNationalCode", controller: "Validator", AdditionalFields = nameof(NationalCode))]
        public string NationalCode { get; set; }


        [JsonProperty(PropertyName = "gender")]
        [Display(ResourceType = typeof(Resource), Name = "Gender")]
        public GenderEnum Gender { get; set; }


        [JsonProperty(PropertyName = "postalcode")]
        [StringLength(10, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 10)]
        [Display(ResourceType = typeof(Resource), Name = "PostalCode")]
        [DataType(DataType.Text)]
        public string PostalCode { get; set; }


        [JsonProperty(PropertyName = "tel")]
        [Display(ResourceType = typeof(Resource), Name = "Tel")]
        [StringLength(20)]
        [DataType(DataType.Text)]
        public string Tel { get; set; }


        [JsonProperty(PropertyName = "commtel1")]
        [Display(ResourceType = typeof(Resource), Name = "Commtel1")]
        [StringLength(20)]
        [DataType(DataType.Text)]
        public string CommTel1 { get; set; }


        [JsonProperty(PropertyName = "commtel2")]
        [Display(ResourceType = typeof(Resource), Name = "Commtel2")]
        [StringLength(20)]
        [DataType(DataType.Text)]
        public string CommTel2 { get; set; }

        [JsonProperty(PropertyName = "email")]
        [Display(ResourceType = typeof(Resource), Name = "Email"), StringLength(255)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }



        [JsonProperty(PropertyName = "address")]
        [Display(ResourceType = typeof(Resource), Name = "Address")]
        [DataType(DataType.MultilineText), StringLength(1000)]
        public string Address { get; set; }


        [JsonProperty(PropertyName = "rate")]
        [Display(ResourceType = typeof(Resource), Name = "Rate")]
        public decimal Rate { get; set; }


        [JsonProperty(PropertyName = "profilepic")]
        [Display(ResourceType = typeof(Resource), Name = "ProfilePic")]
        //[ForeignKey(nameof(ProfilePic))]
        public int? ProfilePicId { get; set; }


        [JsonProperty(PropertyName = "useraccesslevel")]
        public AccessLevel UserAccessLevel { get; set; }




        [StringLength(32)]
        public string PasswordWithOutHash { get; set; }


        #region Relations

        #endregion


    }
}
