using DataAccessLayer.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entity
{
    public partial class TblPescription : BaseModel
    {
        #region Constractor

        public TblPescription()
        {

        }
        #endregion

        #region Fields

        #region Constractor

        #endregion
        [JsonProperty(PropertyName = "title")]
        public string? Title { get; set; }



        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }



        [JsonProperty(PropertyName = "isdelete")]
        public bool IsDelete { get; set; }

        [ForeignKey(nameof(OpgImage))]
        [JsonProperty(PropertyName = "opgimageid")]
        public int? OpgImageId { get; set; }


        [ForeignKey(nameof(User))]
        [JsonProperty(PropertyName = "userid")]
        public Guid? Userid { get; set; }



        #endregion

        #region Relations
        public virtual TblUserUploadFile OpgImage { get; set; }


        public virtual ApplicationUser User { get; set; }

        #endregion


        #region ICollections



        #endregion


    }
}
