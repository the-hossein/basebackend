using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text; 

namespace DataAccessLayer.Entity
{
    public partial class TblOtp: BaseModel
    {
       

        public TblOtp()
        {

        }

        [JsonProperty(PropertyName = "userid")]
        [ForeignKey(nameof(User))]
        public Guid? UserId { get; set; }

        [StringLength(11), Required]
        public string PhoneNumber { get; set; }

        [ForeignKey(nameof(Sms))]
        public int? SmsId { get; set; }


        [JsonProperty(PropertyName = "enterdatetime")]
        public DateTime? EnterDateTime { get; set; }

        [StringLength(10)]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }



        public virtual TblSms Sms { get; set; }
        public virtual ApplicationUser User { get; set; }

    }
}
