﻿using DataAccessLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.ViewModel
{
    public class OtpCheckViewModel
    {
        public TblOtp? Otp { get; set; }
        public string Message { get; set; }
        public int StatusCode { get; set; }
    }
}
