﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.ViewModel
{
    public class OtpViewModel
    {
        public int? OtpId { get; set; }
        public string Message { get; set; }
    }
}
