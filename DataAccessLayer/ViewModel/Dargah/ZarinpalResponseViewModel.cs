﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.ViewModel.Dargah
{
    public class ZarinpalResponseViewModel
    {
        public HttpStatusCode StatusCode { get; set; }
        public string responseString { get; set; }
        public string returnURL { get; set; }
        public int StatusCodeZarinpal { get; set; }
        public string RefId { get; set; }
        public List<ZarinpalTashimiViewModel> Wages { get; set; }
        public string CardHash { get; set; }
        public string CardPan { get; set; }
        public string FreeType { get; set; }
        public int? Fee { get; set; }
    }
}
