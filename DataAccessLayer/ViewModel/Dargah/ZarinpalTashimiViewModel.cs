﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.ViewModel.Dargah
{
    public class ZarinpalTashimiViewModel
    {

        public string iban { get; set; } // شماره شبا

        public int amount { get; set; }

        public string description { get; set; }
    }
}
