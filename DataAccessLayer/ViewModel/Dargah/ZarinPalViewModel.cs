﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.ViewModel.Dargah
{
    public class ZarinPalViewModelRequest
    {
        public string MerchantId { get; set; }
     
        public string Description { get; set; }
        [Required]
        public string ReturnUrl { get; set; }

        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public long Price { get; set; } // Toman

        public Guid UserId { get; set; }

        public Guid? BasketId { get; set; }
    }

    //public class ZarinPalViewModelResponse
    //{
    //    public TYPE Type { get; set; }
    //}
}
