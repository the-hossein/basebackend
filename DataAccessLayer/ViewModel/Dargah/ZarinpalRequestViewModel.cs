﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Enums;

namespace DataAccessLayer.ViewModel.Dargah
{
    public class ZarinpalRequestViewModel
    {
        public ZarinpalPayTypeEnum ZarinpalPayType { get; set; }
        public long Amount { get; set; }
        public string CallbackURL { get; set; }
        public string Mobile { get; set; }
        public string Description { get; set; }

        public Guid? UserId { get; set; }
        public Guid BasketId { get; set; }

        public List<ZarinpalTashimiViewModel> Tashimi { get; set; }
    }
}
