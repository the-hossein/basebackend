﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Validation;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace DataAccessLayer.ViewModel
{
    public class UploadByteArrayFileViewModelRequest
    {
        [Required(ErrorMessage = "Please select a file.")]
        [DataType(DataType.Upload)]
        [MaxFileSize(5 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".xlsx", ".xls", ".zip" , ".rar" })]
        public string FileName { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        public Guid? UserId { get; set; }
        public byte[] Content { get; set; }
    }
    public class UploadByteArrayFileViewModelResponse
    {
        public int Id { get; set; }

        [StringLength(255), Required]
        [JsonProperty(PropertyName = "filepath")]
        public string FilePath { get; set; }
    }
}

