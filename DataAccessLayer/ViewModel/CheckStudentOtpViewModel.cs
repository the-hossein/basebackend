﻿using Newtonsoft.Json;
using ResourcesLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.ViewModel
{
    public class CheckStudentOtpViewModel
    {
        public string PhoneNumber { get; set; }
        public string Code { get; set; }
        public string referralSite { get; set; }

        public int Type { get; set; } = 1;
    }
}
