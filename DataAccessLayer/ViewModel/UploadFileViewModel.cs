﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Validation;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace DataAccessLayer.ViewModel
{
    public class UploadFileViewModelRequest
    {
        [Required(ErrorMessage = "Please select a file.")]
        [DataType(DataType.Upload)]
        [MaxFileSize(1 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".svg", ".jpeg" })]
        public IFormFile File { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        public Guid? UserId { get; set; }
    }
    public class UploadFileViewModelResponse
    {
        public int Id { get; set; }

        [StringLength(255), Required]
        [JsonProperty(PropertyName = "filepath")]
        public string FilePath { get; set; }
    }
}

