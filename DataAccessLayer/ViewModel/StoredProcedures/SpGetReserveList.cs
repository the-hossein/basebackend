﻿using DataAccessLayer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.ViewModel.StoredProcedures
{
    public class SpGetReserveListRequest
    {
    }
    public class SpGetReserveListResponse
    {

        public DateTime ReserveDate { get; set; }

        public TimeEnum ReserveTime { get; set; }

        public ReserveStateEnum ReserveState { get; set; }

        public string UserName { get; set; }
        public string UserFamily { get; set; }
        public string UserFilePath { get; set; }

    }
}
