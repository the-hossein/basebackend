﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer.Entity;
using Microsoft.Extensions.DependencyInjection;
using Dentist.ViewModel;
using DataAccessLayer.ViewModel.StoredProcedures;

namespace Dentist
{
    public static class AutoMapping
    {
        public static void AddMapping(this IServiceCollection services)
        {
            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ApplicationUser, UserRequirementField>().ReverseMap();
            
            });
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}