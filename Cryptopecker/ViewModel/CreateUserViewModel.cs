﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entity;
using DataAccessLayer.Enums;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ResourcesLayer;

namespace Dentist.ViewModel
{
    public partial class UserRequirementField
    {
        [JsonProperty(PropertyName = "username")]
        //[RegularExpression("^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,20}$", ErrorMessage = "Only Alphabets and Numbers allowed.")]
        [Display(ResourceType = typeof(Resource), Name = "Username"), StringLength(50), Required]
        [DataType(DataType.Text)]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 10)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resource), Name = "Password")]
        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "KanoonUserCounter")]
        [JsonProperty(PropertyName = "KanoonUserCounter")]
        public int KanoonUserCounter { get; set; }

        [JsonProperty(PropertyName = "phonenumber")]
        [Display(ResourceType = typeof(Resource), Name = "PhoneNumber")]
        [StringLength(11, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 11)]
        [DataType(DataType.Text)]
        [Remote(action: "VerifyPhoneNumber", controller: "Validator", AdditionalFields = nameof(PhoneNumber))]
        public string PhoneNumber { get; set; }

        [JsonProperty(PropertyName = "email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
    public partial class CreateUserDynamic
    {
        [JsonProperty(PropertyName = "User")]
        public UserRequirementField User { get; set; }


        [JsonProperty(PropertyName = "userRole")]
        public UserRolesEnum UserRole { get; set; }
    }
}
