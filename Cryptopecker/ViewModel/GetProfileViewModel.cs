﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Validation;
using Newtonsoft.Json;
using ResourcesLayer;

namespace Tiola.ViewModel
{
    public partial class GetProfileViewModel
    {
        [Required]

        [JsonProperty(PropertyName = "phonenumber")]
        [Display(ResourceType = typeof(Resource), Name = "PhoneNumber")]
        [StringLength(11, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 11)]
        [DataType(DataType.Text)]
        [PhonenumberValidation(nameof(PhoneNumber))]
        public string PhoneNumber { get; set; }
    }
}
