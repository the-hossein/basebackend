﻿using DataAccessLayer.Validation;
using Newtonsoft.Json;
using ResourcesLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dentist.ViewModel
{
    public class SigninModel
    {
        public string Token1 { get; set; } // UserId

        [JsonProperty(PropertyName = "phonenumber")]
        [Display(ResourceType = typeof(Resource), Name = "PhoneNumber")]
        [StringLength(11, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 11)]
        [DataType(DataType.Text)]
        [PhonenumberValidation(nameof(PhoneNumber))]
        public string PhoneNumber { get; set; }

        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string NewPassword { get; set; }



        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string ConfirmPassword { get; set; }

        public bool NewUser { get; set; }

        public string Password { get; set; }

        public string OTPCode { get; set; }


        public int AttempOTPCode { get; set; }

        public string ReturnUrl { get; set; }
    }
}
