﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dentist.ViewModel
{
    public class ChangePasswordViewModel
    {
        [Required]

        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string OldPassword { get; set; }

        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "رمز عبور با تکرار رمز عبور برابر نمیباشد")]
        public string ConfirmPassword { get; set; }



    }
}
