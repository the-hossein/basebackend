﻿using DataAccessLayer.Validation;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Tiona.ViewModel
{
    public class CheckPhoneNumberViewModelRequest
    {
        [JsonProperty(PropertyName = "phonenumber")]
        //[Display(ResourceType = typeof(Resource), Name = "PhoneNumber")]
        [StringLength(11, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 11)]
        [DataType(DataType.Text)]
        [MinLength(11)]
        [Remote(action: "VerifyPhoneNumber", controller: "Validator", AdditionalFields = nameof(PhoneNumber))]
        [PhonenumberValidation(nameof(PhoneNumber))]
        public string PhoneNumber { get; set; }

    }

    public class CheckPhoneNumberViewModelResponse
    {

    }

}
