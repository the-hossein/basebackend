﻿using DataAccessLayer.Enums;
using DataAccessLayer.Validation;
using Newtonsoft.Json;
using ResourcesLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dentist.ViewModel
{
    public class CounselingViewModel
    {

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "phonenumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty(PropertyName = "service")]
        public ServicesEnum Service { get; set; }

    }
}
