﻿using Newtonsoft.Json;

namespace Dentist.ViewModel
{
    public class SampleViewModel
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "titleen")]
        public string TitleEn { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "descriptionen")]
        public string DescriptionEn { get; set; }

      
       
        [JsonProperty(PropertyName = "image1")]
        public int? Image1 { get; set; }
        
        [JsonProperty(PropertyName = "image2")]
        public int? Image2 { get; set; }

        [JsonProperty(PropertyName = "image3")]
        public int? Image3 { get; set; }

    
        [JsonProperty(PropertyName = "image4")]
        public int? Image4 { get; set; }
    }
}
