﻿using Tiola.ViewModel;
using DataAccessLayer.Enums;
using DataAccessLayer.ViewModel;
using Dentist.ViewModel;

namespace Tiona.ViewModel
{
    public class LoginOrSignupViewModelRequest
    {
        public LoginOrSignupActionEnum Methodname { get; set; }

        public CheckPhoneNumberViewModelRequest? CheckPhoneNumber { get; set; }

        public RequestLoginViewModel? LoginWithPassword { get; set; }

        public CheckStudentOtpViewModel? OtpCheck { get; set; }

        public SignupStudentViewModel? SignupStudent { get; set; }
    }


    public class LoginOrSignupViewModelResponse
    {

    }
}
