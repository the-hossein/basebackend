﻿using DataAccessLayer.Validation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dentist.ViewModel
{
    public class UploadDocumentViewModel
    {
        public int? ParvandeId { get; set; }
        public Guid? UserId { get; set; }


        #region CartMeli
        [Required(ErrorMessage = "Please select a file.")]
        [DataType(DataType.Upload)]
        [MaxFileSize(1 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".svg", ".jpeg" })]
        [Display(Name ="کارت ملی")]
        public IFormFile File_CartMeli { get; set; }

        [StringLength(50)]
        public string Title_CartMeli { get; set; }

        [StringLength(250)]
        public string Description_CartMeli { get; set; }
        #endregion

        #region StudentPic
        [Required(ErrorMessage = "Please select a file.")]
        [DataType(DataType.Upload)]
        [MaxFileSize(1 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".svg", ".jpeg" })]
        [Display(Name = "تصویر دانش آموز")]
        public IFormFile File_StudentPic { get; set; }

        [StringLength(50)]
        public string Title_StudentPic { get; set; }

        [StringLength(250)]
        public string Description_StudentPic { get; set; }

        #endregion


        #region Document
        [Required(ErrorMessage = "Please select a file.")]
        [DataType(DataType.Upload)]
        [MaxFileSize(1 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".svg", ".jpeg" })]
        [Display(Name ="مدارک")]
        public IFormFile File_Document { get; set; }

        [StringLength(50)]
        public string Title_Document { get; set; }

        [StringLength(250)]
        public string Description_Document { get; set; }

        #endregion

    }
}
