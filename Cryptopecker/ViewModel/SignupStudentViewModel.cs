﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer.Enums;
using DataAccessLayer.Validation;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ResourcesLayer;

namespace Tiona.ViewModel
{
    public class SignupStudentViewModel
    {
        [JsonProperty(PropertyName = "phonenumber")]
        //[Display(ResourceType = typeof(Resource), Name = "PhoneNumber")]
        [StringLength(11, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 11)]
        [DataType(DataType.Text)]
        [MinLength(11)]
        [Remote(action: "VerifyPhoneNumber", controller: "Validator", AdditionalFields = nameof(PhoneNumber))]

        public string PhoneNumber { get; set; }

        [JsonProperty(PropertyName = "name")]
        //[Display(ResourceType = typeof(Resource), Name = "Name")]
        [StringLength(50)]
        [DataType(DataType.Text)]
        public string Name { get; set; }


        [JsonProperty(PropertyName = "family")]
        //[Display(ResourceType = typeof(Resource), Name = "Family")]
        [StringLength(100)]
        [DataType(DataType.Text)]
        public string Family { get; set; }
        [JsonProperty(PropertyName = "nationalcode")]
        //[StringLength(10, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 10)]
        //[Display(ResourceType = typeof(Resource), Name = "NationalCode")]
        [DataType(DataType.Text)]
        [Remote(action: "VerifyNationalCode", controller: "Validator", AdditionalFields = nameof(NationalCode))]
        //[NationalCodeValidation(nameof(NationalCode))]
        public string NationalCode { get; set; }


        [JsonProperty(PropertyName = "gender")]
        //[Display(ResourceType = typeof(Resource), Name = "Gender")]
        public GenderEnum Gender { get; set; }
        [JsonProperty(PropertyName = "tel")]
        //[Display(ResourceType = typeof(Resource), Name = "Tel")]
        //[StringLength(20)]
        [DataType(DataType.Text)]
        public string Tel { get; set; }


        public string referralSite { get; set; }


        public int? Mode { get; set; }
    }
}
