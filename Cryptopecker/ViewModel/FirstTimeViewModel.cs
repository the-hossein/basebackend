﻿using DataAccessLayer.Enums;
using DataAccessLayer.Validation;
using DataAccessLayer.ViewModel;
using Newtonsoft.Json;
using ResourcesLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dentist.ViewModel
{
    public class FirstTimeViewModel
    {

        [JsonProperty(PropertyName = "times")]
        public List<TimeState> Times { get; set; }


        [JsonProperty(PropertyName = "date")]
        public DateTime Date { get; set; }


    }
}
