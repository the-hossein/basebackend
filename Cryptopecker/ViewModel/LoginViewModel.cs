﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Dentist.ViewModel
{
    public partial class RequestLoginViewModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }

        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }

    }

    public partial class ResponseJwtViewModel
    {
        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }

    }
}
