﻿using DataAccessLayer.Enums;
using DataAccessLayer.Validation;
using Newtonsoft.Json;
using ResourcesLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dentist.ViewModel
{
    public class ReservationViewModel
    {

        [JsonProperty(PropertyName = "userid")]
        public Guid UserId { get; set; }

        [JsonProperty(PropertyName = "date")]
        public DateTime Date { get; set; }

        [JsonProperty(PropertyName = "time")]
        public TimeEnum Time { get; set; }


        [JsonProperty(PropertyName = "service")]
        public ServicesEnum Service { get; set; }


    }
}
