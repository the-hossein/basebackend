﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dentist.ViewModel
{
    public class UploadFileViewModel
    {
        public Guid? Id { get; set; }
        public IFormFile File { get; set; }
        public DateTime PayDateTime { get; set; }
        public string RefrenceNumber { get; set; }
        public string CardNumber { get; set; }
        public string TraceNo { get; set; }
        public long Amount { get; set; }


    }
}
