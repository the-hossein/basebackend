﻿using DataAccessLayer.Entity;
using DataAccessLayer.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.ViewModel
{
    public class TimeState
    {
        [JsonProperty(PropertyName = "time")]
        public TimeEnum Time { get; set; }

        [JsonProperty(PropertyName = "state")]
        public bool State { get; set; }

        [JsonProperty(PropertyName = "reservestate")]
        public ReserveStateEnum ReserveState { get; set; }
    }
}
