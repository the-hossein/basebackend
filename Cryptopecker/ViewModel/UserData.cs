﻿using DataAccessLayer.Entity;
using DataAccessLayer.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.ViewModel
{
    public class UserData
    {
        [JsonProperty(PropertyName = "user")]
        public ApplicationUser User { get; set; }
        [JsonProperty(PropertyName = "profilePic")]
        public TblUserUploadFile ProfilePic { get; set; }
    }

    public class UserDataAccessLevel
    {
        [JsonProperty(PropertyName = "phonenumber")]
        public string PhoneNumber { get; set; }
        [JsonProperty(PropertyName = "accesslevel")]
        public AccessLevel AccessLevel { get; set; }
    }

    public class PescriptionViewModel
    {
        [JsonProperty(PropertyName = "title")]
        public string? Title { get; set; }


        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }



        [JsonProperty(PropertyName = "opgimageid")]
        public int? OpgImageId { get; set; }

        [JsonProperty(PropertyName = "userid")]
        public Guid? Userid { get; set; }
    }
}
