﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Dentist.Controllers;
using ServicesLayer.Contract;
using DataAccessLayer.Entity;
using AutoMapper;
using DataAccessLayer.Enums;
using DataAccessLayer.ViewModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Extensions;
using Dentist.ViewModel;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Tiona.ViewModel;
using Tiola.ViewModel;
using ServicesLayer.Services.Helper;
using static Dentist.ViewModel.ResultModel;

namespace Dentist.Controllers.Api
{
    [ApiVersion("1.0")]
    [ApiExplorerSettings(GroupName = "v1")]
    [Authorize]
    public class UserController : BaseController
    {
        private SignInManager<ApplicationUser> _signInManager;
        private UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;
        private readonly ILogger<UserController> _logger;
        private readonly IConfiguration _config;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IUserService _userService;
        private readonly IOtpService _otpService;
        private readonly ISmsService _smsService;
        private readonly IReservationService _reservation;
        private readonly IUserUploadFileService _uploadFile;
        private readonly ICounselingService _Counseling;
        private readonly IStoredProcedures _Sp;
        private readonly IPescriptionService _Pescription;
        public UserController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signManager,
            IMapper mapper, ILogger<UserController> logger, IPescriptionService Pescription, IConfiguration config, RoleManager<ApplicationRole> roleManager, ISmsService smsService, IOtpService otpService, IUserService UserService, IUserUploadFileService UploadFile, IReservationService Reservation, ICounselingService Counseling, IStoredProcedures SP)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _signInManager = signManager;
            _mapper = mapper;
            _config = config;
            _logger = logger;
            _smsService = smsService;
            _otpService = otpService;
            _userService = UserService;
            _uploadFile = UploadFile;
            _reservation = Reservation;
            _Counseling = Counseling;
            _Sp = SP;
            _Pescription = Pescription;
        }

        [HttpPost(nameof(CreateUser)), AllowAnonymous]
        public async Task<IActionResult> CreateUser([FromBody] CreateUserDynamic userModel)
        {
            if (ModelState.IsValid)
            {

                var FoundUser = await _userManager.FindByNameAsync(userModel.User.UserName);

                if (FoundUser == null)
                {
                    var NewUser = new ApplicationUser
                    {
                        Id = Guid.NewGuid(),
                        UserName = userModel.User.UserName,
                        PhoneNumber = userModel.User.PhoneNumber,
                        Email = userModel.User.Email,
                        CreatedDatetime = DateTime.Now,
                    };

                    var result = await _userManager.CreateAsync(NewUser, userModel.User.Password);

                    if (result.Succeeded)
                    {
                        _logger.LogInformation("User created a new account with password.");


                        await _userManager.AddToRoleAsync(NewUser, "AdminSite");

                        return Ok(new ResultModel
                        {
                            Code = Ok().StatusCode,
                            Message = "Success",
                            Data = NewUser
                        });
                    }

                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }

                }
                else
                {
                    return Ok(new ResultModel
                    {
                        Code = BadRequest().StatusCode,
                        Message = "UserExist",
                        Data = null
                    });
                }
                return Ok(new ResultModel
                {
                    Code = BadRequest().StatusCode,
                    Message = "BadRequest",
                    Data = null
                });
            }
            return Ok(new ResultModel
            {
                Code = BadRequest().StatusCode,
                Message = "BadRequest",
                Data = null
            });
        }

        private void AddRolesToClaims(List<Claim> claims, IEnumerable<string> roles)
        {
            foreach (var role in roles)
            {
                var roleClaim = new Claim(ClaimTypes.Role, role);
                claims.Add(roleClaim);
            }
        }

        [HttpPost(nameof(Login)), AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginOrSignupViewModelRequest model)
        {
            try
            {
                switch (model.Methodname)
                {
                    #region CheckPhoneNumberLogin
                    case LoginOrSignupActionEnum.CheckPhoneNumberLogin:

                        if (model.CheckPhoneNumber != null && TryValidateModel(model.CheckPhoneNumber, nameof(model.CheckPhoneNumber)))
                        {

                            var foundUser = await _userService.FindByCondition(x => x.UserName == model.CheckPhoneNumber.PhoneNumber);

                            if (foundUser != null)
                            {
                                var ResultOtp = await _otpService.OTPSend(model.CheckPhoneNumber.PhoneNumber, null, null);

                                if (ResultOtp.Message.Contains("کد با موفقیت ارسال شد"))
                                {
                                    return Ok(new ResultModel
                                    {
                                        Code = Ok().StatusCode,
                                        Message = ResultOtp.Message,
                                        Data = new
                                        {
                                            IsNewUser = true
                                        }
                                    });
                                }
                                else
                                {
                                    return BadRequest(new { message = ResultOtp });
                                }

                            }
                            else
                            {
                                return BadRequest(new { message = "لطفا ابتدا ثبت نام کنید" });

                            }
                        }
                        else
                        {
                            return BadRequest(new { message = "داده های ورودی اشتباه است" });
                        }
                        break;

                    #endregion

                    #region LoginWithPassword
                    case LoginOrSignupActionEnum.LoginWithPassword:
                        if (model.LoginWithPassword != null &&
                            TryValidateModel(model.LoginWithPassword, nameof(model.LoginWithPassword)))
                        {
                            var user = await _userManager.FindByNameAsync(model.LoginWithPassword.UserName);
                            if (user != null && await _userManager.CheckPasswordAsync(user, model.LoginWithPassword.Password))
                            {
                                var claims = new List<Claim>
                                {
                                    new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                                    new Claim(ClaimTypes.NameIdentifier, user.UserName.ToString())
                                };

                                // Get User roles and add them to claims
                                var roles = await _userManager.GetRolesAsync(user);
                                AddRolesToClaims(claims, roles);

                                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
                                var token = new JwtSecurityToken(
                                    issuer: _config["Jwt:Issuer"],
                                    audience: _config["Jwt:Audience"],
                                    expires: DateTime.Now.AddDays(5),
                                    claims: claims,
                                    signingCredentials: new Microsoft.IdentityModel.Tokens.SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                                );

                                var SignIn = _signInManager.PasswordSignInAsync(user, model.LoginWithPassword.Password, true, false);
                                return Ok(new ResultModel
                                {
                                    Code = Ok().StatusCode,
                                    Message = "با موفقیت لاگین شدید ",
                                    Data = new
                                    {
                                        token = new JwtSecurityTokenHandler().WriteToken(token),
                                        expiration = token.ValidTo,
                                        rolenames = roles
                                    }
                                });
                            }
                            else
                            {
                                ModelState.AddModelError("CustomError", "رمز عبور با شماره همراه مطابقت ندارد");
                                return BadRequest(new { message = "رمز عبور با شماره همراه مطابقت ندارد", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("CustomError", "داده های ورودی اشتباه است");
                            return BadRequest(new { message = "داده های ورودی اشتباه است", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });
                        }
                        break;
                    #endregion

                    #region CheckOtp
                    case LoginOrSignupActionEnum.CheckOtp:
                        if (model.OtpCheck != null &&
                            TryValidateModel(model.OtpCheck, nameof(model.OtpCheck)))
                        {
                            var FoundOtp = await _otpService.OtpCheck(model.OtpCheck.PhoneNumber, model.OtpCheck.Code);
                            if (FoundOtp.Otp != null)
                            {
                                if (FoundOtp.StatusCode != 200)
                                {
                                    ModelState.AddModelError("CustomError", FoundOtp.Message);
                                    return BadRequest(new { message = FoundOtp, errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });
                                }

                                var user = await _userManager.FindByNameAsync(model.OtpCheck.PhoneNumber);

                                if (user != null)
                                {
                                    var claims = new List<Claim>
                                    {
                                        new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                                        new Claim(ClaimTypes.NameIdentifier, user.UserName.ToString())
                                    };

                                    // Get User roles and add them to claims
                                    var roles = await _userManager.GetRolesAsync(user);
                                    AddRolesToClaims(claims, roles);

                                    var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
                                    var token = new JwtSecurityToken(
                                        issuer: _config["Jwt:Issuer"],
                                        audience: _config["Jwt:Audience"],
                                        expires: DateTime.Now.AddDays(5),
                                        claims: claims,
                                        signingCredentials: new Microsoft.IdentityModel.Tokens.SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                                    );

                                    await _signInManager.SignInAsync(user, true, null);

                                    return Ok(new ResultModel
                                    {
                                        Code = Ok().StatusCode,
                                        Message = "با موفقیت لاگین شدید ",
                                        Data = new
                                        {
                                            token = new JwtSecurityTokenHandler().WriteToken(token),
                                            expiration = token.ValidTo,
                                            rolenames = roles
                                        }
                                    });
                                }
                                else
                                {
                                    return BadRequest(new { message = "کاربری یافت نشد", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });
                                }
                            }
                            else
                            {
                                ModelState.AddModelError("CustomError", "کد ارسالی با شماره وارد شده مطابقت ندارد");
                                return BadRequest(new { message = "داده های وارد شده اشتباه میباشد", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });
                            }

                        }
                        break;
                    #endregion

                    default:
                        break;
                }
            }
            catch (Exception E)
            {
                ModelState.AddModelError("CustomError", E.Message);
                return BadRequest(new { message = "خطای سیستمی", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });

            }
            ModelState.AddModelError("CustomError", "داده های وارد شده اشتباه میباشد");
            return BadRequest(new { message = "داده های وارد شده اشتباه میباشد", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });
        }

        [HttpPost(nameof(Signup)), AllowAnonymous]
        public async Task<IActionResult> Signup([FromBody] LoginOrSignupViewModelRequest model)
        {
            try
            {
                switch (model.Methodname)
                {

                    #region CheckPhoneNumberSignUp
                    case LoginOrSignupActionEnum.CheckPhoneNumberSignUp:

                        if (model.CheckPhoneNumber != null && TryValidateModel(model.CheckPhoneNumber, nameof(model.CheckPhoneNumber)))
                        {

                            var foundUser = await _userService.FindByCondition(x => x.UserName == model.CheckPhoneNumber.PhoneNumber);

                            if (foundUser == null)
                            {
                                var ResultOtp = await _otpService.OTPSend(model.CheckPhoneNumber.PhoneNumber, null, null);

                                if (ResultOtp.Message.Contains("کد با موفقیت ارسال شد"))
                                {
                                    return Ok(new ResultModel
                                    {
                                        Code = Ok().StatusCode,
                                        Message = "نیاز به ثبت نام",
                                        Data = new
                                        {
                                            IsNewUser = true,
                                            Status = ResultOtp
                                        }
                                    });
                                }
                                else
                                {
                                    ModelState.AddModelError("CustomError", "با این شماره همراه کاربری ثبت نام نشده است" + Environment.NewLine + ResultOtp);
                                    return BadRequest(new { message = ResultOtp, errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });
                                }
                            }
                            else
                            {

                                var ResultOtp = await _otpService.OTPSend(model.CheckPhoneNumber.PhoneNumber, foundUser.Id, foundUser.Id);

                                if (ResultOtp.Message.Contains("کد با موفقیت ارسال شد"))
                                {
                                    return Ok(new ResultModel
                                    {
                                        Code = Ok().StatusCode,
                                        Message = "از پیش ثبت نام شده",
                                        Data = new
                                        {
                                            IsNewUser = false,
                                            Status = ResultOtp
                                        }
                                    });
                                }
                                else
                                {
                                    ModelState.AddModelError("CustomError", ResultOtp.Message);
                                    return BadRequest(new { message = ResultOtp, errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });
                                }
                            }
                        }
                        else
                        {
                            return BadRequest(new { message = "داده های ورودی اشتباه است" });
                        }
                        break;

                    #endregion

                    #region LoginOrSignupActionEnum.CheckOtp
                    case LoginOrSignupActionEnum.CheckOtp:
                        if (model.OtpCheck != null &&
                            TryValidateModel(model.OtpCheck, nameof(model.OtpCheck)))
                        {
                            string Message = "با موفقیت لاگین شدید ";
                            var FoundOtp = await _otpService.OtpCheck(model.OtpCheck.PhoneNumber, model.OtpCheck.Code);
                            if (FoundOtp != null)
                            {
                                if (FoundOtp.StatusCode != 200)
                                {
                                    ModelState.AddModelError("CustomError", FoundOtp.Message);
                                    return BadRequest(new { message = "کد وارد شده اشتباه است", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });
                                }

                                //if (model.OtpCheck.Type == 2)
                                //{
                                //    Message = "درخواست مشاوره شما با موفقیت ثبت شد";
                                //    TblSupport NewSupport = new TblSupport();
                                //    NewSupport.PhoneNumber = model.OtpCheck.PhoneNumber;
                                //    NewSupport.OtpId = FoundOtp.Otp.Id;
                                //    NewSupport.SupportType = DataAccessLayer.Enums.SupportTypeEnum.Moshavere_Free;
                                //    NewSupport.CreatedDatetime = DateTime.Now;
                                //    TblSupport TheSupport = await _supportService.CreateSupport(NewSupport);

                                //    var AddSupportToCrm = await _storedProcedures.AddSupportToCrmCall(new DataAccessLayer.ViewModel.StoredProcedures.SpAddSupportToCrmCallViewModelRequest
                                //    {
                                //        Phonenumber = model.OtpCheck.PhoneNumber,
                                //        Description = "درخواست مشاوره Asanamooz",
                                //        ProjectId = CRMCallProjectTypeIdEnum.RequestHelp
                                //    });

                                //    await _telegramService.SendMessageToDynamicGroupPost($"شماره {model.OtpCheck.PhoneNumber} درخواست مشاوره داده اند ، لطفا پیگیری بفرمایید.", TelegramGroups.AsanAmoozCall.GetDescription());
                                //}

                                var user = await _userManager.FindByNameAsync(model.OtpCheck.PhoneNumber);

                                if (user != null)
                                {
                                    var claims = new List<Claim>
                                    {
                                        new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                                        new Claim(ClaimTypes.NameIdentifier, user.UserName.ToString())
                                    };

                                    // Get User roles and add them to claims
                                    var roles = await _userManager.GetRolesAsync(user);
                                    AddRolesToClaims(claims, roles);

                                    var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
                                    var token = new JwtSecurityToken(
                                        issuer: _config["Jwt:Issuer"],
                                        audience: _config["Jwt:Audience"],
                                        expires: DateTime.Now.AddDays(5),
                                        claims: claims,
                                        signingCredentials: new Microsoft.IdentityModel.Tokens.SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                                    );

                                    await _signInManager.SignInAsync(user, true, null);

                                    return Ok(new ResultModel
                                    {
                                        Code = Ok().StatusCode,
                                        Message = Message,
                                        Data = new
                                        {
                                            token = new JwtSecurityTokenHandler().WriteToken(token),
                                            expiration = token.ValidTo,
                                            rolenames = roles
                                        }
                                    });
                                }
                                else
                                {
                                    ApplicationUser newUser = new ApplicationUser
                                    {

                                        UserName = model.OtpCheck.PhoneNumber,
                                        PhoneNumber = model.OtpCheck.PhoneNumber,
                                        CreatedDatetime = DateTime.Now,
                                        UserAccessLevel = AccessLevel.User
                                        //Name = model.SignupStudent.Name,
                                        //Family = model.SignupStudent.Family,

                                    };
                                    string generatedPassword = model.OtpCheck.PhoneNumber.Substring(0, 5) + Helpers.RandomString(5);

                                    var createNewUser = await _userManager.CreateAsync(newUser, generatedPassword);

                                    //UserRolesEnum role = UserRolesEnum.Normal;

                                    //await _userManager.AddToRoleAsync(newUser, role.ToString());

                                    var massage = $"با موفقیت ثبت نام شدید" + Environment.NewLine + "Password: " + generatedPassword;

                                    var FoundRecord = await _smsService.SendMessageKavenegar(newUser.PhoneNumber, "DentistGeneratePassword", newUser.Id, SmsTypeEnum.OtpSend, generatedPassword);

                                    var FoundNewUser = await _userManager.FindByNameAsync(model.OtpCheck.PhoneNumber);

                                    var claims = new List<Claim>
                                    {
                                        new Claim(JwtRegisteredClaimNames.Sub, FoundNewUser.UserName),
                                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                                        new Claim(ClaimTypes.NameIdentifier, FoundNewUser.UserName.ToString())
                                    };

                                    // Get User roles and add them to claims
                                    var roles = await _userManager.GetRolesAsync(FoundNewUser);
                                    AddRolesToClaims(claims, roles);

                                    var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
                                    var token = new JwtSecurityToken(
                                        issuer: _config["Jwt:Issuer"],
                                        audience: _config["Jwt:Audience"],
                                        expires: DateTime.Now.AddDays(5),
                                        claims: claims,
                                        signingCredentials: new Microsoft.IdentityModel.Tokens.SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                                    );

                                    var SignIn = _signInManager.SignInAsync(FoundNewUser, true, null);





                                    return Ok(new ResultModel
                                    {
                                        Code = ResponseStatusCode.Created.GetNumberEnum(),
                                        Message = "با موفقیت ثبت نام گردید",
                                        Data = new
                                        {
                                            token = new JwtSecurityTokenHandler().WriteToken(token),
                                            expiration = token.ValidTo,
                                            rolenames = roles
                                        }
                                    });

                                }
                            }
                            else
                            {
                                ModelState.AddModelError("CustomError", "کد ارسالی با شماره وارد شده مطابقت ندارد");
                                return BadRequest(new { message = "داده های وارد شده اشتباه میباشد", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });
                            }

                        }
                        break;
                    #endregion

                    #region Signup
                    case LoginOrSignupActionEnum.Signup:
                        if (model.SignupStudent != null &&
                             TryValidateModel(model.SignupStudent, nameof(model.SignupStudent)))
                        {
                            var user = await _userManager.FindByNameAsync(model.SignupStudent.PhoneNumber);

                            if (user == null)
                            {
                                var FoundCheckOtp = await _otpService.FindByCondition(a =>
                                    a.PhoneNumber == model.SignupStudent.PhoneNumber && a.EnterDateTime != null);

                                if (FoundCheckOtp != null)
                                {

                                    ApplicationUser newUser = new ApplicationUser
                                    {

                                        UserName = model.SignupStudent.PhoneNumber,
                                        PhoneNumber = model.SignupStudent.PhoneNumber,
                                        CreatedDatetime = DateTime.Now,
                                        UserAccessLevel = AccessLevel.User,

                                    };
                                    //string generatedPassword = model.SignupStudent.PhoneNumber.Substring(0, 5) + Helpers.RandomString(5);
                                    string generatedPassword = model.SignupStudent.PhoneNumber.Substring(0, 5) + Helpers.RandomString(5);

                                    var createNewUser = await _userManager.CreateAsync(newUser, generatedPassword);

                                    UserRolesEnum role = UserRolesEnum.Normal;

                                    await _userManager.AddToRoleAsync(newUser, role.ToString());

                                    var massage = $"با موفقیت ثبت نام شدید" + Environment.NewLine + "Password: " + generatedPassword;

                                    var FoundRecord = await _smsService.SendMessageKavenegar(newUser.PhoneNumber, "DentistGeneratePassword", newUser.Id, SmsTypeEnum.OtpSend, generatedPassword);

                                    var FoundNewUser = await _userManager.FindByNameAsync(model.SignupStudent.PhoneNumber);

                                    var claims = new List<Claim>
                                    {
                                        new Claim(JwtRegisteredClaimNames.Sub, FoundNewUser.UserName),
                                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                                        new Claim(ClaimTypes.NameIdentifier, FoundNewUser.UserName.ToString())
                                    };

                                    // Get User roles and add them to claims
                                    var roles = await _userManager.GetRolesAsync(FoundNewUser);
                                    AddRolesToClaims(claims, roles);

                                    var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
                                    var token = new JwtSecurityToken(
                                        issuer: _config["Jwt:Issuer"],
                                        audience: _config["Jwt:Audience"],
                                        expires: DateTime.Now.AddDays(5),
                                        claims: claims,
                                        signingCredentials: new Microsoft.IdentityModel.Tokens.SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                                    );

                                    var SignIn = _signInManager.SignInAsync(FoundNewUser, true, null);


                                    return Ok(new ResultModel
                                    {
                                        Code = ResponseStatusCode.Created.GetNumberEnum(),
                                        Message = "با موفقیت ثبت نام گردید",
                                        Data = new
                                        {
                                            token = new JwtSecurityTokenHandler().WriteToken(token),
                                            expiration = token.ValidTo,
                                            rolenames = roles
                                        }
                                    });

                                }
                                else
                                {
                                    ModelState.AddModelError("CustomError", "شماره همراه کاربر تایید نشده است");
                                    return BadRequest(new { message = "ورود های وارد شده اشتباه میباشد", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });
                                }
                            }
                            else
                            {
                                ModelState.AddModelError("CustomError", "کاربر قبلا ثبت نام شده است");
                                return BadRequest(new { message = "ورود های وارد شده اشتباه میباشد", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });
                            }

                        }
                        else
                        {
                            ModelState.AddModelError("CustomError", "شماره همراه وارد شده اشتباه میباشد");
                            return BadRequest(new { message = "ورود های وارد شده اشتباه میباشد", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });
                        }
                        break;
                    #endregion

                    default:
                        break;
                }
            }
            catch (Exception E)
            {
                ModelState.AddModelError("CustomError", E.Message);
                return BadRequest(new { message = "خطای سیستمی", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });

            }
            ModelState.AddModelError("CustomError", "داده های وارد شده اشتباه میباشد");
            return BadRequest(new { message = "داده های وارد شده اشتباه میباشد", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });
        }


        [HttpPost(nameof(GetProfile)), Authorize]
        public async Task<IActionResult> GetProfile([FromBody] GetProfileViewModel model)
        {
            try
            {
                UserData User = new UserData();
                var TheUser = await _userService.FindByCondition(a => a.PhoneNumber == model.PhoneNumber);
                User.User = TheUser;
                if (TheUser.ProfilePicId != null)
                {
                    User.ProfilePic = await _uploadFile.GetUserUploadFileWithId(TheUser.ProfilePicId.Value);
                    return Ok(new ResultModel
                    {
                        Code = Ok().StatusCode,
                        Message = "Success",
                        Data = User
                    });
                }
                else
                {
                    return Ok(new ResultModel
                    {
                        Code = Ok().StatusCode,
                        Message = "Success",
                        Data = User
                    });
                }

            }
            catch (Exception e)
            {
                return BadRequest(new { message = "خطا سیستمی", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });

            }
        }

        [HttpPost(nameof(SetReservation)), Authorize]
        public async Task<IActionResult> SetReservation([FromBody] ReservationViewModel model)
        {
            try
            {
                var CheckExistReserveTime = await _reservation.FindByCondition(a => a.Date.Day == model.Date.Day && a.Date.Month == model.Date.Month && a.Date.Year == model.Date.Year && a.State != ReserveStateEnum.Null && a.Time == model.Time);
                if (CheckExistReserveTime == null)
                {
                    var CheckExistNullReserveTime = await _reservation.FindByCondition(a => a.Date.Day == model.Date.Day && a.Date.Month == model.Date.Month && a.Date.Year == model.Date.Year && a.State == ReserveStateEnum.Null && a.Time == model.Time);
                    if (CheckExistNullReserveTime == null)
                    {
                        TblReservation NewReservation = new TblReservation()
                        {
                            UserId = model.UserId,
                            Date = model.Date,
                            Time = model.Time,
                            Services = model.Service,
                            State = ReserveStateEnum.Reserved,
                            CreatedDatetime = DateTime.Now
                        };
                        var Result = await _reservation.Create(NewReservation);
                        return Ok(new ResultModel
                        {
                            Code = Ok().StatusCode,
                            Message = "Success",
                            Data = Result
                        });
                    }
                    else
                    {
                        CheckExistNullReserveTime.UserId = model.UserId;
                        CheckExistNullReserveTime.State = ReserveStateEnum.Reserved;
                        CheckExistNullReserveTime.Services = model.Service;
                        if (CheckExistNullReserveTime.Services == ServicesEnum.Composite || CheckExistNullReserveTime.Services == ServicesEnum.Dentallaminate)
                        {
                            CheckExistNullReserveTime.SpecialService = true;
                        }
                        var Result = await _reservation.Update(CheckExistNullReserveTime);
                        return Ok(new ResultModel
                        {
                            Code = Ok().StatusCode,
                            Message = "Success",
                            Data = Result
                        });
                    }

                }
                else
                {
                    return NotFound(new ResultModel
                    {
                        Code = NotFound().StatusCode,
                        Message = "نوبت انتخاب شده رزرو می باشد",
                        Data = null
                    });
                }

            }
            catch (Exception e)
            {
                return BadRequest(new { message = "خطا سیستمی", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });

            }
        }

        [HttpGet(nameof(LoadTime)), AllowAnonymous]
        public async Task<IActionResult> LoadTime(DateTime Day, ServicesEnum Service)
        {
            var DayOfweek = Day.DayOfWeek;
            try
            {
                DateTime Today = DateTime.Now;

                var ResultDate = Convert.ToInt32((Day - Today).TotalDays);
                if (Today > Day)
                {
                    return Ok(new ResultModel
                    {
                        Code = 206,
                        Message = "تاریخ وارد شده خارج از محدوده نوبت دهی می یاشد",
                        Data = null
                    });
                }
                else if (ResultDate > 30)
                {
                    return Ok(new ResultModel
                    {
                        Code = 206,
                        Message = "تاریخ وارد شده خارج از محدوده نوبت دهی می یاشد",
                        Data = null
                    });
                }
                else if (DayOfweek == DayOfWeek.Friday)
                {
                    return Ok(new ResultModel
                    {
                        Code = 206,
                        Message = "روز انتخاب شده خارج از تقویم کاری می باشد",
                        Data = null
                    });
                }
                else
                {
                    List<TimeState> ExistTime = new List<TimeState>();
                    ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.NineAm, ReserveState = ReserveStateEnum.Null });
                    ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.TenAm, ReserveState = ReserveStateEnum.Null });
                    ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.ElevenAm, ReserveState = ReserveStateEnum.Null });
                    ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.twelveAm, ReserveState = ReserveStateEnum.Null });
                    ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.OnePm, ReserveState = ReserveStateEnum.Null });
                    ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.TwoPm, ReserveState = ReserveStateEnum.Null });
                    ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.ThreePm, ReserveState = ReserveStateEnum.Null });
                    ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.FourPm, ReserveState = ReserveStateEnum.Null });
                    ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.FivePm, ReserveState = ReserveStateEnum.Null });
                    ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.SixPm, ReserveState = ReserveStateEnum.Null });
                    ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.SevenPm, ReserveState = ReserveStateEnum.Null });
                    ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.EightPm, ReserveState = ReserveStateEnum.Null });
                    ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.NinePm, ReserveState = ReserveStateEnum.Null });

                    if (Service == ServicesEnum.Composite || Service == ServicesEnum.Dentallaminate)
                    {
                        var ReserveExist = (await _reservation.FindListByCondition(a => a.Date.Day == Day.Day && a.Date.Month == Day.Month && a.Date.Year == Day.Year && a.SpecialService == true && a.State == ReserveStateEnum.Null)).FirstOrDefault();
                        if (ReserveExist != null)
                        {
                            for (int i = 0; i < ExistTime.Count(); i++)
                            {
                                if (ReserveExist.Time == ExistTime[i].Time)
                                {
                                    ExistTime[i].ReserveState = ReserveStateEnum.Null;
                                    ExistTime[i].State = true;
                                }
                                else
                                {
                                    ExistTime[i].ReserveState = ReserveStateEnum.Disable;
                                    ExistTime[i].State = false;
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < ExistTime.Count(); i++)
                            {
                                ExistTime[i].ReserveState = ReserveStateEnum.Disable;
                                ExistTime[i].State = false;
                            }
                        }
                        return Ok(new ResultModel
                        {
                            Code = 200,
                            Message = "نوبت های یافت شده",
                            Data = ExistTime
                        });
                    }
                    else
                    {
                        var Result = (await _reservation.FindListByCondition(a => a.Date.Day == Day.Day && a.Date.Month == Day.Month && a.Date.Year == Day.Year)).ToList();
                        if (Result.Count() > 0)
                        {
                            var FindSpecialService = Result.Where(a => a.SpecialService == true).FirstOrDefault();
                            if (FindSpecialService != null)
                            {
                                var EmptyRound = Result.Where(a => a.State == ReserveStateEnum.Null).ToList();
                                for (int i = 0; i < ExistTime.Count(); i++)
                                {
                                    ExistTime[i].ReserveState = ReserveStateEnum.Disable;
                                    ExistTime[i].State = false;
                                }
                                if (EmptyRound.Count > 0)
                                {
                                    for (int i = 0; i < EmptyRound.Count(); i++)
                                    {
                                        ExistTime.Where(a => a.Time == EmptyRound[i].Time).FirstOrDefault().State = true;
                                        ExistTime.Where(a => a.Time == EmptyRound[i].Time).FirstOrDefault().ReserveState = ReserveStateEnum.Null;
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < Result.Count(); i++)
                                {
                                    if (Result[i].State == ReserveStateEnum.Disable)
                                    {
                                        ExistTime.Where(a => a.Time == Result[i].Time).FirstOrDefault().State = false;
                                        ExistTime.Where(a => a.Time == Result[i].Time).FirstOrDefault().ReserveState = ReserveStateEnum.Disable;
                                    }
                                    else if (Result[i].State == ReserveStateEnum.Canceled)
                                    {
                                        ExistTime.Where(a => a.Time == Result[i].Time).FirstOrDefault().State = true;
                                        ExistTime.Where(a => a.Time == Result[i].Time).FirstOrDefault().ReserveState = ReserveStateEnum.Null;
                                    }
                                    else if (Result[i].State == ReserveStateEnum.Null)
                                    {
                                        ExistTime.Where(a => a.Time == Result[i].Time).FirstOrDefault().State = true;
                                        ExistTime.Where(a => a.Time == Result[i].Time).FirstOrDefault().ReserveState = ReserveStateEnum.Null;
                                    }
                                    else if (Result[i].State == ReserveStateEnum.Reserved)
                                    {
                                        ExistTime.Where(a => a.Time == Result[i].Time).FirstOrDefault().State = false;
                                        ExistTime.Where(a => a.Time == Result[i].Time).FirstOrDefault().ReserveState = ReserveStateEnum.Reserved;
                                    }
                                }
                            }
                        }
                        return Ok(new ResultModel
                        {
                            Code = Ok().StatusCode,
                            Message = "Success",
                            Data = ExistTime
                        });
                    }
                }



            }
            catch (Exception e)
            {
                return BadRequest(new { message = "خطا سیستمی", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });

            }
        }

        [HttpPost(nameof(SetCounseling)), AllowAnonymous]
        public async Task<IActionResult> SetCounseling([FromBody] CounselingViewModel model)
        {
            try
            {
                TblCounseling NewCounseling = new TblCounseling()
                {
                    Name = model.Name,
                    PhoneNumber = model.PhoneNumber,
                    Service = model.Service,
                    CreatedDatetime = DateTime.Now
                };
                var Result = await _Counseling.Create(NewCounseling);
                return Ok(new ResultModel
                {
                    Code = Ok().StatusCode,
                    Message = "Success",
                    Data = Result
                });

            }
            catch (Exception e)
            {
                return BadRequest(new { message = "خطا سیستمی", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });

            }
        }

        [HttpGet(nameof(FindFirstTime)), AllowAnonymous]
        public async Task<IActionResult> FindFirstTime(ServicesEnum Service)
        {
            try
            {
                FirstTimeViewModel NewFirstTime = new FirstTimeViewModel();
                List<TimeState> ExistTime = new List<TimeState>();
                ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.NineAm, ReserveState = ReserveStateEnum.Null });
                ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.TenAm, ReserveState = ReserveStateEnum.Null });
                ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.ElevenAm, ReserveState = ReserveStateEnum.Null });
                ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.twelveAm, ReserveState = ReserveStateEnum.Null });
                ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.OnePm, ReserveState = ReserveStateEnum.Null });
                ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.TwoPm, ReserveState = ReserveStateEnum.Null });
                ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.ThreePm, ReserveState = ReserveStateEnum.Null });
                ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.FourPm, ReserveState = ReserveStateEnum.Null });
                ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.FivePm, ReserveState = ReserveStateEnum.Null });
                ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.SixPm, ReserveState = ReserveStateEnum.Null });
                ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.SevenPm, ReserveState = ReserveStateEnum.Null });
                ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.EightPm, ReserveState = ReserveStateEnum.Null });
                ExistTime.Add(new TimeState() { State = true, Time = TimeEnum.NinePm, ReserveState = ReserveStateEnum.Null });
                var Result = (await _reservation.FindListByCondition(a => a.Services == Service && a.State == ReserveStateEnum.Null)).OrderBy(a => a.Date).FirstOrDefault();
                if (Result.SpecialService == true)
                {
                    NewFirstTime.Date = Result.Date;
                    for (int i = 0; i < ExistTime.Count(); i++)
                    {
                        if (Result.Time == ExistTime[i].Time)
                        {
                            ExistTime[i].ReserveState = ReserveStateEnum.Null;
                            ExistTime[i].State = true;
                        }
                        else
                        {
                            ExistTime[i].ReserveState = ReserveStateEnum.Disable;
                            ExistTime[i].State = false;
                        }
                    }
                }
                else
                {
                    DateTime Day = Result.Date;
                    var OpenTimes = (await _reservation.FindListByCondition(a => a.Date.Day == Day.Day && a.Date.Month == Day.Month && a.Date.Year == Day.Year)).ToList();
                    if (OpenTimes.Count > 0)
                    {
                        var FindSpecialService = OpenTimes.Where(a => a.SpecialService == true).FirstOrDefault();
                        if (FindSpecialService != null)
                        {
                            var EmptyRound = OpenTimes.Where(a => a.State == ReserveStateEnum.Null && a.SpecialService == false).ToList();
                            for (int i = 0; i < ExistTime.Count(); i++)
                            {
                                ExistTime[i].ReserveState = ReserveStateEnum.Disable;
                                ExistTime[i].State = false;
                            }
                            if (EmptyRound.Count > 0)
                            {
                                for (int i = 0; i < EmptyRound.Count(); i++)
                                {
                                    ExistTime.Where(a => a.Time == EmptyRound[i].Time).FirstOrDefault().State = true;
                                    ExistTime.Where(a => a.Time == EmptyRound[i].Time).FirstOrDefault().ReserveState = ReserveStateEnum.Null;
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < OpenTimes.Count(); i++)
                            {
                                if (OpenTimes[i].State == ReserveStateEnum.Disable)
                                {
                                    ExistTime.Where(a => a.Time == OpenTimes[i].Time).FirstOrDefault().State = false;
                                    ExistTime.Where(a => a.Time == OpenTimes[i].Time).FirstOrDefault().ReserveState = ReserveStateEnum.Disable;
                                }
                                else if (OpenTimes[i].State == ReserveStateEnum.Null)
                                {
                                    ExistTime.Where(a => a.Time == OpenTimes[i].Time).FirstOrDefault().State = true;
                                    ExistTime.Where(a => a.Time == OpenTimes[i].Time).FirstOrDefault().ReserveState = ReserveStateEnum.Null;
                                }
                                else if (OpenTimes[i].State == ReserveStateEnum.Reserved)
                                {
                                    ExistTime.Where(a => a.Time == OpenTimes[i].Time).FirstOrDefault().State = false;
                                    ExistTime.Where(a => a.Time == OpenTimes[i].Time).FirstOrDefault().ReserveState = ReserveStateEnum.Reserved;
                                }
                                else
                                {
                                    ExistTime.Where(a => a.Time == OpenTimes[i].Time).FirstOrDefault().State = false;
                                    ExistTime.Where(a => a.Time == OpenTimes[i].Time).FirstOrDefault().ReserveState = ReserveStateEnum.Disable;
                                }
                            }
                        }
                    }
                }
                NewFirstTime.Times = ExistTime;
                return Ok(new ResultModel
                {
                    Code = Ok().StatusCode,
                    Message = "Success",
                    Data = NewFirstTime
                });

            }
            catch (Exception e)
            {
                return BadRequest(new { message = "خطا سیستمی", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });

            }
        }

        [HttpGet(nameof(GetUserReservation)), AllowAnonymous]
        public async Task<IActionResult> GetUserReservation()
        {
            try
            {
                var Result = (await _Sp.GetReservationList()).ToList();
                return Ok(new ResultModel
                {
                    Code = Ok().StatusCode,
                    Message = "Success",
                    Data = Result
                });

            }
            catch (Exception e)
            {
                return BadRequest(new { message = "خطا سیستمی", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });

            }
        }

        [HttpPost(nameof(UpdateOrSetReservation)), AllowAnonymous]
        public async Task<IActionResult> UpdateOrSetReservation([FromBody] PanelReservationViewModel model)
        {
            try
            {
                var CheckExistReserveTime = await _reservation.FindByCondition(a => a.Date.Day == model.Date.Day && a.Date.Month == model.Date.Month && a.Date.Year == model.Date.Year && a.Time == model.Time);
                if (CheckExistReserveTime != null)
                {
                    CheckExistReserveTime.State = model.State;
                    var Result = await _reservation.Update(CheckExistReserveTime);
                    return Ok(new ResultModel
                    {
                        Code = Ok().StatusCode,
                        Message = "Success",
                        Data = CheckExistReserveTime
                    });
                }
                else
                {
                    TblReservation NewReserveTime = new TblReservation()
                    {
                        State = model.State,
                        Time = model.Time,
                        Date = model.Date,
                        CreatedDatetime = DateTime.Now,
                        SpecialService = model.SpecialService,
                    };
                    var Result = await _reservation.Create(NewReserveTime);
                    return Ok(new ResultModel
                    {
                        Code = Ok().StatusCode,
                        Message = "Success",
                        Data = Result
                    });
                }

            }
            catch (Exception e)
            {
                return BadRequest(new { message = "خطا سیستمی", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });

            }
        }


        [HttpPost(nameof(UpdateAccessLevel)), AllowAnonymous]
        public async Task<IActionResult> UpdateAccessLevel([FromBody] UserDataAccessLevel model)
        {
            try
            {
                var TheUser = await _userService.FindByCondition(a => a.PhoneNumber == model.PhoneNumber);

                if (TheUser == null)
                {
                    return Ok(new ResultModel
                    {
                        Code = NotFound().StatusCode,
                        Message = "Not Found",
                        Data = null
                    });
                }
                else
                {
                    TheUser.UserAccessLevel = model.AccessLevel;
                    var NewData = await _userService.Update(TheUser);
                    return Ok(new ResultModel
                    {
                        Code = Ok().StatusCode,
                        Message = "Success",
                        Data = TheUser
                    });
                }

            }
            catch (Exception e)
            {
                return BadRequest(new { message = "خطا سیستمی", errors = ModelState.Root.Children.Where(a => a.ValidationState != ModelValidationState.Valid).Select(a => a.Errors) });

            }
        }


        //Pescription


        [HttpPost(nameof(AddPescription)), AllowAnonymous]
        public async Task<IActionResult> AddPescription([FromBody] PescriptionViewModel model)
        {
            TblPescription NewPescription = new TblPescription()
            {
                CreatedDatetime = DateTime.Now,
                Description = model.Description,
                OpgImageId = model.OpgImageId != null ? model.OpgImageId : null,
                IsDelete = false,
                Title = model.Title,
                Userid = model.Userid,

            };
            try
            {
                var Result = await _Pescription.Create(NewPescription);

                return Ok(new ResultModel()
                {
                    Code = 200,
                    Message = "Success",
                    Data = Result,

                });
            }
            catch
            {
                return NotFound(new ResultModel()
                {
                    Code = 400,
                    Message = "NotFound",
                    Data = null,

                });
            }

        }

        [HttpGet(nameof(GetAllUsers)), AllowAnonymous]
        public async Task<IActionResult> GetAllUsers()
        {

            try
            {
                var Result = (await _userService.FindListByCondition(a => a.UserAccessLevel == AccessLevel.User)).ToList();

                return Ok(new ResultModel()
                {
                    Code = 200,
                    Message = "Success",
                    Data = Result,

                });
            }
            catch
            {
                return NotFound(new ResultModel()
                {
                    Code = 400,
                    Message = "NotFound",
                    Data = null,

                });
            }

        }

        [HttpPost(nameof(GetuserPescription)), AllowAnonymous]
        public async Task<IActionResult> GetuserPescription(string phonenumber)
        {
            var TheUser = await _userService.FindByCondition(a => a.PhoneNumber == phonenumber);
            var Result = (await _Pescription.FindListByCondition(a => a.Userid == TheUser.Id)).ToList();
            if (TheUser != null)
            {
                return Ok(new ResultModel()
                {
                    Code = 200,
                    Message = "Success",
                    Data = new
                    {
                        pescription = Result,
                        userinfo = TheUser
                    },

                });
            }
            else
            {
                return NotFound(new ResultModel()
                {
                    Code = 404,
                    Message = "Not Found",
                    Data = null,

                });
            }

        }
    }




}





