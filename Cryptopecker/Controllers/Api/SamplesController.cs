﻿using DataAccessLayer.Entity;
using Dentist.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServicesLayer.Contract;
using System;
using System.Threading.Tasks;

namespace Dentist.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class SamplesController : ControllerBase
    {
        ISampleService _Sample;
        public SamplesController(ISampleService Sample)
        {
            _Sample = Sample;
        }

        [HttpPost(nameof(CreateSample)), AllowAnonymous]
        public async Task<IActionResult> CreateSample([FromBody] SampleViewModel model)
        {
            try
            {
                TblSample NewProduct = new TblSample()
                {
                    Title = model.Title,
                    TitleEn = model.TitleEn,
                    CreatedDatetime = DateTime.Now,
                    Description = model.Description,
                    DescriptionEn = model.DescriptionEn,
                    Image1 = model.Image1,
                    Image2 = model.Image2 != 0 ? model.Image2 : null,
                    Image3 = model.Image3 != 0 ? model.Image3 : null,
                    Image4 = model.Image4 != 0 ? model.Image4 : null,

                };
                var Result = await _Sample.Create(NewProduct);
                return Ok(new ResultModel
                {
                    Code = Ok().StatusCode,
                    Message = "success",
                    Data = Result
                });
            }
            catch (Exception)
            {
                return NotFound(new ResultModel
                {
                    Code = NotFound().StatusCode,
                    Message = "Not Found",
                    Data = null
                });
            }
        }


        [HttpPost(nameof(UpdateSample)), AllowAnonymous]
        public async Task<IActionResult> UpdateSample([FromBody] SampleViewModel model)
        {
            try
            {
                var TheSample = await _Sample.GetSampleWithId(model.Id);
                TheSample.Title = model.Title;
                TheSample.TitleEn = model.TitleEn;
                TheSample.CreatedDatetime = DateTime.Now;
                TheSample.Description = model.Description;
                TheSample.DescriptionEn = model.DescriptionEn;
                TheSample.Image1 = model.Image1;
                TheSample.Image2 = model.Image2 != 0 ? model.Image2 : null;
                TheSample.Image3 = model.Image3 != 0 ? model.Image3 : null;
                TheSample.Image4 = model.Image4 != 0 ? model.Image4 : null;
                TheSample.Image4 = model.Image4 != 0 ? model.Image4 : null;
                TheSample.Image4 = model.Image4 != 0 ? model.Image4 : null;


                var Result = await _Sample.Update(TheSample);
                return Ok(new ResultModel
                {
                    Code = Ok().StatusCode,
                    Message = "success",
                    Data = Result
                });
            }
            catch (Exception)
            {
                return NotFound(new ResultModel
                {
                    Code = NotFound().StatusCode,
                    Message = "Not Found",
                    Data = null
                });
            }
        }


        [HttpGet(nameof(GetAllSample)), AllowAnonymous]
        public async Task<IActionResult> GetAllSample()
        {
            try
            {
                var Result = await _Sample.FindAll();
                return Ok(new ResultModel
                {
                    Code = Ok().StatusCode,
                    Message = "success",
                    Data = Result
                });
            }
            catch (Exception)
            {
                return NotFound(new ResultModel
                {
                    Code = NotFound().StatusCode,
                    Message = "Not Found",
                    Data = null
                });
            }
        }


        [HttpGet(nameof(GetSample)), AllowAnonymous]
        public async Task<IActionResult> GetSample(int Id)
        {
            try
            {
                var Result = await _Sample.GetSampleWithId(Id);
                return Ok(new ResultModel
                {
                    Code = Ok().StatusCode,
                    Message = "success",
                    Data = Result
                });
            }
            catch (Exception)
            {
                return NotFound(new ResultModel
                {
                    Code = NotFound().StatusCode,
                    Message = "Not Found",
                    Data = null
                });
            }
        }

    }
}
