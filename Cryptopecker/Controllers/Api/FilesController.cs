﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer.Entity;
using Dentist.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServicesLayer.Contract;
using ServicesLayer.Services.Helper;

namespace Dentist.Controllers.Api
{
    [ApiVersion("1.0")]
    [ApiExplorerSettings(GroupName = "v1")]
    [AllowAnonymous]
    public class FilesController : BaseController
    {
        //FilePath
        string BaseUrl = "https://files.karmaagy.com/";
        private readonly IUserUploadFileService _UploadFile;
        private readonly IUserService _UserService;
        //FilePath
        const String folderName = "UploadFiles";
        //readonly String folderPath = Path.Combine(Directory.GetCurrentDirectory(), folderName);
        readonly string folderPath = "../files.karmaagy.com/UploadFiles";

        #region Property
        private readonly IMapper _Mapper;
        #endregion

        #region Constructor
        public FilesController(IMapper Mapper, IUserUploadFileService UploadFile)
        {
            _UploadFile = UploadFile;
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            _Mapper = Mapper;
        }
        #endregion

        public class SubThingPostModel
        {
            public IFormFile File { get; set; }

        }

        [AllowAnonymous]
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> Upload([FromForm] SubThingPostModel model)
        {
            //[FromForm] doesn't change anything
            string FileName = "";
            using (var fileContentStream = new MemoryStream())
            {

                await model.File.CopyToAsync(fileContentStream);

                if (System.IO.File.Exists(folderPath + "/" + model.File.FileName))
                {
                    string Code = Helpers.RandomString(6);
                    FileName = Code + model.File.FileName;
                    await System.IO.File.WriteAllBytesAsync(Path.Combine(folderPath, FileName), fileContentStream.ToArray());
                }
                else
                {
                    FileName = model.File.FileName;
                    await System.IO.File.WriteAllBytesAsync(Path.Combine(folderPath, FileName), fileContentStream.ToArray());
                }

            }
            TblUserUploadFile NewFile = (new TblUserUploadFile
            {
                CreatedDatetime = DateTime.Now,
                Filename = FileName,
                FilePath = BaseUrl + folderName + "/" + FileName,

            });



            var thefile = await _UploadFile.Create(NewFile);
            return Ok(new ResultModel
            {
                Code = Ok().StatusCode,
                Message = "Success",
                Data = thefile
            });

        }

        [HttpGet("Download/{filename}", Name = "myFile")]
        public async Task<IActionResult> Download([FromRoute] String filename)
        {
            var filePath = Path.Combine(folderPath, filename);
            if (System.IO.File.Exists(filePath))
            {
                return File(await System.IO.File.ReadAllBytesAsync(filePath), "application/octet-stream", filename);
            }
            return NotFound();
        }
    }
}
