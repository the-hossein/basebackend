﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Tiola.ViewModel;
using ServicesLayer.Services.Helper;
using Dentist.ViewModel;
using Dentist;
using Dentist.ViewModel;


namespace Dentist.Controllers
{
    public class AuthController : Controller
    {
        private readonly IConfiguration _config;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;
        private readonly ILogger<AuthController> _logger;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public AuthController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signManager,
            IMapper mapper, ILogger<AuthController> logger, IConfiguration config, RoleManager<ApplicationRole> roleManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _signInManager = signManager;
            _mapper = mapper;
            _config = config;
            _logger = logger;
        }



        #region Change Password
        // GET: Transaction/AddOrEdit(Insert)
        // GET: Transaction/AddOrEdit/5(Update)
        [Helper.NoDirectAccessAttribute]
        [Authorize]
        public async Task<IActionResult> ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> ChangePassword([Bind("OldPassword,NewPassword,ConfirmPassword")] ChangePasswordViewModel model)
        {
            string Error = "";


            if (model.NewPassword != model.ConfirmPassword)
                Error += "رمز عبور وارد شده مطابقت ندارد";

            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(_userManager.GetUserId(User));
                var IsSuccess = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                user.RememberPassword = model.NewPassword;
                if (IsSuccess.Succeeded)
                    return Json(new { isValid = true });
                else
                {
                    foreach (var error in IsSuccess.Errors)
                    {
                        Error += error.Description + Environment.NewLine;
                    }


                }

            }
            {
                Error += "ورود های وارد شده اشتباه میباشد";
            }

            return Json(new { isValid = false, html = Error });
        }
        #endregion

        public IActionResult Logout()
        {
            _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Auth");
        }

        public IActionResult AccessDenied()
        {
            return View(new { errorCode = 403 });
        }
    }



}
