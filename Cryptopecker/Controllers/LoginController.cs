﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using ServicesLayer.Contract;
using DataAccessLayer.Enums;
using ServicesLayer.Services.Helper;
using DataAccessLayer.Entity;
using Dentist.Controllers;
using Dentist.ViewModel;

namespace CRMVistor.Controllers
{
    public class LoginController : Controller
    {
        private readonly IConfiguration _config;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;
        private readonly ILogger<AuthController> _logger;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IUserService _userService;
        public LoginController(IUserService userService, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signManager,
            IMapper mapper, ILogger<AuthController> logger, IConfiguration config, RoleManager<ApplicationRole> roleManager)
        {
            _userService = userService;
            _roleManager = roleManager;
            _userManager = userManager;
            _signInManager = signManager;
            _mapper = mapper;
            _config = config;
            _logger = logger;
        }


        //public IActionResult Index()
        //{
        //    return View();
        //}

        [AllowAnonymous]
        public IActionResult Index(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            RequestLoginViewModel login = new();

            login.ReturnUrl = returnUrl;
            return View(login);
        }

        #region Ajax



        [HttpGet]

        public ActionResult signin(SigninModel model, string ReturnUrl)
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            model.ReturnUrl = ReturnUrl;

            return View(model);
        }


        //[HttpPost]
        //public async Task<JsonResult> signin(string PhoneNumber, string ReturnUrl)
        //{
        //    if (!ModelState.IsValid)
        //        return Json(new { result = false, Status = 0, errorMessage = "شماره همراه وارد شده اشتباه میباشد" });

        //    if (User.Identity.IsAuthenticated)
        //        return Json(new { result = false, Status = 0, errorMessage = "شما از قبل اهراز هویت شده اید" });
        //    else
        //    {
        //        try
        //        {

        //            PhoneNumber = ServicesLayer.Services.Helper.Helpers.PersianToEnglinshNumeric(PhoneNumber);

        //            var FoundUser = await _userService.FindByCondition(a => a.PhoneNumber == PhoneNumber);


        //            if (FoundUser == null)
        //            {
        //                return Json(new { result = false, Status = 0, errorMessage = "شماره شما داخل سیستم ثبت نشده است" });
        //            }

        //            var ResultOtp = await _otpService.OTPSend(PhoneNumber, null, null);

        //            if (ResultOtp.Contains("کد با موفقیت ارسال شد"))
        //            {
        //                string Token = ServicesLayer.Services.Helper.CipherHashing.Encrypt(FoundUser.Id.ToString(), CyberPasswordEncription.Encryption.GetDescription());

        //                Helpers.SetCookie(Response, "Token", Token, null);
        //                Helpers.SetCookie(Response, "ResultOtp", ResultOtp, null);

        //                return Json(new { result = true, Status = 2, errorMessage = "کاربر جدید" });
        //            }
        //            else
        //            {
        //                return Json(new { result = false, Status = 0, errorMessage = ResultOtp });

        //            }




        //        }
        //        catch (Exception)
        //        {
        //            return Json(new { result = false, Status = -1, errorMessage = "خطا در عملیات" });
        //        }

        //    }

        //}




        [HttpGet]
        public ActionResult password(string PhoneNumber)
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            if (Request.Cookies["Token"] == null)
                return RedirectToAction("Index", "Home");

            ViewBag.PhoneNumber = PhoneNumber;
            return View();
        }


        [HttpPost]
        public async Task<ActionResult> PasswordMethod(string Password)
        {

            string Token = Request.Cookies["Token"];

            Guid UserId = Guid.Parse(ServicesLayer.Services.Helper.CipherHashing.Decrypt(Token, CyberPasswordEncription.Encryption.GetDescription()));

            var FoundUser = await _userService.FindByCondition(a => a.Id == UserId);


            var SignIn = await _signInManager.PasswordSignInAsync(FoundUser, Password, true, false);
            if (SignIn.Succeeded)
            {
                return Json(new { result = true, Status = 1, errorMessage = "با موفقیت لاگین شده اید" });

            }
            else
            {
                return Json(new { result = false, Status = -1, errorMessage = "خطا در لاگین" });

            }

        }



        //[HttpGet]
        //public async Task<ActionResult> code()
        //{
        //    if (User.Identity.IsAuthenticated)
        //        return RedirectToAction("Index", "Home");

        //    if (Request.Cookies["Token"] == null)
        //        return RedirectToAction("Index", "Home");



        //    string Token = Request.Cookies["Token"].ToString();

        //    Guid UserId = Guid.Parse(ServicesLayer.Services.Helper.CipherHashing.Decrypt(Token, CyberPasswordEncription.Encryption.GetDescription()));

        //    var FoundUser = await _userService.FindByCondition(a => a.Id == UserId);

        //    if (Request.Cookies["ResultOtp"] != null)
        //    {
        //        ViewBag.ResultOTP = Request.Cookies["ResultOtp"];

        //        Response.Cookies.Append("ResultOtp", "", new CookieOptions()
        //        {
        //            Path = "/",
        //            HttpOnly = false,
        //            IsEssential = true, //<- there
        //            Expires = DateTime.Now.AddDays(-1),
        //        });

        //        var amir = Request.Cookies["ResultOtp"];
        //    }
        //    else
        //    {
        //        var ResultOtp = await _otpService.OTPSend(FoundUser.PhoneNumber, null, null);
        //        ViewBag.ResultOTP = ResultOtp;
        //    }



        //    return View();
        //}

        private void AddRolesToClaims(List<Claim> claims, IEnumerable<string> roles)
        {
            foreach (var role in roles)
            {
                var roleClaim = new Claim(ClaimTypes.Role, role);
                claims.Add(roleClaim);
            }
        }

        //[HttpPost]
        //public async Task<ActionResult> SigninByCode(string OTPCode)
        //{
        //    string Token = Request.Cookies["Token"].ToString();






        //    if (String.IsNullOrEmpty(OTPCode))
        //        return Json(new { result = false, Status = -2, errorMessage = "لطفا کد را وارد نمایید" });
        //    else
        //    {



        //        Guid UserId = Guid.Parse(ServicesLayer.Services.Helper.CipherHashing.Decrypt(Token, CyberPasswordEncription.Encryption.GetDescription()));
        //        var FoundUser = await _userService.FindByCondition(a => a.Id == UserId);
        //        var FoundOtp = await _otpService.OtpCheck(FoundUser.PhoneNumber, OTPCode);

        //        if (FoundOtp.StatusCode != 200)
        //        {
        //            return Json(new { result = false, Status = -3, errorMessage = "کد وارد شده اشتباه میباشد" });
        //        }
        //        else
        //        {
        //            var claims = new List<Claim>
        //                            {
        //                                new Claim(JwtRegisteredClaimNames.Sub, FoundUser.UserName),
        //                                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
        //                                new Claim(ClaimTypes.NameIdentifier, FoundUser.UserName.ToString())
        //                            };

        //            // Get User roles and add them to claims
        //            var roles = await _userManager.GetRolesAsync(FoundUser);
        //            AddRolesToClaims(claims, roles);

        //            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
        //            var token = new JwtSecurityToken(
        //                issuer: _config["Jwt:Issuer"],
        //                audience: _config["Jwt:Audience"],
        //                expires: DateTime.Now.AddDays(5),
        //                claims: claims,
        //                signingCredentials: new Microsoft.IdentityModel.Tokens.SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
        //            );

        //            await _signInManager.SignInAsync(FoundUser, true, null);



        //            return Json(new { result = true, Status = 2, errorMessage = "با موفقیت لاگین شده اید" });

        //        }
        //    }
        //}


        //[HttpPost]
        //public async Task<ActionResult> SendAgainCode(bool Send)
        //{
        //    string Token = Request.Cookies["Token"].ToString();

        //    Guid UserId = Guid.Parse(ServicesLayer.Services.Helper.CipherHashing.Decrypt(Token, CyberPasswordEncription.Encryption.GetDescription()));

        //    var FoundUser = await _userService.FindByCondition(a => a.Id == UserId);

        //    var ResultOtp = await _otpService.OTPSend(FoundUser.PhoneNumber, null, null);
        //    ViewBag.ResultOTP = ResultOtp;

        //    return Json(new { result = true, errorMessage = ResultOtp });
        //}

        #endregion
    }
}
