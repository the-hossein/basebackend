﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServicesLayer.Common.Helper;

namespace Dentist.Controllers
{
    public class ValidatorController : Controller
    {
        #region Constractor

        public ValidatorController()
        {

        }

        #endregion


        //Add it Into ModelView //[Remote(action: "VerifyNationalCode", controller: "Validator", AdditionalFields = nameof(NationalCode))]

        [AcceptVerbs("GET", "POST")]
        public IActionResult VerifyNationalCode(string NationalCode)
        {
            if (!Helpers.IsValidNationalCode(NationalCode))
            {
                return Json($"کد ملی وارد شده اشتباه میباشد");
            }

            return Json(true);
        }


        public IActionResult VerifyPhoneNumber(string PhoneNumber)
        {
            if (!Helpers.IsValidPhoneNumber(PhoneNumber))
            {
                return Json($"شماره همراه وارد شده اشتباه میباشد");
            }

            return Json(true);
        }
    }
}
