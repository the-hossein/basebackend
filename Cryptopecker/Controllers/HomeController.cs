﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RepositoryLayer;
using Dentist.Models;
using ServicesLayer.Contract;
using DataAccessLayer.ViewModel.StoredProcedures;
using System.Security.Claims;
using Dentist.ViewModel;
using DataAccessLayer.Enums;
using ServicesLayer.Services.Helper;

namespace Dentist.Controllers
{
    public class HomeController : Controller
    {
        private readonly SignInManager<ApplicationUser> _SignIn;
        private readonly RoleManager<ApplicationRole> _Roles;
        private readonly UserManager<ApplicationUser> _User;
        private readonly IMapper _mapper;
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _config;
        private readonly ApplicationContext _context;
        private readonly IStoredProcedures _storedProcedures;
        private readonly IUserService _userService;


        public HomeController(SignInManager<ApplicationUser> SignIn, UserManager<ApplicationUser> User, RoleManager<ApplicationRole> Roles, IMapper mapper, ILogger<HomeController> logger, IConfiguration config, ApplicationContext context, IStoredProcedures storedProcedures, IUserService userService)
        {
            _Roles = Roles;
            _User = User;
            _SignIn = SignIn;
            _logger = logger;
            _mapper = mapper;
            _config = config;
            _context = context;
            _storedProcedures = storedProcedures;
            _userService = userService;
        }

        public async Task<IActionResult> Index()
        {
         
            return View();
        }

     
    }
}
