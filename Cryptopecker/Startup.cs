using RepositoryLayer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using IocLayer;
using Microsoft.AspNetCore.Http;
using Dentist.Versioning;
using Dentist.Filters;
using System.Security.Claims;
using ElmahCore.Mvc;
using ElmahCore.Sql;
using Hangfire;
using Hangfire.MemoryStorage;
using HangfireBasicAuthenticationFilter;
using Microsoft.AspNetCore.Authentication.Cookies;
using DataAccessLayer.Entity;
using ElmahCore;
using System.Diagnostics;
using WebMarkupMin.AspNetCore6;
using WebMarkupMin.AspNet.Common.Compressors;

using System.Collections.Generic;
using System.IO.Compression;

namespace Dentist
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        private void CheckSameSite(HttpContext httpContext, CookieOptions options)
        {
            if (options.SameSite == SameSiteMode.None)
            {
                var userAgent = httpContext.Request.Headers["User-Agent"].ToString();
                options.SameSite = SameSiteMode.Unspecified;
            }
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add WebMarkupMin services.
            services.AddWebMarkupMin(options =>
            {
                options.AllowMinificationInDevelopmentEnvironment = true;
                options.AllowCompressionInDevelopmentEnvironment = true;
            })
                .AddHtmlMinification(opt =>
                {
                    opt.MinificationSettings.AttributeQuotesRemovalMode = WebMarkupMin.Core.HtmlAttributeQuotesRemovalMode.KeepQuotes;
                })
                .AddXmlMinification()
                .AddHttpCompression(options =>
                {
                    options.CompressorFactories = new List<ICompressorFactory>
                    {

                        /*new BrotliCompressorFactory(new BrotliCompressionSettings
                        {
                            Level = 1
                        }),*/
                        new DeflateCompressorFactory(new DeflateCompressionSettings
                        {
                            Level = CompressionLevel.Fastest
                        }),
                        new GZipCompressorFactory(new GZipCompressionSettings
                        {
                            Level = CompressionLevel.Fastest
                        })
                    };
                });

            services.AddCors(o => o.AddPolicy("AllowCors", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));


            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
                options.OnAppendCookie = cookieContext =>
                    CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
                options.OnDeleteCookie = cookieContext =>
                    CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
            });

            services.AddSession(options =>
            {
                options.Cookie.IsEssential = true;
                options.Cookie.SameSite = SameSiteMode.Unspecified;
            });

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 1;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;
                options.User.RequireUniqueEmail = false;
                options.ClaimsIdentity.UserIdClaimType = ClaimTypes.NameIdentifier;
            });

            services.AddAutoMapper(typeof(Startup));
            services.AddMapping();

            services.AddDatabaseDeveloperPageExceptionFilter();
            services.AddMvc(options => options.EnableEndpointRouting = false).SetCompatibilityVersion(CompatibilityVersion.Latest).ConfigureApiBehaviorOptions(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    var problems = new CustomBadRequest(context);
                    return new BadRequestObjectResult(problems);
                };
            }).AddViewLocalization(Microsoft.AspNetCore.Mvc.Razor.LanguageViewLocationExpanderFormat.Suffix).AddDataAnnotationsLocalization();
            services.AddRazorPages()
                .AddMvcOptions(options =>
                {
                    options.MaxModelValidationErrors = 50;
                    options.ModelBindingMessageProvider.SetValueMustNotBeNullAccessor(
                        _ => "The field is required.");
                }).AddRazorRuntimeCompilation();



            services.AddControllersWithViews(options => options.EnableEndpointRouting = false).AddRazorRuntimeCompilation();
            services.AddOptions();
            services.AddDbContext<ApplicationContext>(options =>
                options.UseLazyLoadingProxies().UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddDefaultIdentity<ApplicationUser>().AddRoles<ApplicationRole>().AddEntityFrameworkStores<ApplicationContext>().AddDefaultTokenProviders();

            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.ApiVersionReader =
                    new HeaderApiVersionReader("X-API-Version");
            });

            #region Logger

            services.AddLogging();

            #endregion

            #region Repositories Instance
            services.AddRepositoryInstance();
            #endregion

            #region Dependency Injection
            services.AddServicesDependency();
            #endregion

            #region Swagger
            services.AddSwaggerGen(swagger =>
            {

                swagger.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = " CRMVistor v1 API's",
                    Description = $"CRMVistor API's for integration with UI \r\n\r\n � Copyright {DateTime.Now.Year} JK. All rights reserved."
                });
                swagger.SwaggerDoc("v2", new OpenApiInfo
                {
                    Version = "v2",
                    Title = "CRMVistor v2 API's",
                    Description = $"CRMVistor API's for integration with UI \r\n\r\n � Copyright {DateTime.Now.Year} JK. All rights reserved."
                });
                swagger.ResolveConflictingActions(a => a.First());
                swagger.OperationFilter<RemoveVersionFromParameterv>();
                swagger.DocumentFilter<ReplaceVersionWithExactValueInPath>();

                #region Enable Authorization using Swagger (JWT) 
                swagger.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                });

                swagger.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
        {
              new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference
                    {
                        Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                    }
                },
                new string[] {}

        }
                });
                #endregion
            });
            #endregion

            #region Swagger Json property Support
            services.AddSwaggerGenNewtonsoftSupport();

            #endregion

            #region JWT 


            // Adding Authentication  
            services.AddAuthentication(options =>
            {

            })
                .AddCookie(options =>
                {
                    options.LoginPath = "/Panel/Index";
                    options.Events.OnRedirectToLogin = context =>
                    {
                        context.Response.Headers["Location"] = context.RedirectUri;
                        context.Response.StatusCode = 401;
                        return Task.CompletedTask;
                    };
                    options.CookieManager = new ChunkingCookieManager();
                    options.Cookie.HttpOnly = false;
                    options.Cookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.Unspecified;
                    options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                })

                // Adding Jwt Bearer  
                .AddJwtBearer(options =>
                {
                    options.SaveToken = true;
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidAudience = Configuration["Jwt:Audience"],
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };
                });
            #endregion

            services.Configure<SecurityStampValidatorOptions>(options =>
            {

                //options.ValidationInterval = TimeSpan.FromSeconds(10);
            });


            #region HangFire

            services.AddHangfire(config =>
                config.SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                    .UseSimpleAssemblyNameTypeSerializer()
                    .UseDefaultTypeSerializer()
                    .UseMemoryStorage());

            #endregion


            services.AddElmah<SqlErrorLog>(options =>
            {
                options.ConnectionString =
                    Configuration.GetConnectionString("DefaultConnection");
                options.OnPermissionCheck = context => context.User.Identity.IsAuthenticated;
                options.Notifiers.Add(new MyNotifier());
                options.Filters.Add(new CmsErrorLogFilter());
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.IsEssential = true;
                // Cookie settings 
                options.Cookie.HttpOnly = false;
                options.LoginPath = "/Panel/Index"; // If the LoginPath is not set here, ASP.NET Core will default to /Account/Login 
                options.LogoutPath = "/Auth/Logout"; // If the LogoutPath is not set here, ASP.NET Core will default to /Account/Logout 
                options.AccessDeniedPath = "/Auth/AccessDenied"; // If the AccessDeniedPath is not set here, ASP.NET Core will default to /Account/AccessDenied 

                options.Cookie.Name = "AuthIdentity";
                options.ExpireTimeSpan = TimeSpan.FromHours(12);
                options.SlidingExpiration = false; // the cookie would be re-issued on any request half way through the ExpireTimeSpan
                                                   //options.Cookie.Expiration = TimeSpan.FromDays(5);
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseStatusCodePages(context =>
            {

                var response = context.HttpContext.Response;
                if (response.StatusCode != (int)HttpStatusCode.Unauthorized &&
                    response.StatusCode != (int)HttpStatusCode.Forbidden && response.StatusCode != (int)HttpStatusCode.InternalServerError)
                    response.Redirect($"/Home/HandleError?StatusCode={response.StatusCode}");
                //else
                //    response.Redirect($"/Auth");
                return Task.CompletedTask;
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "API v1");
                    c.SwaggerEndpoint("/swagger/v2/swagger.json", "API v2");

                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture(new System.Globalization.CultureInfo("fa-IR")),
                SupportedCultures = new[]
                {
                    new System.Globalization.CultureInfo("fa-IR")
                },
                SupportedUICultures = new[]
                {
                    new System.Globalization.CultureInfo("fa-IR")
                }
            });

            app.UseCors("AllowCors");

            // UseCors must be called before UseResponseCaching           
            app.UseResponseCaching();

            app.UseHttpsRedirection();
            app.UseCookiePolicy();
            app.UseRouting();

            app.UseWebMarkupMin();

            app.UseStaticFiles();

            app.UseAuthorization();
            app.UseAuthentication();
            app.UseSession();

            app.UseMvc(route =>
            {
                route.MapRoute(
                    name: "MyArea",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                route.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseHangfireDashboard("/jobs", new DashboardOptions
            {
                Authorization = new[] { new HangfireCustomBasicAuthenticationFilter{User="Holyfeast",Pass="Amir123amir" }
                }
            });
            app.UseElmah();
            app.UseHangfireServer();
            //RecurringJob.AddOrUpdate<IDailyTasksService>("Run every 5 minute", x => x.DoTasks(), "*/1 * * * *");
        }

        public class CmsErrorLogFilter : IErrorFilter
        {
            public void OnErrorModuleFiltering(object sender, ExceptionFilterEventArgs args)
            {
                if (args.Exception.GetBaseException() is FileNotFoundException)
                    args.Dismiss();
                if (args.Context is HttpContext httpContext)
                    if (httpContext.Response.StatusCode == 404)
                        args.Dismiss();
            }
        }
        public class MyNotifier : IErrorNotifier
        {
            public void Notify(Error error)
            {
                Debug.WriteLine(error.Message);
            }

            public string Name => "my";
        }
    }
}
