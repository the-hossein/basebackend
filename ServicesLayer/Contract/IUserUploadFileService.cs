﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entity; 
using DataAccessLayer.ViewModel;

namespace ServicesLayer.Contract
{
    public interface IUserUploadFileService
    {
        Task<IQueryable<TblUserUploadFile>> FindListByCondition(Expression<Func<TblUserUploadFile, bool>> Condition);
        Task<TblUserUploadFile> FindByCondition(Expression<Func<TblUserUploadFile, bool>> Condition);
        Task<int> GetIdWithCondition(Expression<Func<TblUserUploadFile, bool>> Condition);
        Task<bool> Create(int Id);
        Task<bool> ChangeUserUploadFile(TblUserUploadFile UserUploadFile);
        Task<TblUserUploadFile> GetUserUploadFileWithId(int Id);
        Task<bool> Delete(int Id);
        Task<bool> Update(TblUserUploadFile UserUploadFile);

        List<TblUserUploadFile> GetAllFilteredByPagingAndSearch(Expression<Func<TblUserUploadFile, bool>> filter, int Take,
            int Skip, string search = "");

        int GetAllFilteredByCount(Expression<Func<TblUserUploadFile, bool>> filter);

        int GetAllCount();

        Task<TblUserUploadFile> Create(TblUserUploadFile NewRecord);


        Task<TblUserUploadFile> UploadFile(UploadFileViewModelRequest model);

        Task<TblUserUploadFile> UploadVideo(UploadVideoViewModelRequest model);
        Task<TblUserUploadFile> UploadExcel(UploadFileViewModelRequest model);
        Task<TblUserUploadFile> UploadExcelWithByteArray(UploadByteArrayFileViewModelRequest model);



    }
}