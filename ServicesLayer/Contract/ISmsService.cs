﻿using DataAccessLayer.Entity;
using DataAccessLayer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Contract
{
    public interface ISmsService
    {
        Task<IQueryable<TblSms>> FindListByCondition(Expression<Func<TblSms, bool>> Condition);
        Task<TblSms> FindByCondition(Expression<Func<TblSms, bool>> Condition);
        Task<int> GetIdWithCondition(Expression<Func<TblSms, bool>> Condition);
        Task<bool> CreateSMS(int Id);
        Task<bool> ChangeSMS(TblSms SMS);
        Task<TblSms> GetSMSWithId(int Id);
        Task<bool> Delete(int Id);
        Task<bool> Update(TblSms SMS);

        List<TblSms> GetAllFilteredByPagingAndSearch(Expression<Func<TblSms, bool>> filter, int Take,
            int Skip, string search = "");

        int GetAllFilteredByCount(Expression<Func<TblSms, bool>> filter);

        int GetAllCount();

        Task<bool> CreateSMS(TblSms NewRecord);

        Task<TblSms> SendMessageKavenegar(string phoneNumber, string template, Guid? UserId, SmsTypeEnum smsType , string token );
    }
}
