﻿using DataAccessLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace ServicesLayer.Contract
{
    public interface ICounselingService
    {
        //List<TblCounselingComment> GetAllFilteredByPaging(Expression<Func<TblCounselingComment, bool>> Condition, int Take, int Skip, string Search);
        Task<int?> GetIdWithCondition(Expression<Func<TblCounseling, bool>> Condition);
        //Task<IQueryable<TblCounselingComment>> FindAll();
        //Task<IQueryable<TblCounselingComment>> FindListByCondition(Expression<Func<TblCounselingComment, bool>> Condition);
        Task<TblCounseling> FindByCondition(Expression<Func<TblCounseling, bool>> Condition);
        //Task<TblCounselingComment> GetCounseling(int Id);
        //Task<bool> Create(TblCounselingComment NewCounselingComment);
        //Task<bool> Update(TblCounselingComment UpdateCounselingComment);
        //Task<bool> Delete(int Id);
        Task<IQueryable<TblCounseling>> FindListByCondition(Expression<Func<TblCounseling, bool>> Condition);


        Task<IQueryable<TblCounseling>> FindAll();

        Task<TblCounseling> Create(TblCounseling NewRecord);
        Task<bool> Delete(int Id);
        Task<bool> Update(TblCounseling Counseling);

        Task<TblCounseling> GetCounselingWithId(int Id);
    }
}
