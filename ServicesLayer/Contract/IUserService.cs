﻿using DataAccessLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace ServicesLayer.Contract
{
    public interface IUserService
    {
        //List<TblUserComment> GetAllFilteredByPaging(Expression<Func<TblUserComment, bool>> Condition, int Take, int Skip, string Search);
        Task<Guid?> GetIdWithCondition(Expression<Func<ApplicationUser, bool>> Condition);
        //Task<IQueryable<TblUserComment>> FindAll();
        //Task<IQueryable<TblUserComment>> FindListByCondition(Expression<Func<TblUserComment, bool>> Condition);
        Task<ApplicationUser> FindByCondition(Expression<Func<ApplicationUser, bool>> Condition);
        //Task<TblUserComment> GetUser(int Id);
        //Task<bool> Create(TblUserComment NewUserComment);
        //Task<bool> Update(TblUserComment UpdateUserComment);
        //Task<bool> Delete(int Id);
        Task<IQueryable<ApplicationUser>> FindListByCondition(Expression<Func<ApplicationUser, bool>> Condition);


        Task<IQueryable<ApplicationUser>> FindAll();

        Task<bool> Update(ApplicationUser User);
    }
}
