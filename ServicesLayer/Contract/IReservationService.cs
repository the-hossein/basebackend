﻿using DataAccessLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace ServicesLayer.Contract
{
    public interface IReservationService
    {
        //List<TblReservationComment> GetAllFilteredByPaging(Expression<Func<TblReservationComment, bool>> Condition, int Take, int Skip, string Search);
        Task<int?> GetIdWithCondition(Expression<Func<TblReservation, bool>> Condition);
        //Task<IQueryable<TblReservationComment>> FindAll();
        //Task<IQueryable<TblReservationComment>> FindListByCondition(Expression<Func<TblReservationComment, bool>> Condition);
        Task<TblReservation> FindByCondition(Expression<Func<TblReservation, bool>> Condition);
        //Task<TblReservationComment> GetReservation(int Id);
        //Task<bool> Create(TblReservationComment NewReservationComment);
        //Task<bool> Update(TblReservationComment UpdateReservationComment);
        //Task<bool> Delete(int Id);
        Task<IQueryable<TblReservation>> FindListByCondition(Expression<Func<TblReservation, bool>> Condition);


        Task<IQueryable<TblReservation>> FindAll();

        Task<TblReservation> Create(TblReservation NewRecord);
        Task<bool> Delete(int Id);
        Task<bool> Update(TblReservation Reservation);

        Task<TblReservation> GetReservationWithId(int Id);
    }
}
