﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entity;
using DataAccessLayer.Enums;
using DataAccessLayer.ViewModel.StoredProcedures;

namespace ServicesLayer.Contract
{
    public interface IStoredProcedures
    {
        Task<List<SpGetReserveListResponse>> GetReservationList();

    }
}
