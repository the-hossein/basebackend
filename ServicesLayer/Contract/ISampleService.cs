﻿using DataAccessLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace ServicesLayer.Contract
{
    public interface ISampleService
    {
        //List<TblSampleComment> GetAllFilteredByPaging(Expression<Func<TblSampleComment, bool>> Condition, int Take, int Skip, string Search);
        Task<int?> GetIdWithCondition(Expression<Func<TblSample, bool>> Condition);
        //Task<IQueryable<TblSampleComment>> FindAll();
        //Task<IQueryable<TblSampleComment>> FindListByCondition(Expression<Func<TblSampleComment, bool>> Condition);
        Task<TblSample> FindByCondition(Expression<Func<TblSample, bool>> Condition);
        //Task<TblSampleComment> GetSample(int Id);
        //Task<bool> Create(TblSampleComment NewSampleComment);
        //Task<bool> Update(TblSampleComment UpdateSampleComment);
        //Task<bool> Delete(int Id);
        Task<IQueryable<TblSample>> FindListByCondition(Expression<Func<TblSample, bool>> Condition);


        Task<IQueryable<TblSample>> FindAll();

        Task<TblSample> Create(TblSample NewRecord);
        Task<bool> Delete(int Id);
        Task<bool> Update(TblSample Sample);

        Task<TblSample> GetSampleWithId(int Id);
    }
}
