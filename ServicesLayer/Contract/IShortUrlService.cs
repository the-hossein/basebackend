﻿using DataAccessLayer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Contract
{
    public interface IShortUrlService
    {
        Task<string> Create(ShortUrlViewModel NewShortUrl);
 
    }
}
