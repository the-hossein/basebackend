﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entity;
using DataAccessLayer.ViewModel;

namespace ServicesLayer.Contract
{
    public interface IOtpService
    {
        Task<IQueryable<TblOtp>> FindListByCondition(Expression<Func<TblOtp, bool>> Condition);
        Task<TblOtp> FindByCondition(Expression<Func<TblOtp, bool>> Condition);
        Task<int> GetIdWithCondition(Expression<Func<TblOtp, bool>> Condition);
        Task<bool> CreateOtp(int Id);
        Task<bool> ChangeOtp(TblOtp Otp);
        Task<TblOtp> GetOtpWithId(int Id);
        Task<bool> Delete(int Id);
        Task<bool> Update(TblOtp Otp);

        List<TblOtp> GetAllFilteredByPagingAndSearch(Expression<Func<TblOtp, bool>> filter, int Take,
            int Skip, string search = "");

        int GetAllFilteredByCount(Expression<Func<TblOtp, bool>> filter);

        int GetAllCount();

        Task<bool> CreateOtp(TblOtp NewRecord);

        Task<OtpViewModel> OTPSend(string PhoneNumber, Guid? UserId, Guid? CreatorUserId);

        Task<OtpCheckViewModel> OtpCheck(string PhoneNumber, string Code);
    }
}
