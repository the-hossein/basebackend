﻿using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace ServicesLayer.Contract
{
    public interface ITelegramService
    {
        Task<bool> SendMessageToDynamicGroupPost(string MessageText, string Chatid);
    }
}
