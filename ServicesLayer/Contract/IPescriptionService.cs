﻿using DataAccessLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace ServicesLayer.Contract
{
    public interface IPescriptionService
    {
        //List<TblPescriptionComment> GetAllFilteredByPaging(Expression<Func<TblPescriptionComment, bool>> Condition, int Take, int Skip, string Search);
        Task<int?> GetIdWithCondition(Expression<Func<TblPescription, bool>> Condition);
        //Task<IQueryable<TblPescriptionComment>> FindAll();
        //Task<IQueryable<TblPescriptionComment>> FindListByCondition(Expression<Func<TblPescriptionComment, bool>> Condition);
        Task<TblPescription> FindByCondition(Expression<Func<TblPescription, bool>> Condition);
        //Task<TblPescriptionComment> GetPescription(int Id);
        //Task<bool> Create(TblPescriptionComment NewPescriptionComment);
        //Task<bool> Update(TblPescriptionComment UpdatePescriptionComment);
        //Task<bool> Delete(int Id);
        Task<IQueryable<TblPescription>> FindListByCondition(Expression<Func<TblPescription, bool>> Condition);


        Task<IQueryable<TblPescription>> FindAll();

        Task<TblPescription> Create(TblPescription NewRecord);
        Task<bool> Delete(int Id);
        Task<bool> Update(TblPescription Pescription);

        Task<TblPescription> GetPescriptionWithId(int Id);
    }
}
