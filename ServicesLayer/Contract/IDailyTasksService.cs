﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entity;

namespace ServicesLayer.Contract
{
    public interface IDailyTasksService
    {
        Task DoTasks();
    }
}
