﻿using DataAccessLayer.Entity;
using DataAccessLayer.Models;
using Microsoft.Extensions.Logging;
using RepositoryLayer.Contract;
using ServicesLayer.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Services
{
    public class ReservationService : IReservationService
    {

        private readonly IReservationRepository _Reservation;
        private readonly ILogger<ReservationService> _Logger;
        public ReservationService(IReservationRepository Reservation, ILogger<ReservationService> Logger)
        {
            _Logger = Logger;
            _Reservation = Reservation;
        }

        public async Task<TblReservation> FindByCondition(Expression<Func<TblReservation, bool>> Condition)
        {
            try
            {
                return _Reservation.FindByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<IQueryable<TblReservation>> FindAll()
        {

            try
            {
                return _Reservation.FindAll();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<IQueryable<TblReservation>> FindListByCondition(Expression<Func<TblReservation, bool>> Condition)
        {

            try
            {
                return _Reservation.FindListByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }
        public async Task<int?> GetIdWithCondition(Expression<Func<TblReservation, bool>> Condition)
        {
            try
            {
                var FoundRecord = _Reservation.FindByCondition(Condition);
                if (FoundRecord != null)
                    return FoundRecord.Id;
                else
                    return null;
            }
            catch (Exception)
            {
                Console.Write("This Reservation Is not Exist");
                _Logger.LogInformation("This Reservation Is not Exist");
                return null;
            }

        }

        public async Task<bool> Update(TblReservation UpdateReservation)
        {
            try
            {
                TblReservation FindReservation = await GetReservationWithId(UpdateReservation.Id);
                if (FindReservation != null)
                {
                    _Reservation.Update(UpdateReservation);
                }
                else
                {
                    Console.Write($"This ReservationUploadFile Is not Exist");
                    _Logger.LogInformation($"This ReservationUploadFile Is not Exist");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return false;
            }
        }
        public async Task<bool> Delete(int Id)
        {
            try
            {
                TblReservation FindReservation = await GetReservationWithId(Id);
                if (FindReservation != null)
                {
                    _Reservation.Delete(FindReservation);
                }
                else
                {
                    Console.Write($"This ReservationUploadFile Is not Exist");
                    _Logger.LogInformation($"This ReservationUploadFile Is not Exist");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return false;
            }
        }
        public async Task<TblReservation> GetReservationWithId(int Id)
        {
            try
            {
                TblReservation Reservation = _Reservation.FindByCondition(c => c.Id == Id);
                if (Reservation == null)
                {
                    Console.WriteLine($"Can not Find UserUploadFile With {Id} Id In Db");
                    _Logger.LogInformation($"Can not Find UserUploadFile With {Id} Id In Db");
                    return null;
                }
                return Reservation;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<TblReservation> Create(TblReservation NewRecord)
        {
            try
            {
                if (await GetReservationWithId(NewRecord.Id) == null)
                {
                    TblReservation GetRecord = _Reservation.Create(NewRecord);
                    return GetRecord;
                }
                else
                {
                    Console.WriteLine($"This ClassesCourse Name Is Already Exist => ClassesCourseName = {NewRecord.Id}");
                    _Logger.LogInformation($"This ClassesCourse Name Is Already Exist => ClassesCourseName = {NewRecord.Id}");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }
    }
}
