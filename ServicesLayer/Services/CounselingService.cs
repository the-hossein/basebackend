﻿using DataAccessLayer.Entity;
using DataAccessLayer.Models;
using Microsoft.Extensions.Logging;
using RepositoryLayer.Contract;
using ServicesLayer.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Services
{
    public class CounselingService : ICounselingService
    {

        private readonly ICounselingRepository _Counseling;
        private readonly ILogger<CounselingService> _Logger;
        public CounselingService(ICounselingRepository Counseling, ILogger<CounselingService> Logger)
        {
            _Logger = Logger;
            _Counseling = Counseling;
        }

        public async Task<TblCounseling> FindByCondition(Expression<Func<TblCounseling, bool>> Condition)
        {
            try
            {
                return _Counseling.FindByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<IQueryable<TblCounseling>> FindAll()
        {

            try
            {
                return _Counseling.FindAll();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<IQueryable<TblCounseling>> FindListByCondition(Expression<Func<TblCounseling, bool>> Condition)
        {

            try
            {
                return _Counseling.FindListByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }
        public async Task<int?> GetIdWithCondition(Expression<Func<TblCounseling, bool>> Condition)
        {
            try
            {
                var FoundRecord = _Counseling.FindByCondition(Condition);
                if (FoundRecord != null)
                    return FoundRecord.Id;
                else
                    return null;
            }
            catch (Exception)
            {
                Console.Write("This Counseling Is not Exist");
                _Logger.LogInformation("This Counseling Is not Exist");
                return null;
            }

        }

        public async Task<bool> Update(TblCounseling UpdateCounseling)
        {
            try
            {
                TblCounseling FindCounseling = await GetCounselingWithId(UpdateCounseling.Id);
                if (FindCounseling != null)
                {
                    _Counseling.Update(UpdateCounseling);
                }
                else
                {
                    Console.Write($"This CounselingUploadFile Is not Exist");
                    _Logger.LogInformation($"This CounselingUploadFile Is not Exist");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return false;
            }
        }
        public async Task<bool> Delete(int Id)
        {
            try
            {
                TblCounseling FindCounseling = await GetCounselingWithId(Id);
                if (FindCounseling != null)
                {
                    _Counseling.Delete(FindCounseling);
                }
                else
                {
                    Console.Write($"This CounselingUploadFile Is not Exist");
                    _Logger.LogInformation($"This CounselingUploadFile Is not Exist");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return false;
            }
        }
        public async Task<TblCounseling> GetCounselingWithId(int Id)
        {
            try
            {
                TblCounseling Counseling = _Counseling.FindByCondition(c => c.Id == Id);
                if (Counseling == null)
                {
                    Console.WriteLine($"Can not Find UserUploadFile With {Id} Id In Db");
                    _Logger.LogInformation($"Can not Find UserUploadFile With {Id} Id In Db");
                    return null;
                }
                return Counseling;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<TblCounseling> Create(TblCounseling NewRecord)
        {
            try
            {
                if (await GetCounselingWithId(NewRecord.Id) == null)
                {
                    TblCounseling GetRecord = _Counseling.Create(NewRecord);
                    return GetRecord;
                }
                else
                {
                    Console.WriteLine($"This ClassesCourse Name Is Already Exist => ClassesCourseName = {NewRecord.Id}");
                    _Logger.LogInformation($"This ClassesCourse Name Is Already Exist => ClassesCourseName = {NewRecord.Id}");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }
    }
}
