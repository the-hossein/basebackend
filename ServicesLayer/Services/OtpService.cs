﻿using DataAccessLayer.Entity;
using DataAccessLayer.Enums;
using DataAccessLayer.ViewModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using RepositoryLayer.Contract;
using ServicesLayer.Common;
using ServicesLayer.Common.Helper;
using ServicesLayer.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Services
{
    public class OtpService : IOtpService
    {
        private readonly IOtpRepository _Otprepository;
        private readonly ILogger<OtpService> _logger;
        private readonly ISmsService _sMSService;
        public OtpService(IOtpRepository Otprepository, ILogger<OtpService> logger, ISmsService sMSService)
        {
            _sMSService = sMSService;
            _Otprepository = Otprepository;
            _logger = logger;
        }
        public async Task<IQueryable<TblOtp>> FindListByCondition(Expression<Func<TblOtp, bool>> Condition)
        {

            try
            {
                return _Otprepository.FindListByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return null;
            }
        }
        public async Task<TblOtp> FindByCondition(Expression<Func<TblOtp, bool>> Condition)
        {
            try
            {
                return _Otprepository.FindByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return null;
            }
        }
        public async Task<int> GetIdWithCondition(Expression<Func<TblOtp, bool>> Condition)
        {
            try
            {
                var FoundRecord = _Otprepository.FindByCondition(Condition);
                if (FoundRecord != null)
                    return FoundRecord.Id;
                else
                    return 0;
            }
            catch (Exception e)
            {
                Console.Write($"This Otp Is not Exist");
                _logger.LogInformation($"This Otp Is not Exist");
                return 0;
            }

        }
        public async Task<bool> Update(TblOtp UpdateOtp)
        {
            try
            {
                TblOtp FindOtp = await GetOtpWithId(UpdateOtp.Id);
                if (FindOtp != null)
                {
                    _Otprepository.Update(UpdateOtp);
                }
                else
                {
                    Console.Write($"This Otp Is not Exist");
                    _logger.LogInformation($"This Otp Is not Exist");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return false;
            }
        }
        public async Task<bool> Delete(int Id)
        {
            try
            {
                TblOtp FindOtp = await GetOtpWithId(Id);
                if (FindOtp != null)
                {
                    _Otprepository.Delete(FindOtp);
                }
                else
                {
                    Console.Write($"This Otp Is not Exist");
                    _logger.LogInformation($"This Otp Is not Exist");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return false;
            }
        }
        public async Task<bool> CreateOtp(int Id)
        {
            try
            {
                if (await GetOtpWithId(Id) == null)
                {
                    TblOtp NewOtp = new TblOtp { CreatedDatetime = DateTime.Now };
                    _Otprepository.Create(NewOtp);
                    return true;
                }
                else
                {
                    Console.WriteLine($"This Otp Name Is Already Exist => OtpName = {Id}");
                    _logger.LogInformation($"This Otp Name Is Already Exist => OtpName = {Id}");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return false;
            }
        }

        public async Task<TblOtp> GetOtpWithId(int Id)
        {
            try
            {
                TblOtp FindOtp = _Otprepository.FindByCondition(c => c.Id == Id);
                if (FindOtp == null)
                {
                    Console.WriteLine($"Can not Find Otp With {Id} Id In Db");
                    _logger.LogInformation($"Can not Find Otp With {Id} Id In Db");
                    return null;
                }
                return FindOtp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                _logger.LogInformation(ex.Message);
                return null;
            }
        }
        public async Task<bool> ChangeOtp(TblOtp Otp)
        {
            try
            {
                if (await GetOtpWithId(Otp.Id) != null)
                {
                    _Otprepository.Update(Otp);
                    return true;
                }
                else
                {
                    Console.WriteLine($"Can not Find Otp With {Otp.Id} Id In Db");
                    _logger.LogInformation($"Can not Find Otp With {Otp.Id} Id In Db");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return false;
            }
        }

        public int GetAllFilteredByCount(Expression<Func<TblOtp, bool>> filter)
        {
            return _Otprepository.GetALlFilteredByCount(filter);
        }

        public int GetAllCount()
        {
            return _Otprepository.GetAllCount();
        }

        public List<TblOtp> GetAllFilteredByPagingAndSearch(Expression<Func<TblOtp, bool>> filter, int Take, int Skip, string search = "")
        {
            string sortField = "Id"; //just for example
            string sortDirection = "descending"; //also for example

            if (!String.IsNullOrEmpty(search))
            {
                IQueryable<TblOtp> QuerySearch = _Otprepository.FindListByCondition(filter).Where(a => a.Id.Equals(search)).AsQueryable();
                return QuerySearch.OrderByMember(sortField, sortDirection == "descending").Skip(Skip).Take(Take)
                    .ToList();
            }
            else
            {
                IQueryable<TblOtp> Query = _Otprepository.FindListByCondition(filter).AsQueryable();
                return Query.OrderByMember(sortField, sortDirection == "descending").Skip(Skip).Take(Take)
                    .ToList();
            }

        }

        public async Task<bool> CreateOtp(TblOtp NewRecord)
        {
            try
            {
                if (await GetOtpWithId(NewRecord.Id) == null)
                {
                    _Otprepository.Create(NewRecord);
                    return true;
                }
                else
                {
                    Console.WriteLine($"This OtpCourse Name Is Already Exist => OtpCourseName = {NewRecord.Id}");
                    _logger.LogInformation($"This OtpCourse Name Is Already Exist => OtpCourseName = {NewRecord.Id}");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return false;
            }
        }

        public async Task<TblOtp> CreateNew(string PhoneNumber, Guid? UserId, Guid? CreatorUserId)
        {
            string Code = "858585";
            //string Code = Helpers.RandomString(6);


            var newOtp = new TblOtp()
            {

                CreatedDatetime = DateTime.Now,
                UserId = UserId,
                Code = Code,
                PhoneNumber = PhoneNumber

            };

            await CreateOtp(newOtp);

            return newOtp;
        }

        public async Task<OtpViewModel> OTPSend(string PhoneNumber, Guid? UserId, Guid? CreatorUserId)
        {
            var FoundAllOTPSendForUser = await FindListByCondition(a => a.PhoneNumber == PhoneNumber);


            DateTime Today = DateTime.Now.Date;

            // Check Today Maximum Request in Hour == 5

            //var MaximumRequest = DateTime.Now.AddHours(-1);

            //if (FoundAllOTPSendForUser.Where(a => a.CreatedDatetime >= MaximumRequest && a.CreatedDatetime <= DateTime.Now).Count() > 5)
            //{
            //    return "حد مجاز ارسال کد در هر ساعت 3 پیامک میباشد، لطفا ساعتی دیگر تلاش بفرمایید";
            //}


            // 2 minutes have passed?
            var OldDate = DateTime.Now.AddMinutes(-1);
            if(FoundAllOTPSendForUser!= null)
            {
                if (FoundAllOTPSendForUser.FirstOrDefault(a => a.CreatedDatetime >= OldDate && a.CreatedDatetime <= DateTime.Now) != null)
                {
                    OtpViewModel Otpfaild = new OtpViewModel();
                    Otpfaild.Message = "قبلا درخواست کد داده اید لطفا 1 دقیقه دیگر تلاش بفرمایید";
                    return Otpfaild;
                }
            }
            


            //Create OTP
            var NewOtp = await CreateNew(PhoneNumber, UserId, CreatorUserId);
            OtpViewModel TheOtp = new OtpViewModel();
            //Send Sms 
            try
            {
                var ResultSms = await _sMSService.SendMessageKavenegar(NewOtp.PhoneNumber, "KarmaVerify", NewOtp.UserId, SmsTypeEnum.OtpSend, NewOtp.Code );

                var FoundRecord = await FindByCondition(a => a.Id == NewOtp.Id);

                FoundRecord.SmsId = ResultSms.Id;

                await Update(FoundRecord);
                TheOtp.OtpId = FoundRecord.Id;
                TheOtp.Message = "کد با موفقیت ارسال شد";

            }
            catch (Exception e)
            {
                TheOtp.Message = e.Message;
            }
           

            return TheOtp;


        }


        public async Task<OtpCheckViewModel> OtpCheck(string PhoneNumber, string Code)
        {
            OtpCheckViewModel Result = new OtpCheckViewModel();


            var FoundRecord = (await FindListByCondition(a => a.PhoneNumber == PhoneNumber && a.Code == Code)).OrderByDescending(a => a.CreatedDatetime).FirstOrDefault();


            bool flag = true;
            if (FoundRecord != null)
            {
                Result.Otp = FoundRecord;
                if (DateTime.Now >= FoundRecord.CreatedDatetime.AddMinutes(5))
                {
                    Result.Message = "تاریخ انقضا این کد گذشته است";
                    Result.StatusCode = -1;
                    flag = false;
                }

                if (FoundRecord.EnterDateTime != null)
                {
                    Result.Message = "قبلا از این کد استفاده شده است";
                    Result.StatusCode = -2;
                    flag = false;
                }

                if (flag)
                {
                    FoundRecord.EnterDateTime = DateTime.Now;

                    await Update(FoundRecord);



                    Result.StatusCode = 200;
                    Result.Message = "کد وارد شده معتبر است";


                }
                else
                {
                    Result.Message = "کد مطابقت ندارد";
                    Result.StatusCode = -3;
                }

             
            }
            return Result;

        }
    }
}