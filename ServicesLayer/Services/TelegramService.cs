﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ServicesLayer.Contract;

namespace ServicesLayer.Services
{
    public class TelegramService : ITelegramService
    {

        public class SendMessageToDynamicGroupPostClsResponse
        {
            public bool Send { get; set; }
        }

        public class SendMessageToDynamicGroupPostCls
        {
            public string Text { get; set; }

            public string Chaid { get; set; }
        }

        public async Task<bool> SendMessageToDynamicGroupPost(string MessageText, string Chatid)
        {

            try
            {
                SendMessageToDynamicGroupPostCls NewRequest = new();
                NewRequest.Text = MessageText;
                NewRequest.Chaid = Chatid;



                HttpClient Client = new();
                HttpResponseMessage Response = new ();
                string json = JsonConvert.SerializeObject(NewRequest);
                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                Random r = new ();
                Response = await Client.PostAsync(
                    "https://tel.asanglobal.ir/api/TelegramMessage/SendMessageToDynamicGroupPost", httpContent);

                if (Response.StatusCode == HttpStatusCode.OK)
                {
                    string StrResponse = Response.Content.ReadAsStringAsync().Result;

                    var results = JsonConvert.DeserializeObject<SendMessageToDynamicGroupPostClsResponse>(StrResponse);

                    return results.Send;
                }
            }
            catch (Exception)
            {

            }

            return false;

        }

    }
}
