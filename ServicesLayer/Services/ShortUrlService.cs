﻿using DataAccessLayer.Models;
using DataAccessLayer.ViewModel;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ServicesLayer.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Services
{
    public class ShortUrlService : IShortUrlService
    {
        private readonly ILogger<ShortUrlService> _Logger;

        public ShortUrlService(ILogger<ShortUrlService> Logger)
        {
            _Logger = Logger;
        }
         
        public async Task<string> Create(ShortUrlViewModel NewShortUrl)
        {


            try
            {
                ShortUrlViewModel NewRequest = new ShortUrlViewModel();
                NewRequest.Url = NewShortUrl.Url;
                    


                HttpClient Client = new HttpClient();
                
                HttpResponseMessage Response = new HttpResponseMessage();
                string json = JsonConvert.SerializeObject(NewRequest);

                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                httpContent.Headers.Add("ClientToken", "@mir123@mir");
                Random r = new Random();
                Response = await Client.PostAsync("https://w3s.ir/api/ApiUrlTracking/GetShortUrl", httpContent);

                if (Response.StatusCode == HttpStatusCode.OK)
                {
                    string StrResponse = Response.Content.ReadAsStringAsync().Result;

                    var results = JsonConvert.DeserializeObject<ShortUrlViewModel>(StrResponse);

                    return results.Url;
                }
            }
            catch (Exception E)
            {

            }

            return null;



        }


    }
}
