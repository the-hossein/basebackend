﻿using DataAccessLayer.Entity;
using DataAccessLayer.Models;
using Microsoft.Extensions.Logging;
using RepositoryLayer.Contract;
using ServicesLayer.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Services
{
    public class SampleService : ISampleService
    {

        private readonly ISampleRepository _Sample;
        private readonly ILogger<SampleService> _Logger;
        public SampleService(ISampleRepository Sample, ILogger<SampleService> Logger)
        {
            _Logger = Logger;
            _Sample = Sample;
        }

        public async Task<TblSample> FindByCondition(Expression<Func<TblSample, bool>> Condition)
        {
            try
            {
                return _Sample.FindByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<IQueryable<TblSample>> FindAll()
        {

            try
            {
                return _Sample.FindAll();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<IQueryable<TblSample>> FindListByCondition(Expression<Func<TblSample, bool>> Condition)
        {

            try
            {
                return _Sample.FindListByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }
        public async Task<int?> GetIdWithCondition(Expression<Func<TblSample, bool>> Condition)
        {
            try
            {
                var FoundRecord = _Sample.FindByCondition(Condition);
                if (FoundRecord != null)
                    return FoundRecord.Id;
                else
                    return null;
            }
            catch (Exception)
            {
                Console.Write("This Sample Is not Exist");
                _Logger.LogInformation("This Sample Is not Exist");
                return null;
            }

        }

        public async Task<bool> Update(TblSample UpdateSample)
        {
            try
            {
                TblSample FindSample = await GetSampleWithId(UpdateSample.Id);
                if (FindSample != null)
                {
                    _Sample.Update(UpdateSample);
                }
                else
                {
                    Console.Write($"This SampleUploadFile Is not Exist");
                    _Logger.LogInformation($"This SampleUploadFile Is not Exist");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return false;
            }
        }
        public async Task<bool> Delete(int Id)
        {
            try
            {
                TblSample FindSample = await GetSampleWithId(Id);
                if (FindSample != null)
                {
                    _Sample.Delete(FindSample);
                }
                else
                {
                    Console.Write($"This SampleUploadFile Is not Exist");
                    _Logger.LogInformation($"This SampleUploadFile Is not Exist");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return false;
            }
        }
        public async Task<TblSample> GetSampleWithId(int Id)
        {
            try
            {
                TblSample Sample = _Sample.FindByCondition(c => c.Id == Id);
                if (Sample == null)
                {
                    Console.WriteLine($"Can not Find UserUploadFile With {Id} Id In Db");
                    _Logger.LogInformation($"Can not Find UserUploadFile With {Id} Id In Db");
                    return null;
                }
                return Sample;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<TblSample> Create(TblSample NewRecord)
        {
            try
            {
                if (await GetSampleWithId(NewRecord.Id) == null)
                {
                    TblSample GetRecord = _Sample.Create(NewRecord);
                    return GetRecord;
                }
                else
                {
                    Console.WriteLine($"This ClassesCourse Name Is Already Exist => ClassesCourseName = {NewRecord.Id}");
                    _Logger.LogInformation($"This ClassesCourse Name Is Already Exist => ClassesCourseName = {NewRecord.Id}");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }
    }
}
