﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Services.Helper
{
   public class JsonConvertWrapper
    {
        public static bool TryDeseriliazeObject<TResult>(string value, out TResult result)
        {
            bool res;
            try
            {
                result = JsonConvert.DeserializeObject<TResult>(value);
                res = true;
            }
            catch (Exception)
            {
                result = default;
                res = false;
            }
            return res;
        }
    }
}
