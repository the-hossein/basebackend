﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace ServicesLayer.Services.Helper
{
    public static class Helpers
    {
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string FixShamsiBirthDay(string BirthDay)
        {
            try
            {
                if (BirthDay.Length == 10)
                    return BirthDay;


                string[] BirthDaySpilit = BirthDay.Split('/');


                string Year = BirthDaySpilit[0];
                string Month = BirthDaySpilit[1];
                string Day = BirthDaySpilit[2];

                if (Month.Length == 1)
                    Month = "0" + Month;

                if (Day.Length == 1)
                    Day = "0" + Day;



                return Year + "/" + Month + "/" + Day;


            }
            catch (Exception)
            {

                return BirthDay;
            }

        }








        public static string DescriptionAttr<T>(this T source)
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0) return attributes[0].Description;
            else return source.ToString();
        }


        /// <summary>
        /// به دست آوردن اتریبوت Description یک Enum
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescription(this Enum value)
        {
            var field = value.GetType().GetField(value.ToString());

            var attribute
                = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute))
                    as DescriptionAttribute;

            return attribute == null ? value.ToString() : attribute.Description;
        }


        public static int GetNumberEnum(this Enum value)
        {

            return (int)Enum.Parse(value.GetType(), value.ToString());
        }

        public static T GetEnumFromString<T>(string value)
        {
            if (Enum.IsDefined(typeof(T), value))
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            var enumNames = Enum.GetNames(typeof(T));
            foreach (var enumName in enumNames)
            {
                var e = Enum.Parse(typeof(T), enumName);
                if (value == GetDescription((Enum)e))
                {
                    return (T)e;
                }
            }
            throw new ArgumentException("The value '" + value
                                        + "' does not match a valid enum name or description.");
        }


        public static string ExtractOnlyNumber(this string input)
        {
            return Regex.Match(input, @"\d+").Value;
        }

        public static string ExtractOnlyAlphabet(this string input)
        {
            return new String(input.Where(Char.IsLetter).ToArray());
        }

        public static DateTime ConvertToGregorian(string input)
        {
            var pr = new CultureInfo("fa-ir");
            DateTime res = DateTime.Parse(input, pr);
            return res;
        }
        public static string ConvertToPersianDate(DateTime? input)
        {
            if (input == null)
                return null;

            var pc = new PersianCalendar();
            var res = $"{pc.GetYear(input.Value)}/{pc.GetMonth(input.Value)}/{pc.GetDayOfMonth(input.Value)}";
            return res;

        }
        public static async Task<List<T>> ExecuteQueryList<T>(DbCommand command) where T : class, new()
        {
            using (var reader = command.ExecuteReader())
            {
                var lst = new List<T>();
                var lstColumns = new T().GetType().GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).ToList();
                while (await reader.ReadAsync())
                {
                    var newObject = new T();
                    for (var i = 0; i < reader.FieldCount; i++)
                    {
                        var name = reader.GetName(i);
                        PropertyInfo prop = lstColumns.FirstOrDefault(a => a.Name.ToLower().Equals(name.ToLower()));
                        if (prop == null)
                        {
                            continue;
                        }
                        var val = reader.IsDBNull(i) ? null : reader[i];
                        try { prop.SetValue(newObject, val, null); } catch { prop.SetValue(newObject, null, null); }
                    }
                    lst.Add(newObject);
                }

                return lst;
            }
        }


        public static async Task<T> ExecuteQuery<T>(DbCommand command) where T : class, new()
        {
            using (var reader = command.ExecuteReader())
            {
                var obj = new T();
                var lstColumns = new T().GetType().GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).ToList();
                while (await reader.ReadAsync())
                {
                    var newObject = new T();
                    for (var i = 0; i < reader.FieldCount; i++)
                    {
                        var name = reader.GetName(i);
                        PropertyInfo prop = lstColumns.FirstOrDefault(a => a.Name.ToLower().Equals(name.ToLower()));
                        if (prop == null)
                        {
                            continue;
                        }
                        var val = reader.IsDBNull(i) ? null : reader[i];
                        try { prop.SetValue(newObject, val, null); } catch { prop.SetValue(newObject, null, null); }
                    }
                    obj = newObject;
                }

                return obj;
            }
        }
    }
}
