﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Services.Helper
{
    public class Client
    {
        private HttpClient _httpClient;
        public T SendPostRequest<T>(object jsonBody, string address, List<KeyValuePair<string, string>> headers = null)
        {
            _httpClient = new HttpClient();
            Task<HttpResponseMessage> sender(StringContent reqContent) => _httpClient.PostAsync(address, reqContent);
            var res = SendRequest<T>(sender, jsonBody, headers);
            return res;
        }

        public T SendPutRequest<T>(object jsonBody, string address, List<KeyValuePair<string, string>> headers = null)
        {
            _httpClient = new HttpClient();
            Task<HttpResponseMessage> sender(StringContent reqContent) => _httpClient.PutAsync(address, reqContent);
            var res = SendRequest<T>(sender, jsonBody, headers);
            return res;
        }

        public T SendGetRequest<T>(string address, List<KeyValuePair<string, string>> headers = null)
        {
            _httpClient = new HttpClient();
            Task<HttpResponseMessage> sender(StringContent reqContent) => _httpClient.GetAsync(address);
            var res = SendRequest<T>(sender, null, headers);
            return res;
        }
        public T SendRequest<T>(
            Func<StringContent, Task<HttpResponseMessage>> sender,
            object jsonBody,
            List<KeyValuePair<string, string>> headers = null)
        {
            StringContent reqContent = null;
            if (jsonBody != null)
                reqContent = new StringContent(JsonConvert.SerializeObject(jsonBody), Encoding.UTF8, "application/json");

            if (headers != null)
                foreach (var item in headers)
                    _httpClient.DefaultRequestHeaders.Add(item.Key, item.Value);

            var response = sender(reqContent);
            var contentTask = response.Result.Content.ReadAsStringAsync();
            var content = contentTask.Result;

            if (content != null)
            {
                JsonConvertWrapper.TryDeseriliazeObject(content, out T res);
                return res;
            }
            else
                return default;
        }
    }
}
