﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entity;
using DataAccessLayer.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using RepositoryLayer.Contract;
using ServicesLayer.Common;
using ServicesLayer.Contract;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Processing.Processors.Transforms;

namespace ServicesLayer.Services
{
    public class UserUploadFileService : IUserUploadFileService
    {
        private readonly IUserUploadFileRepository _UserUploadFilerepository;
        private readonly ILogger<UserUploadFileService> _logger;
        public UserUploadFileService(IUserUploadFileRepository UserUploadFilerepository, ILogger<UserUploadFileService> logger)
        {
            _UserUploadFilerepository = UserUploadFilerepository;
            _logger = logger;
        }
        public async Task<IQueryable<TblUserUploadFile>> FindListByCondition(Expression<Func<TblUserUploadFile, bool>> Condition)
        {

            try
            {
                return _UserUploadFilerepository.FindListByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return null;
            }
        }
        public async Task<TblUserUploadFile> FindByCondition(Expression<Func<TblUserUploadFile, bool>> Condition)
        {
            try
            {
                return _UserUploadFilerepository.FindByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return null;
            }
        }
        public async Task<int> GetIdWithCondition(Expression<Func<TblUserUploadFile, bool>> Condition)
        {
            try
            {
                var FoundRecord = _UserUploadFilerepository.FindByCondition(Condition);
                if (FoundRecord != null)
                    return FoundRecord.Id;
                else
                    return 0;
            }
            catch (Exception)
            {
                Console.Write($"This UserUploadFile Is not Exist");
                _logger.LogInformation($"This UserUploadFile Is not Exist");
                return 0;
            }

        }
        public async Task<bool> Update(TblUserUploadFile UpdateUserUploadFile)
        {
            try
            {
                TblUserUploadFile FindUserUploadFile = await GetUserUploadFileWithId(UpdateUserUploadFile.Id);
                if (FindUserUploadFile != null)
                {
                    _UserUploadFilerepository.Update(UpdateUserUploadFile);
                }
                else
                {
                    Console.Write($"This UserUploadFile Is not Exist");
                    _logger.LogInformation($"This UserUploadFile Is not Exist");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return false;
            }
        }
        public async Task<bool> Delete(int Id)
        {
            try
            {
                TblUserUploadFile FindUserUploadFile = await GetUserUploadFileWithId(Id);
                if (FindUserUploadFile != null)
                {
                    _UserUploadFilerepository.Delete(FindUserUploadFile);
                }
                else
                {
                    Console.Write($"This UserUploadFile Is not Exist");
                    _logger.LogInformation($"This UserUploadFile Is not Exist");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return false;
            }
        }
        public async Task<bool> Create(int Id)
        {
            try
            {
                if (await GetUserUploadFileWithId(Id) == null)
                {
                    TblUserUploadFile NewUserUploadFile = new () { CreatedDatetime = DateTime.Now };
                    _UserUploadFilerepository.Create(NewUserUploadFile);
                    return true;
                }
                else
                {
                    Console.WriteLine($"This UserUploadFile Name Is Already Exist => UserUploadFileName = {Id}");
                    _logger.LogInformation($"This UserUploadFile Name Is Already Exist => UserUploadFileName = {Id}");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return false;
            }
        }

        public async Task<TblUserUploadFile> GetUserUploadFileWithId(int Id)
        {
            try
            {
                TblUserUploadFile FindUserUploadFile = _UserUploadFilerepository.FindByCondition(c => c.Id == Id);
                if (FindUserUploadFile == null)
                {
                    Console.WriteLine($"Can not Find UserUploadFile With {Id} Id In Db");
                    _logger.LogInformation($"Can not Find UserUploadFile With {Id} Id In Db");
                    return null;
                }
                return FindUserUploadFile;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                _logger.LogInformation(ex.Message);
                return null;
            }
        }
        public async Task<bool> ChangeUserUploadFile(TblUserUploadFile UserUploadFile)
        {
            try
            {
                if (await GetUserUploadFileWithId(UserUploadFile.Id) != null)
                {
                    _UserUploadFilerepository.Update(UserUploadFile);
                    return true;
                }
                else
                {
                    Console.WriteLine($"Can not Find UserUploadFile With {UserUploadFile.Id} Id In Db");
                    _logger.LogInformation($"Can not Find UserUploadFile With {UserUploadFile.Id} Id In Db");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return false;
            }
        }

        public int GetAllFilteredByCount(Expression<Func<TblUserUploadFile, bool>> filter)
        {
            return _UserUploadFilerepository.GetALlFilteredByCount(filter);
        }

        public int GetAllCount()
        {
            return _UserUploadFilerepository.GetAllCount();
        }

        public List<TblUserUploadFile> GetAllFilteredByPagingAndSearch(Expression<Func<TblUserUploadFile, bool>> filter, int Take, int Skip, string search = "")
        {
            string sortField = "Id"; //just for example
            string sortDirection = "descending"; //also for example

            if (!String.IsNullOrEmpty(search))
            {
                IQueryable<TblUserUploadFile> QuerySearch = _UserUploadFilerepository.FindListByCondition(filter).Where(a => a.Id.Equals(search)).AsQueryable();
                return QuerySearch.OrderByMember(sortField, sortDirection == "descending").Skip(Skip).Take(Take)
                    .ToList();
            }
            else
            {
                IQueryable<TblUserUploadFile> Query = _UserUploadFilerepository.FindListByCondition(filter).AsQueryable();
                return Query.OrderByMember(sortField, sortDirection == "descending").Skip(Skip).Take(Take)
                    .ToList();
            }

        }

        public async Task<TblUserUploadFile> Create(TblUserUploadFile NewRecord)
        {
            try
            {
                if (await GetUserUploadFileWithId(NewRecord.Id) == null)
                {
                    _UserUploadFilerepository.Create(NewRecord);
                    return NewRecord;
                }
                else
                {
                    Console.WriteLine($"This UserUploadFileCourse Name Is Already Exist => UserUploadFileCourseName = {NewRecord.Id}");
                    _logger.LogInformation($"This UserUploadFileCourse Name Is Already Exist => UserUploadFileCourseName = {NewRecord.Id}");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return null;
            }
        }


        private byte[] ResizeAndUploadImageAsync(IFormFile inputImage, int maxWidth)
        {
            using (var writeStream = new MemoryStream())
            {
                using (var image = SixLabors.ImageSharp.Image.Load(inputImage.OpenReadStream())) // IFormFile inputImage
                {
                    if (image.Width > maxWidth)
                    {
                        var thumbScaleFactor = maxWidth / image.Width;
                        image.Mutate(i => i.Resize(maxWidth, image.Height *
                            thumbScaleFactor));
                    }

                    //Encode here for quality
                    var encoder = new JpegEncoder()
                    {
                        Quality = 100 //Use variable to set between 5-30 based on your requirements
                    };

                    //This saves to the memoryStream with encoder
                    image.Save(writeStream, encoder);

                    return writeStream.ToArray();
                }
            }
        }
        private byte[] ConvertImageToByteArray(IFormFile inputImage)
        {
            byte[] result = null;

            // memory stream
            using (var memoryStream = new MemoryStream())
            // filestream
            using (var image = SixLabors.ImageSharp.Image.Load(inputImage.OpenReadStream())) // IFormFile inputImage
            {
                var before = memoryStream.Length;/* Removed this, assuming you are using for debugging?*/
                var beforeMutations = image.Size();

                // dummy resize options
                int width = 200;
                int height = 200;
                IResampler sampler = KnownResamplers.Lanczos3;
                bool compand = true;
                ResizeMode mode = ResizeMode.Stretch;

                // init resize object
                var resizeOptions = new ResizeOptions
                {
                    Size = new SixLabors.ImageSharp.Size(width, height),
                    Sampler = sampler,
                    Compand = compand,
                    Mode = mode
                };

                // mutate image
                image.Mutate(x => x
                    .Resize(resizeOptions));

                var afterMutations = image.Size();

                //Encode here for quality
                var encoder = new JpegEncoder()
                {
                    //Use variable to set between 5-30 based on your requirements
                };

                //This saves to the memoryStream with encoder
                image.Save(memoryStream, encoder);
                memoryStream.Position = 0; // The position needs to be reset.

                // prepare result to byte[]
                result = memoryStream.ToArray();

                var after = memoryStream.Length; // kind of not needed.
            }

            return result;
        }



        public async Task<TblUserUploadFile> UploadFile(UploadFileViewModelRequest model)
        {

            //FilePath
            const String folderName = "wwwroot/UploadFiles";
            String folderPath = Path.Combine(Directory.GetCurrentDirectory(), folderName);


            string Filename = DateTime.Now.ToString().Trim().Replace("/", "-").Replace(":", "-").Trim() + "F" +
                              model.File.FileName.Trim();

            var NewRecord = new TblUserUploadFile()
            {

                CreatedDatetime = DateTime.Now,
                Title = model.Title,
                Description = model.Description,
                UserId = model.UserId,
                FilePath = "/" + folderName.Replace("wwwroot/", "") + "/" + Filename,
                SaveSuccess = false,
                Filename = Filename
            };

            var SaveRecord = await Create(NewRecord);

            using (var fileContentStream = new MemoryStream())
            {

                await model.File.CopyToAsync(fileContentStream);

                await System.IO.File.WriteAllBytesAsync(Path.Combine(folderPath, Filename),
                    ResizeAndUploadImageAsync(model.File, 800).ToArray());
            }

            var FoundRecord = await FindByCondition(a => a.Filename == NewRecord.Filename);

            FoundRecord.SaveSuccess = true;

            await Update(FoundRecord);


            return FoundRecord;

        }

        public async Task<TblUserUploadFile> UploadVideo(UploadVideoViewModelRequest model)
        {

            //FilePath
            const String folderName = "wwwroot/UploadFiles";
            String folderPath = Path.Combine(Directory.GetCurrentDirectory(), folderName);


            string Filename = DateTime.Now.ToString().Trim().Replace("/", "-").Replace(":", "-").Trim() + "F" +
                              model.File.FileName.Trim();

            var NewRecord = new TblUserUploadFile()
            {

                CreatedDatetime = DateTime.Now,
                Title = model.Title,
                Description = model.Description,
                UserId = model.UserId,
                FilePath = "/" + folderName.Replace("wwwroot/", "") + "/" + Filename,
                SaveSuccess = false,
                Filename = Filename
            };

            var SaveRecord = await Create(NewRecord);

            using (var fileContentStream = new MemoryStream())
            {

                await model.File.CopyToAsync(fileContentStream);




                Stream stream = model.File.OpenReadStream();
                stream.CopyTo(fileContentStream);
                byte[] bytes = fileContentStream.ToArray();



                await File.WriteAllBytesAsync(Path.Combine(folderPath, Filename), bytes);


            }

            var FoundRecord = await FindByCondition(a => a.Filename == NewRecord.Filename);

            FoundRecord.SaveSuccess = true;

            await Update(FoundRecord);


            return FoundRecord;

        }


        public async Task<TblUserUploadFile> UploadExcel(UploadFileViewModelRequest model)
        {

            //FilePath
            const String folderName = "wwwroot/UploadFiles";
            String folderPath = Path.Combine(Directory.GetCurrentDirectory(), folderName);


            string Filename = DateTime.Now.ToString().Trim().Replace("/", "-").Replace(":", "-").Trim() + "F" +
                              model.File.FileName.Trim();

            var NewRecord = new TblUserUploadFile()
            {

                CreatedDatetime = DateTime.Now,
                Title = model.Title,
                Description = model.Description,
                UserId = model.UserId,
                FilePath = "/" + folderName.Replace("wwwroot/", "") + "/" + Filename,
                SaveSuccess = false,
                Filename = Filename
            };

            var SaveRecord = await Create(NewRecord);

            using (var fileContentStream = new MemoryStream())
            {

                await model.File.CopyToAsync(fileContentStream);


                Stream stream = model.File.OpenReadStream();
                stream.CopyTo(fileContentStream);
                byte[] bytes = fileContentStream.ToArray();



                await File.WriteAllBytesAsync(Path.Combine(folderPath, Filename), bytes);


            }

            var FoundRecord = await FindByCondition(a => a.Filename == NewRecord.Filename);

            FoundRecord.SaveSuccess = true;

            await Update(FoundRecord);


            return FoundRecord;

        }


        public async Task<TblUserUploadFile> UploadExcelWithByteArray(UploadByteArrayFileViewModelRequest model)
        {

            //FilePath
            const String folderName = "wwwroot/UploadFiles";
            String folderPath = Path.Combine(Directory.GetCurrentDirectory(), folderName);


            string Filename = DateTime.Now.ToString().Trim().Replace("/", "-").Replace(":", "-").Trim() + "F" +
                              model.FileName.Trim();

            var NewRecord = new TblUserUploadFile()
            {

                CreatedDatetime = DateTime.Now,
                Title = model.Title,
                Description = model.Description,
                UserId = model.UserId,
                FilePath = "/" + folderName.Replace("wwwroot/", "") + "/" + Filename,
                SaveSuccess = false,
                Filename = Filename
            };

            var SaveRecord = await Create(NewRecord);

            using (var fileContentStream = new MemoryStream())
            {




                await File.WriteAllBytesAsync(Path.Combine(folderPath, Filename), model.Content);


            }

            var FoundRecord = await FindByCondition(a => a.Filename == NewRecord.Filename);

            FoundRecord.SaveSuccess = true;

            await Update(FoundRecord);


            return FoundRecord;

        }

    }
}
