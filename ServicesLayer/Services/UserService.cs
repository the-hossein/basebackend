﻿using DataAccessLayer.Entity;
using DataAccessLayer.Models;
using Microsoft.Extensions.Logging;
using RepositoryLayer.Contract;
using ServicesLayer.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Services
{
    public class UserService : IUserService
    {

        private readonly IUserRepository _User;
        private readonly ILogger<UserService> _Logger;
        public UserService(IUserRepository User, ILogger<UserService> Logger)
        {
            _Logger = Logger;
            _User = User;
        }

        public async Task<ApplicationUser> FindByCondition(Expression<Func<ApplicationUser, bool>> Condition)
        {
            try
            {
                return _User.FindByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<IQueryable<ApplicationUser>> FindAll()
        {

            try
            {
                return _User.FindAll();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<IQueryable<ApplicationUser>> FindListByCondition(Expression<Func<ApplicationUser, bool>> Condition)
        {

            try
            {
                return _User.FindListByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }
        public async Task<Guid?> GetIdWithCondition(Expression<Func<ApplicationUser, bool>> Condition)
        {
            try
            {
                var FoundRecord = _User.FindByCondition(Condition);
                if (FoundRecord != null)
                    return FoundRecord.Id;
                else
                    return null;
            }
            catch (Exception)
            {
                Console.Write("This User Is not Exist");
                _Logger.LogInformation("This User Is not Exist");
                return null;
            }

        }

        public async Task<bool> Update(ApplicationUser UpdateUser)
        {
            try
            {
                ApplicationUser User = await FindByCondition(a => a.Id == UpdateUser.Id);
                if (User != null)
                {
                    _User.Update(UpdateUser);
                }
                else
                {
                    Console.Write($"This ProductUploadFile Is not Exist");
                    _Logger.LogInformation($"This ProductUploadFile Is not Exist");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return false;
            }
        }

    }
}
