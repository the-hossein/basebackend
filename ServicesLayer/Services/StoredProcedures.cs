﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Enums;
using DataAccessLayer.ViewModel.StoredProcedures;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using RepositoryLayer;
using ServicesLayer.Contract;
using ServicesLayer.Services.Helper;

namespace ServicesLayer.Services
{
    public class StoredProcedures : IStoredProcedures
    {
        private readonly ApplicationContext _context;

        public StoredProcedures(ApplicationContext context)
        {
            _context = context;
        }


        public async Task<List<SpGetReserveListResponse>> GetReservationList()
        {
            List<SpGetReserveListResponse> result = new();
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = $"Sp_GetReservatoin";
                command.CommandType = CommandType.StoredProcedure;
                //command.Parameters.Add(new SqlParameter("@UserId", model.UserId));
                _context.Database.OpenConnection();

                result = await Helpers.ExecuteQueryList<SpGetReserveListResponse>(command);
            }
            return result;
        }


       


      

    }
}
