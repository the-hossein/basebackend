﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Enums;
using DataAccessLayer.Entity;
using Microsoft.Extensions.Logging;
using ServicesLayer.Services.Helper;
using RepositoryLayer.Repositories;
using RepositoryLayer.Contract;
using ServicesLayer.Common;
using ServicesLayer.Contract;
using Newtonsoft.Json;
using Kavenegar;
using Kavenegar.Exceptions;

namespace ServicesLayer.Services
{
    public class SmsService : ISmsService
    {
        private readonly ISmsRepository _smsRepository;
        private readonly ILogger<SmsService> _logger;
        public SmsService(ISmsRepository smsRepository, ILogger<SmsService> logger)
        {
            _smsRepository = smsRepository;
            _logger = logger;
        }
        public async Task<IQueryable<TblSms>> FindListByCondition(Expression<Func<TblSms, bool>> Condition)
        {

            try
            {
                return _smsRepository.FindListByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return null;
            }
        }
        public async Task<TblSms> FindByCondition(Expression<Func<TblSms, bool>> Condition)
        {
            try
            {
                return _smsRepository.FindByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return null;
            }
        }
        public async Task<int> GetIdWithCondition(Expression<Func<TblSms, bool>> Condition)
        {
            try
            {
                var FoundRecord = _smsRepository.FindByCondition(Condition);
                if (FoundRecord != null)
                    return FoundRecord.Id;
                else
                    return 0;
            }
            catch (Exception e)
            {
                Console.Write($"This SMS Is not Exist");
                _logger.LogInformation($"This SMS Is not Exist");
                return 0;
            }

        }
        public async Task<bool> Update(TblSms UpdateSMS)
        {
            try
            {
                TblSms FindSMS = await GetSMSWithId(UpdateSMS.Id);
                if (FindSMS != null)
                {
                    _smsRepository.Update(UpdateSMS);
                }
                else
                {
                    Console.Write($"This SMS Is not Exist");
                    _logger.LogInformation($"This SMS Is not Exist");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return false;
            }
        }
        public async Task<bool> Delete(int Id)
        {
            try
            {
                TblSms FindSMS = await GetSMSWithId(Id);
                if (FindSMS != null)
                {
                    _smsRepository.Delete(FindSMS);
                }
                else
                {
                    Console.Write($"This SMS Is not Exist");
                    _logger.LogInformation($"This SMS Is not Exist");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return false;
            }
        }
        public async Task<bool> CreateSMS(int Id)
        {
            try
            {
                if (await GetSMSWithId(Id) == null)
                {
                    TblSms NewSMS = new TblSms { CreatedDatetime = DateTime.Now };
                    _smsRepository.Create(NewSMS);
                    return true;
                }
                else
                {
                    Console.WriteLine($"This SMS Name Is Already Exist => SMSName = {Id}");
                    _logger.LogInformation($"This SMS Name Is Already Exist => SMSName = {Id}");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return false;
            }
        }

        public async Task<TblSms> GetSMSWithId(int Id)
        {
            try
            {
                TblSms FindSMS = _smsRepository.FindByCondition(c => c.Id == Id);
                if (FindSMS == null)
                {
                    Console.WriteLine($"Can not Find SMS With {Id} Id In Db");
                    _logger.LogInformation($"Can not Find SMS With {Id} Id In Db");
                    return null;
                }
                return FindSMS;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                _logger.LogInformation(ex.Message);
                return null;
            }
        }
        public async Task<bool> ChangeSMS(TblSms SMS)
        {
            try
            {
                if (await GetSMSWithId(SMS.Id) != null)
                {
                    _smsRepository.Update(SMS);
                    return true;
                }
                else
                {
                    Console.WriteLine($"Can not Find SMS With {SMS.Id} Id In Db");
                    _logger.LogInformation($"Can not Find SMS With {SMS.Id} Id In Db");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return false;
            }
        }

        public int GetAllFilteredByCount(Expression<Func<TblSms, bool>> filter)
        {
            return _smsRepository.GetALlFilteredByCount(filter);
        }

        public int GetAllCount()
        {
            return _smsRepository.GetAllCount();
        }

        public List<TblSms> GetAllFilteredByPagingAndSearch(Expression<Func<TblSms, bool>> filter, int Take, int Skip, string search = "")
        {
            string sortField = "Id"; //just for example
            string sortDirection = "descending"; //also for example

            if (!String.IsNullOrEmpty(search))
            {
                IQueryable<TblSms> QuerySearch = _smsRepository.FindListByCondition(filter).Where(a => a.Id.Equals(search)).AsQueryable();
                return QuerySearch.OrderByMember(sortField, sortDirection == "descending").Skip(Skip).Take(Take)
                    .ToList();
            }
            else
            {
                IQueryable<TblSms> Query = _smsRepository.FindListByCondition(filter).AsQueryable();
                return Query.OrderByMember(sortField, sortDirection == "descending").Skip(Skip).Take(Take)
                    .ToList();
            }

        }

        public async Task<bool> CreateSMS(TblSms NewRecord)
        {
            try
            {
                if (await GetSMSWithId(NewRecord.Id) == null)
                {
                    _smsRepository.Create(NewRecord);
                    return true;
                }
                else
                {
                    Console.WriteLine($"This SMSCourse Name Is Already Exist => SMSCourseName = {NewRecord.Id}");
                    _logger.LogInformation($"This SMSCourse Name Is Already Exist => SMSCourseName = {NewRecord.Id}");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _logger.LogInformation(ex.Message);
                return false;
            }
        }







        public async Task<TblSms> SendMessageKavenegar(string phoneNumber, string template, Guid? UserId, SmsTypeEnum smsType , string token)
        {
            string Token = "30704964525865306A544670646966536C635952324F5775374F664270586769395A7634627472387059553D";
            string SenderNumber = "10008663";

            TblSms NewSMS = new TblSms()
            {

                CreatedDatetime = DateTime.Now,
                To = phoneNumber,
                Massage = template,
                UserId = UserId,
                From = SenderNumber,
                DeliverStatus = 0,
                SmsType = smsType,
            };

            var CreateSmsStatus = await CreateSMS(NewSMS);


            Kavenegar.Models.SendResult RseponseKavenegar = new Kavenegar.Models.SendResult();
            string ErrorKavenegar = "";
            try
            {
                
                if(token != null)
                {
                    var api = new KavenegarApi(Token);
                    RseponseKavenegar = api.VerifyLookup(phoneNumber, token, template);

                }
                else
                {
                    var api = new KavenegarApi(Token);
                    RseponseKavenegar = api.Send(SenderNumber, phoneNumber, template);
                }

            }
            catch (ApiException ex)
            {
                ErrorKavenegar = ex.Message;
                // در صورتی که خروجی وب سرویس 200 نباشد این خطارخ می دهد.
                Console.Write("Message : " + ex.Message);
            }
            catch (HttpException ex)
            {
                ErrorKavenegar = ex.Message;
                // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد
                Console.Write("Message : " + ex.Message);
            }

            var FoundRecord = await FindByCondition(a => a.Id == NewSMS.Id);

            if (!String.IsNullOrEmpty(ErrorKavenegar))
            {
                FoundRecord.KaveNegarResult = ErrorKavenegar;
            }
            else
            {
                string jsonString = JsonConvert.SerializeObject(RseponseKavenegar);
                FoundRecord.KaveNegarResult = jsonString;

            }

            await Update(FoundRecord);

            return FoundRecord;

        }




    }
}
