﻿using DataAccessLayer.Entity;
using DataAccessLayer.Models;
using Microsoft.Extensions.Logging;
using RepositoryLayer.Contract;
using ServicesLayer.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Services
{
    public class PescriptionService : IPescriptionService
    {

        private readonly IPescriptionRepository _Pescription;
        private readonly ILogger<PescriptionService> _Logger;
        public PescriptionService(IPescriptionRepository Pescription, ILogger<PescriptionService> Logger)
        {
            _Logger = Logger;
            _Pescription = Pescription;
        }

        public async Task<TblPescription> FindByCondition(Expression<Func<TblPescription, bool>> Condition)
        {
            try
            {
                return _Pescription.FindByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<IQueryable<TblPescription>> FindAll()
        {

            try
            {
                return _Pescription.FindAll();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<IQueryable<TblPescription>> FindListByCondition(Expression<Func<TblPescription, bool>> Condition)
        {

            try
            {
                return _Pescription.FindListByCondition(Condition);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }
        public async Task<int?> GetIdWithCondition(Expression<Func<TblPescription, bool>> Condition)
        {
            try
            {
                var FoundRecord = _Pescription.FindByCondition(Condition);
                if (FoundRecord != null)
                    return FoundRecord.Id;
                else
                    return null;
            }
            catch (Exception)
            {
                Console.Write("This Pescription Is not Exist");
                _Logger.LogInformation("This Pescription Is not Exist");
                return null;
            }

        }

        public async Task<bool> Update(TblPescription UpdatePescription)
        {
            try
            {
                TblPescription FindPescription = await GetPescriptionWithId(UpdatePescription.Id);
                if (FindPescription != null)
                {
                    _Pescription.Update(UpdatePescription);
                }
                else
                {
                    Console.Write($"This PescriptionUploadFile Is not Exist");
                    _Logger.LogInformation($"This PescriptionUploadFile Is not Exist");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return false;
            }
        }
        public async Task<bool> Delete(int Id)
        {
            try
            {
                TblPescription FindPescription = await GetPescriptionWithId(Id);
                if (FindPescription != null)
                {
                    _Pescription.Delete(FindPescription);
                }
                else
                {
                    Console.Write($"This PescriptionUploadFile Is not Exist");
                    _Logger.LogInformation($"This PescriptionUploadFile Is not Exist");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return false;
            }
        }
        public async Task<TblPescription> GetPescriptionWithId(int Id)
        {
            try
            {
                TblPescription Pescription = _Pescription.FindByCondition(c => c.Id == Id);
                if (Pescription == null)
                {
                    Console.WriteLine($"Can not Find UserUploadFile With {Id} Id In Db");
                    _Logger.LogInformation($"Can not Find UserUploadFile With {Id} Id In Db");
                    return null;
                }
                return Pescription;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }

        public async Task<TblPescription> Create(TblPescription NewRecord)
        {
            try
            {
                if (await GetPescriptionWithId(NewRecord.Id) == null)
                {
                    TblPescription GetRecord = _Pescription.Create(NewRecord);
                    return GetRecord;
                }
                else
                {
                    Console.WriteLine($"This ClassesCourse Name Is Already Exist => ClassesCourseName = {NewRecord.Id}");
                    _Logger.LogInformation($"This ClassesCourse Name Is Already Exist => ClassesCourseName = {NewRecord.Id}");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                _Logger.LogInformation(ex.Message);
                return null;
            }
        }
    }
}
