﻿using System;

namespace ServicesLayer.Common.Formatter
{
    public static class Formatter
    {

        /// <param name="dt"></param>
        /// <returns>date in "yyyy/MM/dd HH:mm" format</returns>
        public static string DT(DateTime? dt)
        {
            if (dt == null) return "";
            return Convert.ToDateTime(dt).ToString("yyyy/MM/dd HH:mm");
        }

        /// <param name="dt"></param>
        /// <returns>date in "yy/MM/dd HH:mm" format</returns>
        public static string ShortDT(DateTime? dt)
        {
            if (dt == null) return "";
            return Convert.ToDateTime(dt).ToString("yy/MM/dd HH:mm");
        }

        /// <param name="dt"></param>
        /// <returns>date in "yyyy/MM/dd" format</returns>
        public static string Date(DateTime? dt)
        {
            if (dt == null) return "";
            return Convert.ToDateTime(dt).ToString("yyyy/MM/dd");
        }

        /// <param name="dt"></param>
        /// <returns>date in "yy/MM/dd" format</returns>
        public static string ShortDate(DateTime? dt)
        {
            if (dt == null) return "";
            return Convert.ToDateTime(dt).ToString("yy/MM/dd");
        }

        /// <param name="dt"></param>
        /// <param name="format"></param>
        /// <returns>date in given format</returns>
        public static string CustomDT(DateTime? dt, string format)
        {
            if (dt == null) return "";
            return Convert.ToDateTime(dt).ToString(format);
        }


    }
}
