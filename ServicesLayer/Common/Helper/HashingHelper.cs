﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Common.Helper
{
    public class HashingHelper
    {
        public static string HashUsingPbkdf2(string password, string salt)
        {

            byte[] bytesBuff = Encoding.Unicode.GetBytes(password);
            using (Aes aes = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(salt, bytesBuff);
                aes.Key = crypto.GetBytes(32);
                aes.IV = crypto.GetBytes(16);
                using (System.IO.MemoryStream mStream = new System.IO.MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }
                    password = Convert.ToBase64String(mStream.ToArray());
                }
            }
            return password;
        }
    }
}
