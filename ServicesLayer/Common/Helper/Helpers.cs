﻿using DataAccessLayer.ViewModel.StoredProcedures;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DataAccessLayer.Entity;

namespace ServicesLayer.Common.Helper
{
    public static class Helpers
    {
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string FixShamsiBirthDay(string BirthDay)
        {
            try
            {
                if (BirthDay.Length == 10)
                    return BirthDay;


                string[] BirthDaySpilit = BirthDay.Split('/');


                string Year = BirthDaySpilit[0];
                string Month = BirthDaySpilit[1];
                string Day = BirthDaySpilit[2];

                if (Month.Length == 1)
                    Month = "0" + Month;

                if (Day.Length == 1)
                    Day = "0" + Day;



                return Year + "/" + Month + "/" + Day;


            }
            catch (Exception)
            {

                return BirthDay;
            }

        }

        public static bool IsValidBirthDate(this string Birth)
        {
            if (Regex.IsMatch(Birth, @"(?:1[23]\d{2})(\/|\-)(?:0?[1-9]|1[0-2])(\/|\-)(?:0?[1-9]|[12][0-9]|3[01])$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsValidPostCode(this string PostCode)
        {
            if (String.IsNullOrEmpty(PostCode))
                return false;

            var allDigitEqual = new[]
               {
                "1111111111", "6511111111", "8111111111", "0000000000", "3111111111", "7911111111", "8888888888",
                "7777777777", "111111111", "0111111111" , "8999999999" , "1234567891", "1234567890", "9411111111", "1288899999", "1222222222"
            };

            if (allDigitEqual.Contains(PostCode)) return false;

            if (PostCode.Length != 10)
                return false;

            Regex regex = new Regex("^[0-9]+$");

            if (regex.IsMatch(PostCode))
            {
                return true;
            }


            return false;
        }


        /// <summary>
        ///     تعیین معتبر بودن کد ملی
        /// </summary>
        /// <param name="nationalCode">کد ملی وارد شده</param>
        /// <returns>
        ///     در صورتی که کد ملی صحیح باشد خروجی <c>true</c> و در صورتی که کد ملی اشتباه باشد خروجی <c>false</c> خواهد بود
        /// </returns>
        /// <exception cref="System.Exception"></exception>
        public static bool IsValidNationalCode(this string nationalCode)
        {
            try
            {
                //در صورتی که کد ملی وارد شده تهی باشد

                if (string.IsNullOrEmpty(nationalCode))
                    throw new Exception("لطفا کد ملی را صحیح وارد نمایید");


                //در صورتی که کد ملی وارد شده طولش کمتر از 10 رقم باشد
                if (nationalCode.Length != 10)
                    throw new Exception("طول کد ملی باید ده کاراکتر باشد");

                //در صورتی که کد ملی ده رقم عددی نباشد
                var regex = new Regex(@"\d{10}");
                if (!regex.IsMatch(nationalCode))
                    throw new Exception("کد ملی تشکیل شده از ده رقم عددی می‌باشد؛ لطفا کد ملی را صحیح وارد نمایید");

                //در صورتی که رقم‌های کد ملی وارد شده یکسان باشد
                var allDigitEqual = new[]
                {
                "0000000000", "1111111111", "2222222222", "3333333333", "4444444444", "5555555555", "6666666666",
                "7777777777", "8888888888", "9999999999" , "1234567891" , "9876543210", "1234567890"
            };
                if (allDigitEqual.Contains(nationalCode)) return false;


                //عملیات شرح داده شده در بالا
                var chArray = nationalCode.ToCharArray();
                var num0 = Convert.ToInt32(chArray[0].ToString()) * 10;
                var num2 = Convert.ToInt32(chArray[1].ToString()) * 9;
                var num3 = Convert.ToInt32(chArray[2].ToString()) * 8;
                var num4 = Convert.ToInt32(chArray[3].ToString()) * 7;
                var num5 = Convert.ToInt32(chArray[4].ToString()) * 6;
                var num6 = Convert.ToInt32(chArray[5].ToString()) * 5;
                var num7 = Convert.ToInt32(chArray[6].ToString()) * 4;
                var num8 = Convert.ToInt32(chArray[7].ToString()) * 3;
                var num9 = Convert.ToInt32(chArray[8].ToString()) * 2;
                var a = Convert.ToInt32(chArray[9].ToString());

                var b = num0 + num2 + num3 + num4 + num5 + num6 + num7 + num8 + num9;
                var c = b % 11;

                return ((c < 2) && (a == c)) || ((c >= 2) && (11 - c == a));
            }
            catch (Exception)
            {

                return false;
            }

        }




        public static bool IsValidFullname(string Fullname)
        {
            //input has 11 digits that all of them are not equal
            if (!Regex.IsMatch(Fullname, @"^[\u0600-\u06FF\.\'\-]{2,50}(?: [\u0600-\u06FF\.\'\-]{2,50})+$"))
                return false;



            return true;
        }



        public static string DescriptionAttr<T>(this T source)
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0) return attributes[0].Description;
            else return source.ToString();
        }


        /// <summary>
        /// به دست آوردن اتریبوت Description یک Enum
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescription(this Enum value)
        {
            var field = value.GetType().GetField(value.ToString());

            var attribute
                = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute))
                    as DescriptionAttribute;

            return attribute == null ? value.ToString() : attribute.Description;
        }

        public static T GetEnumFromString<T>(string value)
        {
            if (Enum.IsDefined(typeof(T), value))
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            var enumNames = Enum.GetNames(typeof(T));
            foreach (var enumName in enumNames)
            {
                var e = Enum.Parse(typeof(T), enumName);
                if (value == GetDescription((Enum)e))
                {
                    return (T)e;
                }
            }
            throw new ArgumentException("The value '" + value
                                        + "' does not match a valid enum name or description.");
        }


        public static bool IsValidPhoneNumber(string input)
        {
            if (input.Length != 11)
                return false;
            input = input.Trim();

            Regex regex1 = new Regex(@"09(\d{2})(" + input.Substring(4, 1) + @"{7})");
            Regex regex2 = new Regex(@"09(\d{2})(1234567)");
            Regex regex3 = new Regex(@"09(\d{2})(7654321)");

            // Step 2: call Match on Regex instance.
            Match match1 = regex1.Match(input);
            Match match2 = regex2.Match(input);
            Match match3 = regex3.Match(input);
            // Step 3: test for Success.

            if (!String.IsNullOrWhiteSpace(input) && input.Length == 11 && !match1.Success && !match2.Success && !match3.Success)
            {

                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool IsNumber(string input)
        {
            var match = Regex.Match(input, @"^\d+$", RegexOptions.IgnoreCase);

            if (!match.Success)
            {
                // does not match
                return false;
            }
            return true;
        }

        public static string ExtractOnlyNumber(this string input)
        {
            return Regex.Match(input, @"\d+").Value;
        }

        public static string ExtractOnlyAlphabet(this string input)
        {
            return new String(input.Where(Char.IsLetter).ToArray());
        }


       

    }
}
