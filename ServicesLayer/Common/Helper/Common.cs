﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Common.Helper
{
    public static class Common
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
   (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new ();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }


        public static IEnumerable<T> RandomPermutation<T>(this IEnumerable<T> sequence)
        {
            Random random = new ();

            T[] retArray = sequence.ToArray();

            for (int i = 0; i < retArray.Length - 1; i += 1)
            {
                int swapIndex = random.Next(i, retArray.Length);
                if (swapIndex != i)
                {
                    T temp = retArray[i];
                    retArray[i] = retArray[swapIndex];
                    retArray[swapIndex] = temp;
                }
            }
            return retArray;
        }

        public static bool IsEmpty(this string Object)
        {
            return string.IsNullOrEmpty(Object);
        }

        public static bool IsNull<T>(this T Obj)
        {
            return Obj == null;
        }

        public static T To<T>(this IConvertible obj)
        {
            try
            {
                var type = typeof(T);
                return (T)Convert.ChangeType(obj, type);
            }
            catch (Exception)
            {

                throw new Exception("Error");
            }
        }

        public static T To<T>(this IConvertible value, IConvertible ifError)
        {
            try
            {
                Type t = typeof(T);
                Type u = Nullable.GetUnderlyingType(t);

                if (u != null)
                {
                    if (value == null || value.Equals(""))
                        return (T)ifError;

                    return (T)Convert.ChangeType(value, u);
                }
                else
                {
                    if (value == null || value.Equals(""))
                        return (T)(ifError.To<T>());

                    return (T)Convert.ChangeType(value, t);
                }
            }
            catch
            {
                return (T)ifError;
            }
        }
        /// <summary>
        /// تبدیل ک ، ی ،ه فارسی
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ToPersianContent(this string s)
        {
            return (s.Replace("ي", "ی").Replace("ك", "ک").Replace("ة", "ه"));
        }
    }
}
